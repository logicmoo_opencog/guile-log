:- module(clpfd6,[lex_chain/1]).


:- use_module(library(apply)).
%:- use_module(library(apply_macros)).
%:- use_module(library(assoc))
:- use_module(library(vhash)).
:- use_module(library(error)).
:- use_module(library(lists)).
:- use_module(library(pairs)).
:- use_module(library(clpfd1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% lex_chain(+Lists)
%
% Lists are lexicographically non-decreasing.

lex_chain(Lss) :-
        must_be(list(list), Lss),
        maplist(maplist(fd_variable), Lss),
        (   Lss == [] -> true
        ;   Lss = [First|Rest],
            make_propagator(presidual(lex_chain(Lss)), Prop),
            foldl(lex_chain_(Prop), Rest, First, _)
        ).

lex_chain_(Prop, Ls, Prev, Ls) :-
        maplist(prop_init(Prop), Ls),
        lex_le(Prev, Ls).

lex_le([], []).
lex_le([V1|V1s], [V2|V2s]) :-
        ?(V1) #=< ?(V2),
        (   integer(V1) ->
            (   integer(V2) ->
                (   V1 =:= V2 -> lex_le(V1s, V2s) ;  true )
            ;   freeze(V2, lex_le([V1|V1s], [V2|V2s]))
            )
        ;   freeze(V1, lex_le([V1|V1s], [V2|V2s]))
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% tuples_in(+Tuples, +Relation).
%
% True iff all Tuples are elements of Relation. Each element of the
% list Tuples is a list of integers or finite domain variables.
% Relation is a list of lists of integers. Arbitrary finite relations,
% such as compatibility tables, can be modeled in this way. For
% example, if 1 is compatible with 2 and 5, and 4 is compatible with 0
% and 3:
%
% ==
% ?- tuples_in([[X,Y]], [[1,2],[1,5],[4,0],[4,3]]), X = 4.
% X = 4,
% Y in 0\/3.
% ==
%
% As another example, consider a train schedule represented as a list
% of quadruples, denoting departure and arrival places and times for
% each train. In the following program, Ps is a feasible journey of
% length 3 from A to D via trains that are part of the given schedule.
%
% ==
% :- use_module(library(clpfd)).
%
% trains([[1,2,0,1],
%         [2,3,4,5],
%         [2,3,0,1],
%         [3,4,5,6],
%         [3,4,2,3],
%         [3,4,8,9]]).
%
% threepath(A, D, Ps) :-
%         Ps = [[A,B,_T0,T1],[B,C,T2,T3],[C,D,T4,_T5]],
%         T2 #> T1,
%         T4 #> T3,
%         trains(Ts),
%         tuples_in(Ps, Ts).
% ==
%
% In this example, the unique solution is found without labeling:
%
% ==
% ?- threepath(1, 4, Ps).
% Ps = [[1, 2, 0, 1], [2, 3, 4, 5], [3, 4, 8, 9]].
% ==

tuples_in(Tuples, Relation) :-
        must_be(list(list), Tuples),
        maplist(maplist(fd_variable), Tuples),
        must_be(list(list(integer)), Relation),
        maplist(relation_tuple(Relation), Tuples),
        do_queue.

relation_tuple(Relation, Tuple) :-
        relation_unifiable(Relation, Tuple, Us, _, _),
        (   ground(Tuple) -> memberchk(Tuple, Relation)
        ;   tuple_domain(Tuple, Us),
            (   Tuple = [_,_|_] -> tuple_freeze(Tuple, Us)
            ;   true
            )
        ).

tuple_domain([], _).
tuple_domain([T|Ts], Relation0) :-
        maplist(list_first_rest, Relation0, Firsts, Relation1),
        (   var(T) ->
            (   Firsts = [Unique] -> T = Unique
            ;   list_to_domain(Firsts, FDom),
                fd_get(T, TDom, TPs),
                domains_intersection(TDom, FDom, TDom1),
                fd_put(T, TDom1, TPs)
            )
        ;   true
        ),
        tuple_domain(Ts, Relation1).

tuple_freeze(Tuple, Relation) :-
        put_attr(R, clpfd_relation, Relation),
        make_propagator(rel_tuple(R, Tuple), Prop),
        tuple_freeze_(Tuple, Prop).

tuple_freeze_([], _).
tuple_freeze_([T|Ts], Prop) :-
        (   var(T) ->
            init_propagator(T, Prop),
            trigger_prop(Prop)
        ;   true
        ),
        tuple_freeze_(Ts, Prop).

relation_unifiable([], _, [], Changed, Changed).
relation_unifiable([R|Rs], Tuple, Us, Changed0, Changed) :-
        (   all_in_domain(R, Tuple) ->
            Us = [R|Rest],
            relation_unifiable(Rs, Tuple, Rest, Changed0, Changed)
        ;   relation_unifiable(Rs, Tuple, Us, true, Changed)
        ).

all_in_domain([], []).
all_in_domain([A|As], [T|Ts]) :-
        (   fd_get(T, Dom, _) ->
            domain_contains(Dom, A)
        ;   T =:= A
        ),
        all_in_domain(As, Ts).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% trivial propagator, used only to remember pending constraints
run_propagator(presidual(_), _).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(pdifferent(Left,Right,X,_), MState) :-
        run_propagator(pexclude(Left,Right,X), MState).

run_propagator(weak_distinct(Left,Right,X,_), _MState) :-
        (   ground(X) ->
            disable_queue,
            exclude_fire(Left, Right, X),
            enable_queue
        ;   outof_reducer(Left, Right, X)
            %(   var(X) -> kill_if_isolated(Left, Right, X, MState)
            %;   true
            %)
        ).

run_propagator(pexclude(Left,Right,X), _) :-
        (   ground(X) ->
            disable_queue,
            exclude_fire(Left, Right, X),
            enable_queue
        ;   true
        ).

run_propagator(pdistinct(Ls), _MState) :-
        distinct(Ls).

run_propagator(check_distinct(Left,Right,X), _) :-
        \+ list_contains(Left, X),
        \+ list_contains(Right, X).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(pelement(N, Is, V), MState) :-
        (   fd_get(N, NDom, _) ->
            (   fd_get(V, VDom, VPs) ->
                integers_remaining(Is, 1, NDom, empty, VDom1),
                domains_intersection(VDom, VDom1, VDom2),
                fd_put(V, VDom2, VPs)
            ;   true
            )
        ;   kill(MState), nth1(N, Is, V)
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(pgcc_single(Vs, Pairs), _) :- gcc_global(Vs, Pairs).

run_propagator(pgcc_check_single(Pairs), _) :- gcc_check(Pairs).

run_propagator(pgcc_check(Pairs), _) :- gcc_check(Pairs).

run_propagator(pgcc(Vs, _, Pairs), _) :- gcc_global(Vs, Pairs).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(pcircuit(Vs), _MState) :-
        distinct(Vs),
        propagate_circuit(Vs).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(pneq(A, B), MState) :-
        (   nonvar(A) ->
            (   nonvar(B) -> A =\= B, kill(MState)
            ;   fd_get(B, BD0, BExp0),
                domain_remove(BD0, A, BD1),
                kill(MState),
                fd_put(B, BD1, BExp0)
            )
        ;   nonvar(B) -> run_propagator(pneq(B, A), MState)
        ;   A \== B,
            fd_get(A, _, AI, AS, _), fd_get(B, _, BI, BS, _),
            (   AS cis_lt BI -> kill(MState)
            ;   AI cis_gt BS -> kill(MState)
            ;   true
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(pgeq(A,B), MState) :-
        (   A == B -> kill(MState)
        ;   nonvar(A) ->
            (   nonvar(B) -> kill(MState), A >= B
            ;   fd_get(B, BD, BPs),
                domain_remove_greater_than(BD, A, BD1),
                kill(MState),
                fd_put(B, BD1, BPs)
            )
        ;   nonvar(B) ->
            fd_get(A, AD, APs),
            domain_remove_smaller_than(AD, B, AD1),
            kill(MState),
            fd_put(A, AD1, APs)
        ;   fd_get(A, AD, AL, AU, APs),
            fd_get(B, _, BL, BU, _),
            AU cis_geq BL,
            (   AL cis_geq BU -> kill(MState)
            ;   AU == BL -> kill(MState), A = B
            ;   NAL cis max(AL,BL),
                domains_intersection(AD, from_to(NAL,AU), NAD),
                fd_put(A, NAD, APs),
                (   fd_get(B, BD2, BL2, BU2, BPs2) ->
                    NBU cis min(BU2, AU),
                    domains_intersection(BD2, from_to(BL2,NBU), NBD),
                    fd_put(B, NBD, BPs2)
                ;   true
                )
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(rel_tuple(R, Tuple), MState) :-
        get_attr(R, clpfd_relation, Relation),
        (   ground(Tuple) -> kill(MState), memberchk(Tuple, Relation)
        ;   relation_unifiable(Relation, Tuple, Us, false, Changed),
            Us = [_|_],
            (   Tuple = [First,Second], ( ground(First) ; ground(Second) ) ->
                kill(MState)
            ;   true
            ),
            (   Us = [Single] -> kill(MState), Single = Tuple
            ;   Changed ->
                put_attr(R, clpfd_relation, Us),
                disable_queue,
                tuple_domain(Tuple, Us),
                enable_queue
            ;   true
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(pserialized(S_I, D_I, S_J, D_J, _), MState) :-
        (   nonvar(S_I), nonvar(S_J) ->
            kill(MState),
            (   S_I + D_I =< S_J -> true
            ;   S_J + D_J =< S_I -> true
            ;   false
            )
        ;   serialize_lower_upper(S_I, D_I, S_J, D_J, MState),
            serialize_lower_upper(S_J, D_J, S_I, D_I, MState)
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% abs(X-Y) #\= C
run_propagator(absdiff_neq(X,Y,C), MState) :-
        (   C < 0 -> kill(MState)
        ;   nonvar(X) ->
            kill(MState),
            (   nonvar(Y) -> abs(X - Y) =\= C
            ;   V1 is X - C, neq_num(Y, V1),
                V2 is C + X, neq_num(Y, V2)
            )
        ;   nonvar(Y) -> kill(MState),
            V1 is C + Y, neq_num(X, V1),
            V2 is Y - C, neq_num(X, V2)
        ;   true
        ).

% abs(X-Y) #>= C
run_propagator(absdiff_geq(X,Y,C), MState) :-
        (   C =< 0 -> kill(MState)
        ;   nonvar(X) ->
            kill(MState),
            (   nonvar(Y) -> abs(X-Y) >= C
            ;   P1 is X - C, P2 is X + C,
                Y in inf..P1 \/ P2..sup
            )
        ;   nonvar(Y) ->
            kill(MState),
            P1 is Y - C, P2 is Y + C,
            X in inf..P1 \/ P2..sup
        ;   true
        ).

% X #\= Y + Z
run_propagator(x_neq_y_plus_z(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) ->
                (   nonvar(Z) -> kill(MState), X =\= Y + Z
                ;   kill(MState), XY is X - Y, neq_num(Z, XY)
                )
            ;   nonvar(Z) -> kill(MState), XZ is X - Z, neq_num(Y, XZ)
            ;   true
            )
        ;   nonvar(Y) ->
            (   nonvar(Z) ->
                kill(MState), YZ is Y + Z, neq_num(X, YZ)
            ;   Y =:= 0 -> kill(MState), neq(X, Z)
            ;   true
            )
        ;   Z == 0 -> kill(MState), neq(X, Y)
        ;   true
        ).

% X #=< Y + C
run_propagator(x_leq_y_plus_c(X,Y,C), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), X =< Y + C
            ;   kill(MState),
                R is X - C,
                fd_get(Y, YD, YPs),
                domain_remove_smaller_than(YD, R, YD1),
                fd_put(Y, YD1, YPs)
            )
        ;   nonvar(Y) ->
            kill(MState),
            R is Y + C,
            fd_get(X, XD, XPs),
            domain_remove_greater_than(XD, R, XD1),
            fd_put(X, XD1, XPs)
        ;   (   X == Y -> C >= 0, kill(MState)
            ;   fd_get(Y, YD, _),
                (   domain_supremum(YD, n(YSup)) ->
                    YS1 is YSup + C,
                    fd_get(X, XD, XPs),
                    domain_remove_greater_than(XD, YS1, XD1),
                    fd_put(X, XD1, XPs)
                ;   true
                ),
                (   fd_get(X, XD2, _), domain_infimum(XD2, n(XInf)) ->
                    XI1 is XInf - C,
                    (   fd_get(Y, YD1, YPs1) ->
                        domain_remove_smaller_than(YD1, XI1, YD2),
                        (   domain_infimum(YD2, n(YInf)),
                            domain_supremum(XD2, n(XSup)),
                            XSup =< YInf + C ->
                            kill(MState)
                        ;   true
                        ),
                        fd_put(Y, YD2, YPs1)
                    ;   true
                    )
                ;   true
                )
            )
        ).

run_propagator(scalar_product_neq(Cs0,Vs0,P0), MState) :-
        coeffs_variables_const(Cs0, Vs0, Cs, Vs, 0, I),
        P is P0 - I,
        (   Vs = [] -> kill(MState), P =\= 0
        ;   Vs = [V], Cs = [C] ->
            kill(MState),
            (   C =:= 1 -> neq_num(V, P)
            ;   C*V #\= P
            )
        ;   Cs == [1,-1] -> kill(MState), Vs = [A,B], x_neq_y_plus_z(A, B, P)
        ;   Cs == [-1,1] -> kill(MState), Vs = [A,B], x_neq_y_plus_z(B, A, P)
        ;   P =:= 0, Cs = [1,1,-1] ->
            kill(MState), Vs = [A,B,C], x_neq_y_plus_z(C, A, B)
        ;   P =:= 0, Cs = [1,-1,1] ->
            kill(MState), Vs = [A,B,C], x_neq_y_plus_z(B, A, C)
        ;   P =:= 0, Cs = [-1,1,1] ->
            kill(MState), Vs = [A,B,C], x_neq_y_plus_z(A, B, C)
        ;   true
        ).

run_propagator(scalar_product_leq(Cs0,Vs0,P0), MState) :-
        coeffs_variables_const(Cs0, Vs0, Cs, Vs, 0, I),
        P is P0 - I,
        (   Vs = [] -> kill(MState), P >= 0
        ;   sum_finite_domains(Cs, Vs, Infs, Sups, 0, 0, Inf, Sup),
            D1 is P - Inf,
            disable_queue,
            (   Infs == [], Sups == [] ->
                Inf =< P,
                (   Sup =< P -> kill(MState)
                ;   remove_dist_upper_leq(Cs, Vs, D1)
                )
            ;   Infs == [] -> Inf =< P, remove_dist_upper(Sups, D1)
            ;   Sups = [_], Infs = [_] ->
                remove_upper(Infs, D1)
            ;   Infs = [_] -> remove_upper(Infs, D1)
            ;   true
            ),
            enable_queue
        ).

run_propagator(scalar_product_eq(Cs0,Vs0,P0), MState) :-
        coeffs_variables_const(Cs0, Vs0, Cs, Vs, 0, I),
        P is P0 - I,
        (   Vs = [] -> kill(MState), P =:= 0
        ;   Vs = [V], Cs = [C] -> kill(MState), P mod C =:= 0, V is P // C
        ;   Cs == [1,1] -> kill(MState), Vs = [A,B], A + B #= P
        ;   Cs == [1,-1] -> kill(MState), Vs = [A,B], A #= P + B
        ;   Cs == [-1,1] -> kill(MState), Vs = [A,B], B #= P + A
        ;   Cs == [-1,-1] -> kill(MState), Vs = [A,B], P1 is -P, A + B #= P1
        ;   P =:= 0, Cs == [1,1,-1] -> kill(MState), Vs = [A,B,C], A + B #= C
        ;   P =:= 0, Cs == [1,-1,1] -> kill(MState), Vs = [A,B,C], A + C #= B
        ;   P =:= 0, Cs == [-1,1,1] -> kill(MState), Vs = [A,B,C], B + C #= A
        ;   sum_finite_domains(Cs, Vs, Infs, Sups, 0, 0, Inf, Sup),
            % nl, writeln(Infs-Sups-Inf-Sup),
            D1 is P - Inf,
            D2 is Sup - P,
            disable_queue,
            (   Infs == [], Sups == [] ->
                between(Inf, Sup, P),
                remove_dist_upper_lower(Cs, Vs, D1, D2)
            ;   Sups = [] -> P =< Sup, remove_dist_lower(Infs, D2)
            ;   Infs = [] -> Inf =< P, remove_dist_upper(Sups, D1)
            ;   Sups = [_], Infs = [_] ->
                remove_lower(Sups, D2),
                remove_upper(Infs, D1)
            ;   Infs = [_] -> remove_upper(Infs, D1)
            ;   Sups = [_] -> remove_lower(Sups, D2)
            ;   true
            ),
            enable_queue
        ).

% X + Y = Z
run_propagator(pplus(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   X =:= 0 -> kill(MState), Y = Z
            ;   Y == Z -> kill(MState), X =:= 0
            ;   nonvar(Y) -> kill(MState), Z is X + Y
            ;   nonvar(Z) -> kill(MState), Y is Z - X
            ;   fd_get(Z, ZD, ZPs),
                fd_get(Y, YD, _),
                domain_shift(YD, X, Shifted_YD),
                domains_intersection(ZD, Shifted_YD, ZD1),
                fd_put(Z, ZD1, ZPs),
                (   fd_get(Y, YD1, YPs) ->
                    O is -X,
                    domain_shift(ZD1, O, YD2),
                    domains_intersection(YD1, YD2, YD3),
                    fd_put(Y, YD3, YPs)
                ;   true
                )
            )
        ;   nonvar(Y) -> run_propagator(pplus(Y,X,Z), MState)
        ;   nonvar(Z) ->
            (   X == Y -> kill(MState), even(Z), X is Z // 2
            ;   fd_get(X, XD, _),
                fd_get(Y, YD, YPs),
                domain_negate(XD, XDN),
                domain_shift(XDN, Z, YD1),
                domains_intersection(YD, YD1, YD2),
                fd_put(Y, YD2, YPs),
                (   fd_get(X, XD1, XPs) ->
                    domain_negate(YD2, YD2N),
                    domain_shift(YD2N, Z, XD2),
                    domains_intersection(XD1, XD2, XD3),
                    fd_put(X, XD3, XPs)
                ;   true
                )
            )
        ;   (   X == Y -> kill(MState), 2*X #= Z
            ;   X == Z -> kill(MState), Y = 0
            ;   Y == Z -> kill(MState), X = 0
            ;   fd_get(X, XD, XL, XU, XPs),
                fd_get(Y, _, YL, YU, _),
                fd_get(Z, _, ZL, ZU, _),
                NXL cis max(XL, ZL-YU),
                NXU cis min(XU, ZU-YL),
                update_bounds(X, XD, XPs, XL, XU, NXL, NXU),
                (   fd_get(Y, YD2, YL2, YU2, YPs2) ->
                    NYL cis max(YL2, ZL-NXU),
                    NYU cis min(YU2, ZU-NXL),
                    update_bounds(Y, YD2, YPs2, YL2, YU2, NYL, NYU)
                ;   NYL = n(Y), NYU = n(Y)
                ),
                (   fd_get(Z, ZD2, ZL2, ZU2, ZPs2) ->
                    NZL cis max(ZL2,NXL+NYL),
                    NZU cis min(ZU2,NXU+NYU),
                    update_bounds(Z, ZD2, ZPs2, ZL2, ZU2, NZL, NZU)
                ;   true
                )
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(ptimes(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Z is X * Y
            ;   X =:= 0 -> kill(MState), Z = 0
            ;   X =:= 1 -> kill(MState), Z = Y
            ;   nonvar(Z) -> kill(MState), 0 =:= Z mod X, Y is Z // X
            ;   (   Y == Z -> kill(MState), Y = 0
                ;   fd_get(Y, YD, _),
                    fd_get(Z, ZD, ZPs),
                    domain_expand(YD, X, Scaled_YD),
                    domains_intersection(ZD, Scaled_YD, ZD1),
                    fd_put(Z, ZD1, ZPs),
                    (   fd_get(Y, YDom2, YPs2) ->
                        domain_contract(ZD1, X, Contract),
                        domains_intersection(YDom2, Contract, NYDom),
                        fd_put(Y, NYDom, YPs2)
                    ;   kill(MState), Z is X * Y
                    )
                )
            )
        ;   nonvar(Y) -> run_propagator(ptimes(Y,X,Z), MState)
        ;   nonvar(Z) ->
            (   X == Y ->
                kill(MState),
                integer_kth_root(Z, 2, R),
                NR is -R,
                X in NR \/ R
            ;   fd_get(X, XD, XL, XU, XPs),
                fd_get(Y, YD, YL, YU, _),
                min_max_factor(n(Z), n(Z), YL, YU, XL, XU, NXL, NXU),
                update_bounds(X, XD, XPs, XL, XU, NXL, NXU),
                (   fd_get(Y, YD2, YL2, YU2, YPs2) ->
                    min_max_factor(n(Z), n(Z), NXL, NXU, YL2, YU2, NYL, NYU),
                    update_bounds(Y, YD2, YPs2, YL2, YU2, NYL, NYU)
                ;   (   Y =\= 0 -> 0 =:= Z mod Y, kill(MState), X is Z // Y
                    ;   kill(MState), Z = 0
                    )
                ),
                (   Z =:= 0 ->
                    (   \+ domain_contains(XD, 0) -> kill(MState), Y = 0
                    ;   \+ domain_contains(YD, 0) -> kill(MState), X = 0
                    ;   true
                    )
                ;  neq_num(X, 0), neq_num(Y, 0)
                )
            )
        ;   (   X == Y -> kill(MState), X^2 #= Z
            ;   fd_get(X, XD, XL, XU, XPs),
                fd_get(Y, _, YL, YU, _),
                fd_get(Z, ZD, ZL, ZU, _),
                (   Y == Z, \+ domain_contains(ZD, 0) -> kill(MState), X = 1
                ;   X == Z, \+ domain_contains(ZD, 0) -> kill(MState), Y = 1
                ;   min_max_factor(ZL, ZU, YL, YU, XL, XU, NXL, NXU),
                    update_bounds(X, XD, XPs, XL, XU, NXL, NXU),
                    (   fd_get(Y, YD2, YL2, YU2, YPs2) ->
                        min_max_factor(ZL, ZU, NXL, NXU, YL2, YU2, NYL, NYU),
                        update_bounds(Y, YD2, YPs2, YL2, YU2, NYL, NYU)
                    ;   NYL = n(Y), NYU = n(Y)
                    ),
                    (   fd_get(Z, ZD2, ZL2, ZU2, ZPs2) ->
                        min_product(NXL, NXU, NYL, NYU, NZL),
                        max_product(NXL, NXU, NYL, NYU, NZU),
                        (   NZL cis_leq ZL2, NZU cis_geq ZU2 -> ZD3 = ZD2
                        ;   domains_intersection(ZD2, from_to(NZL,NZU), ZD3),
                            fd_put(Z, ZD3, ZPs2)
                        ),
                        (   domain_contains(ZD3, 0) ->  true
                        ;   neq_num(X, 0), neq_num(Y, 0)
                        )
                    ;   true
                    )
                )
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% X div Y = Z
run_propagator(pdiv(X,Y,Z), MState) :- kill(MState), Z #= (X-(X mod Y)) // Y.

% X rdiv Y = Z
run_propagator(prdiv(X,Y,Z), MState) :- kill(MState), Z*Y #= X.

% X // Y = Z (round towards zero)
run_propagator(ptzdiv(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Y =\= 0, Z is X // Y
            ;   fd_get(Y, YD, YL, YU, YPs),
                (   nonvar(Z) ->
                    (   Z =:= 0 ->
                        NYL is -abs(X) - 1,
                        NYU is abs(X) + 1,
                        domains_intersection(YD, split(0, from_to(inf,n(NYL)),
                                                       from_to(n(NYU), sup)),
                                             NYD),
                        fd_put(Y, NYD, YPs)
                    ;   (   sign(X) =:= sign(Z) ->
                            NYL cis max(n(X) // (n(Z)+sign(n(Z))) + n(1), YL),
                            NYU cis min(n(X) // n(Z), YU)
                        ;   NYL cis max(n(X) // n(Z), YL),
                            NYU cis min(n(X) // (n(Z)+sign(n(Z))) - n(1), YU)
                        ),
                        update_bounds(Y, YD, YPs, YL, YU, NYL, NYU)
                    )
                ;   fd_get(Z, ZD, ZL, ZU, ZPs),
                    (   X >= 0, ( YL cis_gt n(0) ; YU cis_lt n(0) )->
                        NZL cis max(n(X)//YU, ZL),
                        NZU cis min(n(X)//YL, ZU)
                    ;   X < 0, ( YL cis_gt n(0) ; YU cis_lt n(0) ) ->
                        NZL cis max(n(X)//YL, ZL),
                        NZU cis min(n(X)//YU, ZU)
                    ;   % TODO: more stringent bounds, cover Y
                        NZL cis max(-abs(n(X)), ZL),
                        NZU cis min(abs(n(X)), ZU)
                    ),
                    update_bounds(Z, ZD, ZPs, ZL, ZU, NZL, NZU),
                    (   X >= 0, NZL cis_gt n(0), fd_get(Y, YD1, YPs1) ->
                        NYL cis n(X) // (NZU + n(1)) + n(1),
                        NYU cis n(X) // NZL,
                        domains_intersection(YD1, from_to(NYL, NYU), NYD1),
                        fd_put(Y, NYD1, YPs1)
                    ;   true
                    )
                )
            )
        ;   nonvar(Y) ->
            Y =\= 0,
            (   Y =:= 1 -> kill(MState), X = Z
            ;   Y =:= -1 -> kill(MState), Z #= -X
            ;   fd_get(X, XD, XL, XU, XPs),
                (   nonvar(Z) ->
                    kill(MState),
                    (   sign(Z) =:= sign(Y) ->
                        NXL cis max(n(Z)*n(Y), XL),
                        NXU cis min((abs(n(Z))+n(1))*abs(n(Y))-n(1), XU)
                    ;   Z =:= 0 ->
                        NXL cis max(-abs(n(Y)) + n(1), XL),
                        NXU cis min(abs(n(Y)) - n(1), XU)
                    ;   NXL cis max((n(Z)+sign(n(Z)))*n(Y)+n(1), XL),
                        NXU cis min(n(Z)*n(Y), XU)
                    ),
                    update_bounds(X, XD, XPs, XL, XU, NXL, NXU)
                ;   fd_get(Z, ZD, ZPs),
                    domain_contract_less(XD, Y, Contracted),
                    domains_intersection(ZD, Contracted, NZD),
                    fd_put(Z, NZD, ZPs),
                    (   fd_get(X, XD2, XPs2) ->
                        domain_expand_more(NZD, Y, Expanded),
                        domains_intersection(XD2, Expanded, NXD2),
                        fd_put(X, NXD2, XPs2)
                    ;   true
                    )
                )
            )
        ;   nonvar(Z) ->
            fd_get(X, XD, XL, XU, XPs),
            fd_get(Y, _, YL, YU, _),
            (   YL cis_geq n(0), XL cis_geq n(0) ->
                NXL cis max(YL*n(Z), XL),
                NXU cis min(YU*(n(Z)+n(1))-n(1), XU)
            ;   %TODO: cover more cases
                NXL = XL, NXU = XU
            ),
            update_bounds(X, XD, XPs, XL, XU, NXL, NXU)
        ;   (   X == Y -> kill(MState), Z = 1
            ;   fd_get(X, _, XL, XU, _),
                fd_get(Y, _, YL, _, _),
                fd_get(Z, ZD, ZPs),
                NZU cis max(abs(XL), XU),
                NZL cis -NZU,
                domains_intersection(ZD, from_to(NZL,NZU), NZD0),
                (   XL cis_geq n(0), YL cis_geq n(0) ->
                    domain_remove_smaller_than(NZD0, 0, NZD1)
                ;   % TODO: cover more cases
                    NZD1 = NZD0
                ),
                fd_put(Z, NZD1, ZPs)
            )
        ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Y = abs(X)

run_propagator(pabs(X,Y), MState) :-
        (   nonvar(X) -> kill(MState), Y is abs(X)
        ;   nonvar(Y) ->
            kill(MState),
            Y >= 0,
            YN is -Y,
            X in YN \/ Y
        ;   fd_get(X, XD, XPs),
            fd_get(Y, YD, _),
            domain_negate(YD, YDNegative),
            domains_union(YD, YDNegative, XD1),
            domains_intersection(XD, XD1, XD2),
            fd_put(X, XD2, XPs),
            (   fd_get(Y, YD1, YPs1) ->
                domain_negate(XD2, XD2Neg),
                domains_union(XD2, XD2Neg, YD2),
                domain_remove_smaller_than(YD2, 0, YD3),
                domains_intersection(YD1, YD3, YD4),
                fd_put(Y, YD4, YPs1)
            ;   true
            )
        ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z = X mod Y

run_propagator(pmod(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Y =\= 0, Z is X mod Y
            ;   true
            )
        ;   nonvar(Y) ->
            Y =\= 0,
            (   abs(Y) =:= 1 -> kill(MState), Z = 0
            ;   var(Z) ->
                YP is abs(Y) - 1,
                (   Y > 0, fd_get(X, _, n(XL), n(XU), _) ->
                    (   XL >= 0, XU < Y ->
                        kill(MState), Z = X, ZL = XL, ZU = XU
                    ;   ZL = 0, ZU = YP
                    )
                ;   Y > 0 -> ZL = 0, ZU = YP
                ;   YN is -YP, ZL = YN, ZU = 0
                ),
                (   fd_get(Z, ZD, ZPs) ->
                    domains_intersection(ZD, from_to(n(ZL), n(ZU)), ZD1),
                    domain_infimum(ZD1, n(ZMin)),
                    domain_supremum(ZD1, n(ZMax)),
                    fd_put(Z, ZD1, ZPs)
                ;   ZMin = Z, ZMax = Z
                ),
                (   fd_get(X, XD, XPs), domain_infimum(XD, n(XMin)) ->
                    Z1 is XMin mod Y,
                    (   between(ZMin, ZMax, Z1) -> true
                    ;   Y > 0 ->
                        Next is ((XMin - ZMin + Y - 1) div Y)*Y + ZMin,
                        domain_remove_smaller_than(XD, Next, XD1),
                        fd_put(X, XD1, XPs)
                    ;   neq_num(X, XMin)
                    )
                ;   true
                ),
                (   fd_get(X, XD2, XPs2), domain_supremum(XD2, n(XMax)) ->
                    Z2 is XMax mod Y,
                    (   between(ZMin, ZMax, Z2) -> true
                    ;   Y > 0 ->
                        Prev is ((XMax - ZMin) div Y)*Y + ZMax,
                        domain_remove_greater_than(XD2, Prev, XD3),
                        fd_put(X, XD3, XPs2)
                    ;   neq_num(X, XMax)
                    )
                ;   true
                )
            ;   fd_get(X, XD, XPs),
                % if possible, propagate at the boundaries
                (   domain_infimum(XD, n(Min)) ->
                    (   Min mod Y =:= Z -> true
                    ;   Y > 0 ->
                        Next is ((Min - Z + Y - 1) div Y)*Y + Z,
                        domain_remove_smaller_than(XD, Next, XD1),
                        fd_put(X, XD1, XPs)
                    ;   neq_num(X, Min)
                    )
                ;   true
                ),
                (   fd_get(X, XD2, XPs2) ->
                    (   domain_supremum(XD2, n(Max)) ->
                        (   Max mod Y =:= Z -> true
                        ;   Y > 0 ->
                            Prev is ((Max - Z) div Y)*Y + Z,
                            domain_remove_greater_than(XD2, Prev, XD3),
                            fd_put(X, XD3, XPs2)
                        ;   neq_num(X, Max)
                        )
                    ;   true
                    )
                ;   true
                )
            )
        ;   X == Y -> kill(MState), Z = 0
        ;   true % TODO: propagate more
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z = X rem Y

run_propagator(prem(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Y =\= 0, Z is X rem Y
            ;   U is abs(X),
                fd_get(Y, YD, _),
                (   X >=0, domain_infimum(YD, n(Min)), Min >= 0 -> L = 0
                ;   L is -U
                ),
                Z in L..U
            )
        ;   nonvar(Y) ->
            Y =\= 0,
            (   abs(Y) =:= 1 -> kill(MState), Z = 0
            ;   var(Z) ->
                YP is abs(Y) - 1,
                YN is -YP,
                (   Y > 0, fd_get(X, _, n(XL), n(XU), _) ->
                    (   abs(XL) < Y, XU < Y -> kill(MState), Z = X, ZL = XL
                    ;   XL < 0, abs(XL) < Y -> ZL = XL
                    ;   XL >= 0 -> ZL = 0
                    ;   ZL = YN
                    ),
                    (   XU > 0, XU < Y -> ZU = XU
                    ;   XU < 0 -> ZU = 0
                    ;   ZU = YP
                    )
                ;   ZL = YN, ZU = YP
                ),
                (   fd_get(Z, ZD, ZPs) ->
                    domains_intersection(ZD, from_to(n(ZL), n(ZU)), ZD1),
                    fd_put(Z, ZD1, ZPs)
                ;   ZD1 = from_to(n(Z), n(Z))
                ),
                (   fd_get(X, XD, _), domain_infimum(XD, n(Min)) ->
                    Z1 is Min rem Y,
                    (   domain_contains(ZD1, Z1) -> true
                    ;   neq_num(X, Min)
                    )
                ;   true
                ),
                (   fd_get(X, XD1, _), domain_supremum(XD1, n(Max)) ->
                    Z2 is Max rem Y,
                    (   domain_contains(ZD1, Z2) -> true
                    ;   neq_num(X, Max)
                    )
                ;   true
                )
            ;   fd_get(X, XD1, XPs1),
                % if possible, propagate at the boundaries
                (   domain_infimum(XD1, n(Min)) ->
                    (   Min rem Y =:= Z -> true
                    ;   Y > 0, Min > 0 ->
                        Next is ((Min - Z + Y - 1) div Y)*Y + Z,
                        domain_remove_smaller_than(XD1, Next, XD2),
                        fd_put(X, XD2, XPs1)
                    ;   % TODO: bigger steps in other cases as well
                        neq_num(X, Min)
                    )
                ;   true
                ),
                (   fd_get(X, XD3, XPs3) ->
                    (   domain_supremum(XD3, n(Max)) ->
                        (   Max rem Y =:= Z -> true
                        ;   Y > 0, Max > 0  ->
                            Prev is ((Max - Z) div Y)*Y + Z,
                            domain_remove_greater_than(XD3, Prev, XD4),
                            fd_put(X, XD4, XPs3)
                        ;   % TODO: bigger steps in other cases as well
                            neq_num(X, Max)
                        )
                    ;   true
                    )
                ;   true
                )
            )
        ;   X == Y -> kill(MState), Z = 0
        ;   fd_get(Z, ZD, ZPs) ->
            fd_get(Y, _, YInf, YSup, _),
            fd_get(X, _, XInf, XSup, _),
            M cis max(abs(YInf),YSup),
            (   XInf cis_geq n(0) -> Inf0 = n(0)
            ;   Inf0 = XInf
            ),
            (   XSup cis_leq n(0) -> Sup0 = n(0)
            ;   Sup0 = XSup
            ),
            NInf cis max(max(Inf0, -M + n(1)), min(XInf,-XSup)),
            NSup cis min(min(Sup0, M - n(1)), max(abs(XInf),XSup)),
            domains_intersection(ZD, from_to(NInf,NSup), ZD1),
            fd_put(Z, ZD1, ZPs)
        ;   true % TODO: propagate more
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z = max(X,Y)

run_propagator(pmax(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Z is max(X,Y)
            ;   nonvar(Z) ->
                (   Z =:= X -> kill(MState), X #>= Y
                ;   Z > X -> Z = Y
                ;   false % Z < X
                )
            ;   fd_get(Y, _, YInf, YSup, _),
                (   YInf cis_gt n(X) -> Z = Y
                ;   YSup cis_lt n(X) -> Z = X
                ;   YSup = n(M) ->
                    fd_get(Z, ZD, ZPs),
                    domain_remove_greater_than(ZD, M, ZD1),
                    fd_put(Z, ZD1, ZPs)
                ;   true
                )
            )
        ;   nonvar(Y) -> run_propagator(pmax(Y,X,Z), MState)
        ;   fd_get(Z, ZD, ZPs) ->
            fd_get(X, _, XInf, XSup, _),
            fd_get(Y, _, YInf, YSup, _),
            (   YInf cis_gt YSup -> kill(MState), Z = Y
            ;   YSup cis_lt XInf -> kill(MState), Z = X
            ;   n(M) cis max(XSup, YSup) ->
                domain_remove_greater_than(ZD, M, ZD1),
                fd_put(Z, ZD1, ZPs)
            ;   true
            )
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z = min(X,Y)

run_propagator(pmin(X,Y,Z), MState) :-
        (   nonvar(X) ->
            (   nonvar(Y) -> kill(MState), Z is min(X,Y)
            ;   nonvar(Z) ->
                (   Z =:= X -> kill(MState), X #=< Y
                ;   Z < X -> Z = Y
                ;   false % Z > X
                )
            ;   fd_get(Y, _, YInf, YSup, _),
                (   YSup cis_lt n(X) -> Z = Y
                ;   YInf cis_gt n(X) -> Z = X
                ;   YInf = n(M) ->
                    fd_get(Z, ZD, ZPs),
                    domain_remove_smaller_than(ZD, M, ZD1),
                    fd_put(Z, ZD1, ZPs)
                ;   true
                )
            )
        ;   nonvar(Y) -> run_propagator(pmin(Y,X,Z), MState)
        ;   fd_get(Z, ZD, ZPs) ->
            fd_get(X, _, XInf, XSup, _),
            fd_get(Y, _, YInf, YSup, _),
            (   YSup cis_lt YInf -> kill(MState), Z = Y
            ;   YInf cis_gt XSup -> kill(MState), Z = X
            ;   n(M) cis min(XInf, YInf) ->
                domain_remove_smaller_than(ZD, M, ZD1),
                fd_put(Z, ZD1, ZPs)
            ;   true
            )
        ;   true
        ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Z = X ^ Y

run_propagator(pexp(X,Y,Z), MState) :-
        (   X == 1 -> kill(MState), Z = 1
        ;   X == 0 -> kill(MState), Z in 0..1, Z #<==> Y #= 0
        ;   Y == 0 -> kill(MState), Z = 1
        ;   Y == 1 -> kill(MState), Z = X
        ;   nonvar(X) ->
            (   nonvar(Y) ->
                (   Y >= 0 -> true ; X =:= -1 ),
                kill(MState),
                Z is X^Y
            ;   nonvar(Z) ->
                (   Z > 1 ->
                    kill(MState),
                    integer_log_b(Z, X, 1, Y)
                ;   true
                )
            ;   fd_get(Y, _, YL, YU, _),
                fd_get(Z, ZD, ZPs),
                (   X > 0, YL cis_geq n(0) ->
                    NZL cis n(X)^YL,
                    NZU cis n(X)^YU,
                    domains_intersection(ZD, from_to(NZL,NZU), NZD),
                    fd_put(Z, NZD, ZPs)
                ;   true
                )
            )
        ;   nonvar(Z) ->
            (   nonvar(Y) ->
                integer_kth_root(Z, Y, R),
                kill(MState),
                (   even(Y) ->
                    N is -R,
                    X in N \/ R
                ;   X = R
                )
            ;   fd_get(X, _, n(NXL), _, _), NXL > 1 ->
                (   Z > 1, between(NXL, Z, Exp), NXL^Exp > Z ->
                    Exp1 is Exp - 1,
                    fd_get(Y, YD, YPs),
                    domains_intersection(YD, from_to(n(1),n(Exp1)), YD1),
                    fd_put(Y, YD1, YPs),
                    (   fd_get(X, XD, XPs) ->
                        domain_infimum(YD1, n(YL)),
                        integer_kth_root_leq(Z, YL, RU),
                        domains_intersection(XD, from_to(n(NXL),n(RU)), XD1),
                        fd_put(X, XD1, XPs)
                    ;   true
                    )
                ;   true
                )
            ;   true
            )
        ;   nonvar(Y), Y > 0 ->
            (   even(Y) ->
                geq(Z, 0)
            ;   true
            ),
            (   fd_get(X, XD, XL, XU, _), fd_get(Z, ZD, ZL, ZU, ZPs) ->
                (   domain_contains(ZD, 0) -> XD1 = XD
                ;   domain_remove(XD, 0, XD1)
                ),
                (   domain_contains(XD, 0) -> ZD1 = ZD
                ;   domain_remove(ZD, 0, ZD1)
                ),
                (   even(Y) ->
                    (   XL cis_geq n(0) ->
                        NZL cis XL^n(Y)
                    ;   XU cis_leq n(0) ->
                        NZL cis XU^n(Y)
                    ;   NZL = n(0)
                    ),
                    NZU cis max(abs(XL),abs(XU))^n(Y),
                    domains_intersection(ZD1, from_to(NZL,NZU), ZD2)
                ;   (   finite(XL) ->
                        NZL cis XL^n(Y),
                        NZU cis XU^n(Y),
                        domains_intersection(ZD1, from_to(NZL,NZU), ZD2)
                    ;   ZD2 = ZD1
                    )
                ),
                fd_put(Z, ZD2, ZPs),
                (   even(Y), ZU = n(Num) ->
                    integer_kth_root_leq(Num, Y, RU),
                    (   XL cis_geq n(0), ZL = n(Num1) ->
                        integer_kth_root_leq(Num1, Y, RL0),
                        (   RL0^Y < Num1 -> RL is RL0 + 1
                        ;   RL = RL0
                        )
                    ;   RL is -RU
                    ),
                    RL =< RU,
                    NXD = from_to(n(RL),n(RU))
                ;   odd(Y), ZL cis_geq n(0), ZU = n(Num) ->
                    integer_kth_root_leq(Num, Y, RU),
                    ZL = n(Num1),
                    integer_kth_root_leq(Num1, Y, RL0),
                    (   RL0^Y < Num1 -> RL is RL0 + 1
                    ;   RL = RL0
                    ),
                    RL =< RU,
                    NXD = from_to(n(RL),n(RU))
                ;   NXD = XD1   % TODO: propagate more
                ),
                (   fd_get(X, XD2, XPs) ->
                    domains_intersection(XD2, XD1, XD3),
                    domains_intersection(XD3, NXD, XD4),
                    fd_put(X, XD4, XPs)
                ;   true
                )
            ;   true
            )
        ;   fd_get(X, _, XL, _, _),
            XL cis_gt n(0),
            fd_get(Y, _, YL, _, _),
            YL cis_gt n(0),
            fd_get(Z, ZD, ZPs) ->
            n(NZL) cis XL^YL,
            domain_remove_smaller_than(ZD, NZL, ZD1),
            fd_put(Z, ZD1, ZPs)
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(pzcompare(Order, A, B), MState) :-
        (   A == B -> kill(MState), Order = (=)
        ;   (   nonvar(A) ->
                (   nonvar(B) ->
                    kill(MState),
                    (   A > B -> Order = (>)
                    ;   Order = (<)
                    )
                ;   fd_get(B, _, BL, BU, _),
                    (   BL cis_gt n(A) -> kill(MState), Order = (<)
                    ;   BU cis_lt n(A) -> kill(MState), Order = (>)
                    ;   true
                    )
                )
            ;   nonvar(B) ->
                fd_get(A, _, AL, AU, _),
                (   AL cis_gt n(B) -> kill(MState), Order = (>)
                ;   AU cis_lt n(B) -> kill(MState), Order = (<)
                ;   true
                )
            ;   fd_get(A, _, AL, AU, _),
                fd_get(B, _, BL, BU, _),
                (   AL cis_gt BU -> kill(MState), Order = (>)
                ;   AU cis_lt BL -> kill(MState), Order = (<)
                ;   true
                )
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reified constraints

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(reified_in(V,Dom,B), MState) :-
        (   integer(V) ->
            kill(MState),
            (   domain_contains(Dom, V) -> B = 1
            ;   B = 0
            )
        ;   B == 1 -> kill(MState), domain(V, Dom)
        ;   B == 0 -> kill(MState), domain_complement(Dom, C), domain(V, C)
        ;   fd_get(V, VD, _),
            (   domains_intersection(VD, Dom, I) ->
                (   I == VD -> kill(MState), B = 1
                ;   true
                )
            ;   kill(MState), B = 0
            )
        ).

run_propagator(reified_tuple_in(Tuple, R, B), MState) :-
        get_attr(R, clpfd_relation, Relation),
        (   B == 1 -> kill(MState), tuples_in([Tuple], Relation)
        ;   (   ground(Tuple) ->
                kill(MState),
                (   memberchk(Tuple, Relation) -> B = 1
                ;   B = 0
                )
            ;   relation_unifiable(Relation, Tuple, Us, _, _),
                (   Us = [] -> kill(MState), B = 0
                ;   true
                )
            )
        ).

run_propagator(tuples_not_in(Tuples, Relation, B), MState) :-
        (   B == 0 ->
            kill(MState),
            tuples_in_conjunction(Tuples, Relation, Conj),
            #\ Conj
        ;   true
        ).

run_propagator(kill_reified_tuples(B, Ps, Bs), _) :-
        (   B == 0 ->
            maplist(kill_entailed, Ps),
            phrase(as(Bs), As),
            maplist(kill_entailed, As)
        ;   true
        ).

run_propagator(reified_fd(V,B), MState) :-
        (   fd_inf(V, I), I \== inf, fd_sup(V, S), S \== sup ->
            kill(MState),
            B = 1
        ;   B == 0 ->
            (   fd_inf(V, inf) -> true
            ;   fd_sup(V, sup) -> true
            ;   false
            )
        ;   true
        ).

% The result of X/Y, X mod Y, and X rem Y is undefined iff Y is 0.

run_propagator(pskeleton(X,Y,D,Skel,Z,_), MState) :-
        (   Y == 0 -> kill(MState), D = 0
        ;   D == 1 -> kill(MState), neq_num(Y, 0), skeleton([X,Y,Z], Skel)
        ;   integer(Y), Y =\= 0 -> kill(MState), D = 1, skeleton([X,Y,Z], Skel)
        ;   fd_get(Y, YD, _), \+ domain_contains(YD, 0) ->
            kill(MState),
            D = 1, skeleton([X,Y,Z], Skel)
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run_propagator(reified_geq(DX,X,DY,Y,Ps,B), MState) :-
        (   DX == 0 -> kill(MState, Ps), B = 0
        ;   DY == 0 -> kill(MState, Ps), B = 0
        ;   B == 1 -> kill(MState), DX = 1, DY = 1, geq(X, Y)
        ;   DX == 1, DY == 1 ->
            (   var(B) ->
                (   nonvar(X) ->
                    (   nonvar(Y) ->
                        kill(MState),
                        (   X >= Y -> B = 1 ; B = 0 )
                    ;   fd_get(Y, _, YL, YU, _),
                        (   n(X) cis_geq YU -> kill(MState, Ps), B = 1
                        ;   n(X) cis_lt YL -> kill(MState, Ps), B = 0
                        ;   true
                        )
                    )
                ;   nonvar(Y) ->
                    fd_get(X, _, XL, XU, _),
                    (   XL cis_geq n(Y) -> kill(MState, Ps), B = 1
                    ;   XU cis_lt n(Y) -> kill(MState, Ps), B = 0
                    ;   true
                    )
                ;   X == Y -> kill(MState, Ps), B = 1
                ;   fd_get(X, _, XL, XU, _),
                    fd_get(Y, _, YL, YU, _),
                    (   XL cis_geq YU -> kill(MState, Ps), B = 1
                    ;   XU cis_lt YL -> kill(MState, Ps), B = 0
                    ;   true
                    )
                )
            ;   B =:= 0 -> kill(MState), X #< Y
            ;   true
            )
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(reified_eq(DX,X,DY,Y,Ps,B), MState) :-
        (   DX == 0 -> kill(MState, Ps), B = 0
        ;   DY == 0 -> kill(MState, Ps), B = 0
        ;   B == 1 -> kill(MState), DX = 1, DY = 1, X = Y
        ;   DX == 1, DY == 1 ->
            (   var(B) ->
                (   nonvar(X) ->
                    (   nonvar(Y) ->
                        kill(MState),
                        (   X =:= Y -> B = 1 ; B = 0)
                    ;   fd_get(Y, YD, _),
                        (   domain_contains(YD, X) -> true
                        ;   kill(MState, Ps), B = 0
                        )
                    )
                ;   nonvar(Y) -> run_propagator(reified_eq(DY,Y,DX,X,Ps,B), MState)
                ;   X == Y -> kill(MState), B = 1
                ;   fd_get(X, _, XL, XU, _),
                    fd_get(Y, _, YL, YU, _),
                    (   XL cis_gt YU -> kill(MState, Ps), B = 0
                    ;   YL cis_gt XU -> kill(MState, Ps), B = 0
                    ;   true
                    )
                )
            ;   B =:= 0 -> kill(MState), X #\= Y
            ;   true
            )
        ;   true
        ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(reified_neq(DX,X,DY,Y,Ps,B), MState) :-
        (   DX == 0 -> kill(MState, Ps), B = 0
        ;   DY == 0 -> kill(MState, Ps), B = 0
        ;   B == 1 -> kill(MState), DX = 1, DY = 1, X #\= Y
        ;   DX == 1, DY == 1 ->
            (   var(B) ->
                (   nonvar(X) ->
                    (   nonvar(Y) ->
                        kill(MState),
                        (   X =\= Y -> B = 1 ; B = 0)
                    ;   fd_get(Y, YD, _),
                        (   domain_contains(YD, X) -> true
                        ;   kill(MState, Ps), B = 1
                        )
                    )
                ;   nonvar(Y) -> run_propagator(reified_neq(DY,Y,DX,X,Ps,B), MState)
                ;   X == Y -> kill(MState), B = 0
                ;   fd_get(X, _, XL, XU, _),
                    fd_get(Y, _, YL, YU, _),
                    (   XL cis_gt YU -> kill(MState, Ps), B = 1
                    ;   YL cis_gt XU -> kill(MState, Ps), B = 1
                    ;   true
                    )
                )
            ;   B =:= 0 -> kill(MState), X = Y
            ;   true
            )
        ;   true
        ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(reified_and(X,Ps1,Y,Ps2,B), MState) :-
        (   nonvar(X) ->
            kill(MState),
            (   X =:= 0 -> maplist(kill_entailed, Ps2), B = 0
            ;   B = Y
            )
        ;   nonvar(Y) -> run_propagator(reified_and(Y,Ps2,X,Ps1,B), MState)
        ;   B == 1 -> kill(MState), X = 1, Y = 1
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(reified_or(X,Ps1,Y,Ps2,B), MState) :-
        (   nonvar(X) ->
            kill(MState),
            (   X =:= 1 -> maplist(kill_entailed, Ps2), B = 1
            ;   B = Y
            )
        ;   nonvar(Y) -> run_propagator(reified_or(Y,Ps2,X,Ps1,B), MState)
        ;   B == 0 -> kill(MState), X = 0, Y = 0
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(reified_not(X,Y), MState) :-
        (   X == 0 -> kill(MState), Y = 1
        ;   X == 1 -> kill(MState), Y = 0
        ;   Y == 0 -> kill(MState), X = 1
        ;   Y == 1 -> kill(MState), X = 0
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
run_propagator(pimpl(X, Y, Ps), MState) :-
        (   nonvar(X) ->
            kill(MState),
            (   X =:= 1 -> Y = 1
            ;   maplist(kill_entailed, Ps)
            )
        ;   nonvar(Y) ->
            kill(MState),
            (   Y =:= 0 -> X = 0
            ;   maplist(kill_entailed, Ps)
            )
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

update_bounds(X, XD, XPs, XL, XU, NXL, NXU) :-
        (   NXL == XL, NXU == XU -> true
        ;   domains_intersection(XD, from_to(NXL, NXU), NXD),
            fd_put(X, NXD, XPs)
        ).

min_product(L1, U1, L2, U2, Min) :-
        Min cis min(min(L1*L2,L1*U2),min(U1*L2,U1*U2)).
max_product(L1, U1, L2, U2, Max) :-
        Max cis max(max(L1*L2,L1*U2),max(U1*L2,U1*U2)).

finite(n(_)).

in_(L, U, X) :-
        fd_get(X, XD, XPs),
        domains_intersection(XD, from_to(L,U), NXD),
        fd_put(X, NXD, XPs).

min_max_factor(L1, U1, L2, U2, L3, U3, Min, Max) :-
        (   U1 cis_lt n(0),
            L2 cis_lt n(0), U2 cis_gt n(0),
            L3 cis_lt n(0), U3 cis_gt n(0) ->
            maplist(in_(L1,U1), [Z1,Z2]),
            in_(L2, n(-1), X1), in_(n(1), U3, Y1),
            (   X1*Y1 #= Z1 ->
                (   fd_get(Y1, _, Inf1, Sup1, _) -> true
                ;   Inf1 = n(Y1), Sup1 = n(Y1)
                )
            ;   Inf1 = inf, Sup1 = n(-1)
            ),
            in_(n(1), U2, X2), in_(L3, n(-1), Y2),
            (   X2*Y2 #= Z2 ->
                (   fd_get(Y2, _, Inf2, Sup2, _) -> true
                ;   Inf2 = n(Y2), Sup2 = n(Y2)
                )
            ;   Inf2 = n(1), Sup2 = sup
            ),
            Min cis max(min(Inf1,Inf2), L3),
            Max cis min(max(Sup1,Sup2), U3)
        ;   L1 cis_gt n(0),
            L2 cis_lt n(0), U2 cis_gt n(0),
            L3 cis_lt n(0), U3 cis_gt n(0) ->
            maplist(in_(L1,U1), [Z1,Z2]),
            in_(L2, n(-1), X1), in_(L3, n(-1), Y1),
            (   X1*Y1 #= Z1 ->
                (   fd_get(Y1, _, Inf1, Sup1, _) -> true
                ;   Inf1 = n(Y1), Sup1 = n(Y1)
                )
            ;   Inf1 = n(1), Sup1 = sup
            ),
            in_(n(1), U2, X2), in_(n(1), U3, Y2),
            (   X2*Y2 #= Z2 ->
                (   fd_get(Y2, _, Inf2, Sup2, _) -> true
                ;   Inf2 = n(Y2), Sup2 = n(Y2)
                )
            ;   Inf2 = inf, Sup2 = n(-1)
            ),
            Min cis max(min(Inf1,Inf2), L3),
            Max cis min(max(Sup1,Sup2), U3)
        ;   min_factor(L1, U1, L2, U2, Min0),
            Min cis max(L3,Min0),
            max_factor(L1, U1, L2, U2, Max0),
            Max cis min(U3,Max0)
        ).

min_factor(L1, U1, L2, U2, Min) :-
        (   L1 cis_geq n(0), L2 cis_gt n(0), finite(U2) ->
            Min cis div(L1+U2-n(1),U2)
        ;   L1 cis_gt n(0), U2 cis_lt n(0) -> Min cis div(U1,U2)
        ;   L1 cis_gt n(0), L2 cis_geq n(0) -> Min = n(1)
        ;   L1 cis_gt n(0) -> Min cis -U1
        ;   U1 cis_lt n(0), U2 cis_leq n(0) ->
            (   finite(L2) -> Min cis div(U1+L2+n(1),L2)
            ;   Min = n(1)
            )
        ;   U1 cis_lt n(0), L2 cis_geq n(0) -> Min cis div(L1,L2)
        ;   U1 cis_lt n(0) -> Min = L1
        ;   L2 cis_leq n(0), U2 cis_geq n(0) -> Min = inf
        ;   Min cis min(min(div(L1,L2),div(L1,U2)),min(div(U1,L2),div(U1,U2)))
        ).
max_factor(L1, U1, L2, U2, Max) :-
        (   L1 cis_geq n(0), L2 cis_geq n(0) -> Max cis div(U1,L2)
        ;   L1 cis_gt n(0), U2 cis_leq n(0) ->
            (   finite(L2) -> Max cis div(L1-L2-n(1),L2)
            ;   Max = n(-1)
            )
        ;   L1 cis_gt n(0) -> Max = U1
        ;   U1 cis_lt n(0), U2 cis_lt n(0) -> Max cis div(L1,U2)
        ;   U1 cis_lt n(0), L2 cis_geq n(0) ->
            (   finite(U2) -> Max cis div(U1-U2+n(1),U2)
            ;   Max = n(-1)
            )
        ;   U1 cis_lt n(0) -> Max cis -L1
        ;   L2 cis_leq n(0), U2 cis_geq n(0) -> Max = sup
        ;   Max cis max(max(div(L1,L2),div(L1,U2)),max(div(U1,L2),div(U1,U2)))
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   J-C. Régin: "A filtering algorithm for constraints of difference in
   CSPs", AAAI-94, Seattle, WA, USA, pp 362--367, 1994
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

distinct_attach([], _, _).
distinct_attach([X|Xs], Prop, Right) :-
        (   var(X) ->
            init_propagator(X, Prop),
            make_propagator(pexclude(Xs,Right,X), P1),
            init_propagator(X, P1),
            trigger_prop(P1)
        ;   exclude_fire(Xs, Right, X)
        ),
        distinct_attach(Xs, Prop, [X|Right]).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   For each integer of the union of domains, an attributed variable is
   introduced, to benefit from constant-time access. Attributes are:

   value ... integer corresponding to the node
   free  ... whether this (right) node is still free
   edges ... [flow_from(F,From)] and [flow_to(F,To)] where F has an
             attribute "flow" that is either 0 or 1 and an attribute "used"
             if it is part of a maximum matching
   parent ... used in breadth-first search
   g0_edges ... [flow_to(F,To)] as above
   visited ... true if node was visited in DFS
   index, in_stack, lowlink ... used in Tarjan's SCC algorithm
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

difference_arcs(Vars, FreeLeft, FreeRight) :-
        empty_assoc(E),
        phrase(difference_arcs(Vars, FreeLeft), [E], [NumVar]),
        assoc_to_list(NumVar, LsNumVar),
        pairs_values(LsNumVar, FreeRight).

domain_to_list(Domain, List) :- phrase(domain_to_list(Domain), List).

domain_to_list(split(_, Left, Right)) -->
        domain_to_list(Left), domain_to_list(Right).
domain_to_list(empty)                 --> [].
domain_to_list(from_to(n(F),n(T)))    --> { numlist(F, T, Ns) }, list(Ns).

difference_arcs([], []) --> [].
difference_arcs([V|Vs], FL0) -->
        (   { fd_get(V, Dom, _), domain_to_list(Dom, Ns) } ->
            { FL0 = [V|FL] },
            enumerate(Ns, V),
            difference_arcs(Vs, FL)
        ;   difference_arcs(Vs, FL0)
        ).

enumerate([], _) --> [].
enumerate([N|Ns], V) -->
        state(NumVar0, NumVar),
        { (   get_assoc(N, NumVar0, Y) -> NumVar0 = NumVar
          ;   put_assoc(N, NumVar0, Y, NumVar),
              put_attr(Y, value, N)
          ),
          put_attr(F, flow, 0),
          append_edge(Y, edges, flow_from(F,V)),
          append_edge(V, edges, flow_to(F,Y)) },
        enumerate(Ns, V).

append_edge(V, Attr, E) :-
        (   get_attr(V, Attr, Es) ->
            put_attr(V, Attr, [E|Es])
        ;   put_attr(V, Attr, [E])
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Strategy: Breadth-first search until we find a free right vertex in
   the value graph, then find an augmenting path in reverse.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

clear_parent(V) :- del_attr(V, parent).

maximum_matching([]).
maximum_matching([FL|FLs]) :-
        augmenting_path_to([[FL]], Levels, To),
        phrase(augmenting_path(FL, To), Path),
        maplist(maplist(clear_parent), Levels),
        del_attr(To, free),
        adjust_alternate_1(Path),
        maximum_matching(FLs).

reachables([]) --> [].
reachables([V|Vs]) -->
        { get_attr(V, edges, Es) },
        reachables_(Es, V),
        reachables(Vs).

reachables_([], _) --> [].
reachables_([E|Es], V) -->
        edge_reachable(E, V),
        reachables_(Es, V).

edge_reachable(flow_to(F,To), V) -->
        (   { get_attr(F, flow, 0),
              \+ get_attr(To, parent, _) } ->
            { put_attr(To, parent, V-F) },
            [To]
        ;   []
        ).
edge_reachable(flow_from(F,From), V) -->
        (   { get_attr(F, flow, 1),
              \+ get_attr(From, parent, _) } ->
            { put_attr(From, parent, V-F) },
            [From]
        ;   []
        ).

augmenting_path_to(Levels0, Levels, Right) :-
        Levels0 = [Vs|_],
        Levels1 = [Tos|Levels0],
        phrase(reachables(Vs), Tos),
        Tos = [_|_],
        (   member(Right, Tos), get_attr(Right, free, true) ->
            Levels = Levels1
        ;   augmenting_path_to(Levels1, Levels, Right)
        ).

augmenting_path(S, V) -->
        (   { V == S } -> []
        ;   { get_attr(V, parent, V1-Augment) },
            [Augment],
            augmenting_path(S, V1)
        ).

adjust_alternate_1([A|Arcs]) :-
        put_attr(A, flow, 1),
        adjust_alternate_0(Arcs).

adjust_alternate_0([]).
adjust_alternate_0([A|Arcs]) :-
        put_attr(A, flow, 0),
        adjust_alternate_1(Arcs).

% Instead of applying Berge's property directly, we can translate the
% problem in such a way, that we have to search for the so-called
% strongly connected components of the graph.

g_g0(V) :-
        get_attr(V, edges, Es),
        maplist(g_g0_(V), Es).

g_g0_(V, flow_to(F,To)) :-
        (   get_attr(F, flow, 1) ->
            append_edge(V, g0_edges, flow_to(F,To))
        ;   append_edge(To, g0_edges, flow_to(F,V))
        ).


g0_successors(V, Tos) :-
        (   get_attr(V, g0_edges, Tos0) ->
            maplist(arg(2), Tos0, Tos)
        ;   Tos = []
        ).

put_free(F) :- put_attr(F, free, true).

free_node(F) :- get_attr(F, free, true).

del_vars_attr(Vars, Attr) :- maplist(del_attr_(Attr), Vars).

del_attr_(Attr, Var) :- del_attr(Var, Attr).

with_local_attributes(Vars, Attrs, Goal, Result) :-
        catch((maplist(del_vars_attr(Vars), Attrs),
               Goal,
               maplist(del_attrs, Vars),
               % reset all attributes, only the result matters
               throw(local_attributes(Result,Vars))),
              local_attributes(Result,Vars),
              true).

distinct(Vars) :-
        with_local_attributes(Vars, [edges,parent,g0_edges,index,visited],
              (difference_arcs(Vars, FreeLeft, FreeRight0),
               length(FreeLeft, LFL),
               length(FreeRight0, LFR),
               LFL =< LFR,
               maplist(put_free, FreeRight0),
               maximum_matching(FreeLeft),
               include(free_node, FreeRight0, FreeRight),
               maplist(g_g0, FreeLeft),
               scc(FreeLeft, g0_successors),
               maplist(dfs_used, FreeRight),
               phrase(distinct_goals(FreeLeft), Gs)), Gs),
        disable_queue,
        maplist(call, Gs),
        enable_queue.

distinct_goals([]) --> [].
distinct_goals([V|Vs]) -->
        { get_attr(V, edges, Es) },
        distinct_goals_(Es, V),
        distinct_goals(Vs).

distinct_goals_([], _) --> [].
distinct_goals_([flow_to(F,To)|Es], V) -->
        (   { get_attr(F, flow, 0),
              \+ get_attr(F, used, true),
              get_attr(V, lowlink, L1),
              get_attr(To, lowlink, L2),
              L1 =\= L2 } ->
            { get_attr(To, value, N) },
            [neq_num(V, N)]
        ;   []
        ),
        distinct_goals_(Es, V).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Mark used edges.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

dfs_used(V) :-
        (   get_attr(V, visited, true) -> true
        ;   put_attr(V, visited, true),
            (   get_attr(V, g0_edges, Es) ->
                dfs_used_edges(Es)
            ;   true
            )
        ).

dfs_used_edges([]).
dfs_used_edges([flow_to(F,To)|Es]) :-
        put_attr(F, used, true),
        dfs_used(To),
        dfs_used_edges(Es).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Tarjan's strongly connected components algorithm.

   DCGs are used to implicitly pass around the global index, stack
   and the predicate relating a vertex to its successors.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

scc(Vs, Succ) :- phrase(scc(Vs), [s(0,[],Succ)], _).

scc([])     --> [].
scc([V|Vs]) -->
        (   vindex_defined(V) -> scc(Vs)
        ;   scc_(V), scc(Vs)
        ).

vindex_defined(V) --> { get_attr(V, index, _) }.

vindex_is_index(V) -->
        state(s(Index,_,_)),
        { put_attr(V, index, Index) }.

vlowlink_is_index(V) -->
        state(s(Index,_,_)),
        { put_attr(V, lowlink, Index) }.

index_plus_one -->
        state(s(I,Stack,Succ), s(I1,Stack,Succ)),
        { I1 is I+1 }.

s_push(V)  -->
        state(s(I,Stack,Succ), s(I,[V|Stack],Succ)),
        { put_attr(V, in_stack, true) }.

vlowlink_min_lowlink(V, VP) -->
        { get_attr(V, lowlink, VL),
          get_attr(VP, lowlink, VPL),
          VL1 is min(VL, VPL),
          put_attr(V, lowlink, VL1) }.

successors(V, Tos) --> state(s(_,_,Succ)), { call(Succ, V, Tos) }.

scc_(V) -->
        vindex_is_index(V),
        vlowlink_is_index(V),
        index_plus_one,
        s_push(V),
        successors(V, Tos),
        each_edge(Tos, V),
        (   { get_attr(V, index, VI),
              get_attr(V, lowlink, VI) } -> pop_stack_to(V, VI)
        ;   []
        ).

pop_stack_to(V, N) -->
        state(s(I,[First|Stack],Succ), s(I,Stack,Succ)),
        { del_attr(First, in_stack) },
        (   { First == V } -> []
        ;   { put_attr(First, lowlink, N) },
            pop_stack_to(V, N)
        ).

each_edge([], _) --> [].
each_edge([VP|VPs], V) -->
        (   vindex_defined(VP) ->
            (   v_in_stack(VP) ->
                vlowlink_min_lowlink(V, VP)
            ;   []
            )
        ;   scc_(VP),
            vlowlink_min_lowlink(V, VP)
        ),
        each_edge(VPs, V).

state(S), [S] --> [S].

state(S0, S), [S] --> [S0].

v_in_stack(V) --> { get_attr(V, in_stack, true) }.

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Weak arc consistent constraint of difference, currently only
   available internally. Candidate for all_different/2 option.

   See Neng-Fa Zhou: "Programming Finite-Domain Constraint Propagators
   in Action Rules", Theory and Practice of Logic Programming, Vol.6,
   No.5, pp 483-508, 2006
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

weak_arc_all_distinct(Ls) :-
        must_be(list, Ls),
        put_attr(O, clpfd_original, weak_arc_all_distinct(Ls)),
        all_distinct(Ls, [], O),
        do_queue.

all_distinct([], _, _).
all_distinct([X|Right], Left, Orig) :-
        %\+ list_contains(Right, X),
        (   var(X) ->
            make_propagator(weak_distinct(Left,Right,X,Orig), Prop),
            init_propagator(X, Prop),
            trigger_prop(Prop)
%             make_propagator(check_distinct(Left,Right,X), Prop2),
%             init_propagator(X, Prop2),
%             trigger_prop(Prop2)
        ;   exclude_fire(Left, Right, X)
        ),
        outof_reducer(Left, Right, X),
        all_distinct(Right, [X|Left], Orig).

exclude_fire(Left, Right, E) :-
        all_neq(Left, E),
        all_neq(Right, E).

list_contains([X|Xs], Y) :-
        (   X == Y -> true
        ;   list_contains(Xs, Y)
        ).

kill_if_isolated(Left, Right, X, MState) :-
        append(Left, Right, Others),
        fd_get(X, XDom, _),
        (   all_empty_intersection(Others, XDom) -> kill(MState)
        ;   true
        ).

all_empty_intersection([], _).
all_empty_intersection([V|Vs], XDom) :-
        (   fd_get(V, VDom, _) ->
            domains_intersection_(VDom, XDom, empty),
            all_empty_intersection(Vs, XDom)
        ;   all_empty_intersection(Vs, XDom)
        ).

outof_reducer(Left, Right, Var) :-
        (   fd_get(Var, Dom, _) ->
            append(Left, Right, Others),
            domain_num_elements(Dom, N),
            num_subsets(Others, Dom, 0, Num, NonSubs),
            (   n(Num) cis_geq N -> false
            ;   n(Num) cis N - n(1) ->
                reduce_from_others(NonSubs, Dom)
            ;   true
            )
        ;   %\+ list_contains(Right, Var),
            %\+ list_contains(Left, Var)
            true
        ).

reduce_from_others([], _).
reduce_from_others([X|Xs], Dom) :-
        (   fd_get(X, XDom, XPs) ->
            domain_subtract(XDom, Dom, NXDom),
            fd_put(X, NXDom, XPs)
        ;   true
        ),
        reduce_from_others(Xs, Dom).

num_subsets([], _Dom, Num, Num, []).
num_subsets([S|Ss], Dom, Num0, Num, NonSubs) :-
        (   fd_get(S, SDom, _) ->
            (   domain_subdomain(Dom, SDom) ->
                Num1 is Num0 + 1,
                num_subsets(Ss, Dom, Num1, Num, NonSubs)
            ;   NonSubs = [S|Rest],
                num_subsets(Ss, Dom, Num0, Num, Rest)
            )
        ;   num_subsets(Ss, Dom, Num0, Num, NonSubs)
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  serialized(+Starts, +Durations)
%
%   Describes a set of non-overlapping tasks.
%   Starts = [S_1,...,S_n], is a list of variables or integers,
%   Durations = [D_1,...,D_n] is a list of non-negative integers.
%   Constrains Starts and Durations to denote a set of
%   non-overlapping tasks, i.e.: S_i + D_i =< S_j or S_j + D_j =<
%   S_i for all 1 =< i < j =< n. Example:
%
%   ==
%   ?- length(Vs, 3),
%      Vs ins 0..3,
%      serialized(Vs, [1,2,3]),
%      label(Vs).
%   Vs = [0, 1, 3] ;
%   Vs = [2, 0, 3] ;
%   false.
%   ==
%
%  @see Dorndorf et al. 2000, "Constraint Propagation Techniques for the
%       Disjunctive Scheduling Problem"

serialized(Starts, Durations) :-
        must_be(list(integer), Durations),
        pairs_keys_values(SDs, Starts, Durations),
        put_attr(Orig, clpfd_original, serialized(Starts, Durations)),
        serialize(SDs, Orig).

serialize([], _).
serialize([S-D|SDs], Orig) :-
        D >= 0,
        serialize(SDs, S, D, Orig),
        serialize(SDs, Orig).

serialize([], _, _, _).
serialize([S-D|Rest], S0, D0, Orig) :-
        D >= 0,
        propagator_init_trigger([S0,S], pserialized(S,D,S0,D0,Orig)),
        serialize(Rest, S0, D0, Orig).

% consistency check / propagation
% Currently implements 2-b-consistency

earliest_start_time(Start, EST) :-
        (   fd_get(Start, D, _) ->
            domain_infimum(D, EST)
        ;   EST = n(Start)
        ).

latest_start_time(Start, LST) :-
        (   fd_get(Start, D, _) ->
            domain_supremum(D, LST)
        ;   LST = n(Start)
        ).

serialize_lower_upper(S_I, D_I, S_J, D_J, MState) :-
        (   var(S_I) ->
            serialize_lower_bound(S_I, D_I, S_J, D_J, MState),
            (   var(S_I) -> serialize_upper_bound(S_I, D_I, S_J, D_J, MState)
            ;   true
            )
        ;   true
        ).

serialize_lower_bound(I, D_I, J, D_J, MState) :-
        fd_get(I, DomI, Ps),
        (   domain_infimum(DomI, n(EST_I)),
            latest_start_time(J, n(LST_J)),
            EST_I + D_I > LST_J,
            earliest_start_time(J, n(EST_J)) ->
            (   nonvar(J) -> kill(MState)
            ;   true
            ),
            EST is EST_J+D_J,
            domain_remove_smaller_than(DomI, EST, DomI1),
            fd_put(I, DomI1, Ps)
        ;   true
        ).

serialize_upper_bound(I, D_I, J, D_J, MState) :-
        fd_get(I, DomI, Ps),
        (   domain_supremum(DomI, n(LST_I)),
            earliest_start_time(J, n(EST_J)),
            EST_J + D_J > LST_I,
            latest_start_time(J, n(LST_J)) ->
            (   nonvar(J) -> kill(MState)
            ;   true
            ),
            LST is LST_J-D_I,
            domain_remove_greater_than(DomI, LST, DomI1),
            fd_put(I, DomI1, Ps)
        ;   true
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%    element(?N, +Vs, ?V)
%
%     The N-th element of the list of finite domain variables Vs is V.
%     Analogous to nth1/3.

element(N, Is, V) :-
        must_be(list, Is),
        length(Is, L),
        N in 1..L,
        element_(Is, 1, N, V),
        propagator_init_trigger([N|Is], pelement(N,Is,V)).

element_domain(V, VD) :-
        (   fd_get(V, VD, _) -> true
        ;   VD = from_to(n(V), n(V))
        ).

element_([], _, _, _).
element_([I|Is], N0, N, V) :-
        ?(I) #\= ?(V) #==> ?(N) #\= N0,
        N1 is N0 + 1,
        element_(Is, N1, N, V).

integers_remaining([], _, _, D, D).
integers_remaining([V|Vs], N0, Dom, D0, D) :-
        (   domain_contains(Dom, N0) ->
            element_domain(V, VD),
            domains_union(D0, VD, D1)
        ;   D1 = D0
        ),
        N1 is N0 + 1,
        integers_remaining(Vs, N1, Dom, D1, D).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%    global_cardinality(+Vs, +Pairs)
%
%     Global      Cardinality      constraint.        Equivalent      to
%     global_cardinality(Vs, Pairs, []). Example:
%
%     ==
%     ?- Vs = [_,_,_], global_cardinality(Vs, [1-2,3-_]), label(Vs).
%     Vs = [1, 1, 3] ;
%     Vs = [1, 3, 1] ;
%     Vs = [3, 1, 1].
%     ==

global_cardinality(Xs, Pairs) :- global_cardinality(Xs, Pairs, []).

%%    global_cardinality(+Vs, +Pairs, +Options)
%
%     Global Cardinality constraint. Vs  is  a   list  of  finite domain
%     variables, Pairs is a list  of  Key-Num   pairs,  where  Key is an
%     integer and Num is a finite  domain variable. The constraint holds
%     iff each V in Vs is equal to   some key, and for each Key-Num pair
%     in Pairs, the number of occurrences of   Key in Vs is Num. Options
%     is a list of options. Supported options are:
%
%     * consistency(value)
%     A weaker form of consistency is used.
%
%     * cost(Cost, Matrix)
%     Matrix is a list of rows, one for each variable, in the order
%     they occur in Vs. Each of these rows is a list of integers, one
%     for each key, in the order these keys occur in Pairs. When
%     variable v_i is assigned the value of key k_j, then the
%     associated cost is Matrix_{ij}. Cost is the sum of all costs.

global_cardinality(Xs, Pairs, Options) :-
        must_be(list(list), [Xs,Pairs,Options]),
        maplist(fd_variable, Xs),
        maplist(gcc_pair, Pairs),
        pairs_keys_values(Pairs, Keys, Nums),
        (   sort(Keys, Keys1), same_length(Keys, Keys1) -> true
        ;   domain_error(gcc_unique_key_pairs, Pairs)
        ),
        length(Xs, L),
        Nums ins 0..L,
        list_to_drep(Keys, Drep),
        Xs ins Drep,
        gcc_pairs(Pairs, Xs, Pairs1),
        % pgcc_check must be installed before triggering other
        % propagators
        propagator_init_trigger(Xs, pgcc_check(Pairs1)),
        propagator_init_trigger(Nums, pgcc_check_single(Pairs1)),
        (   member(OD, Options), OD == consistency(value) -> true
        ;   propagator_init_trigger(Nums, pgcc_single(Xs, Pairs1)),
            propagator_init_trigger(Xs, pgcc(Xs, Pairs, Pairs1))
        ),
        (   member(OC, Options), functor(OC, cost, 2) ->
            OC = cost(Cost, Matrix),
            must_be(list(list(integer)), Matrix),
            maplist(keys_costs(Keys), Xs, Matrix, Costs),
            sum(Costs, #=, Cost)
        ;   true
        ).

keys_costs(Keys, X, Row, C) :-
        element(N, Keys, X),
        element(N, Row, C).

gcc_pair(Pair) :-
        (   Pair = Key-Val ->
            must_be(integer, Key),
            fd_variable(Val)
        ;   domain_error(gcc_pair, Pair)
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   For each Key-Num0 pair, we introduce an auxiliary variable Num and
   attach the following attributes to it:

   clpfd_gcc_num: equal Num0, the user-visible counter variable
   clpfd_gcc_vs: the remaining variables in the constraint that can be
   equal Key.
   clpfd_gcc_occurred: stores how often Key already occurred in vs.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

gcc_pairs([], _, []).
gcc_pairs([Key-Num0|KNs], Vs, [Key-Num|Rest]) :-
        put_attr(Num, clpfd_gcc_num, Num0),
        put_attr(Num, clpfd_gcc_vs, Vs),
        put_attr(Num, clpfd_gcc_occurred, 0),
        gcc_pairs(KNs, Vs, Rest).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    J.-C. Régin: "Generalized Arc Consistency for Global Cardinality
    Constraint", AAAI-96 Portland, OR, USA, pp 209--215, 1996
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

gcc_global(Vs, KNs) :-
        gcc_check(KNs),
        % reach fix-point: all elements of clpfd_gcc_vs must be variables
        do_queue,
        with_local_attributes(Vs, [edges,parent,index],
              (gcc_arcs(KNs, S, Vals),
               variables_with_num_occurrences(Vs, VNs),
               maplist(target_to_v(T), VNs),
               (   get_attr(S, edges, Es) ->
                   put_attr(S, parent, none), % Mark S as seen to avoid going back to S.
                   feasible_flow(Es, S, T), % First construct a feasible flow (if any)
                   maximum_flow(S, T),      % only then, maximize it.
                   gcc_consistent(T),
                   scc(Vals, gcc_successors),
                   phrase(gcc_goals(Vals), Gs)
               ;   Gs = [] )), Gs),
        disable_queue,
        maplist(call, Gs),
        enable_queue.

gcc_consistent(T) :-
        get_attr(T, edges, Es),
        maplist(saturated_arc, Es).

saturated_arc(arc_from(_,U,_,Flow)) :- get_attr(Flow, flow, U).

gcc_goals([]) --> [].
gcc_goals([Val|Vals]) -->
        { get_attr(Val, edges, Es) },
        gcc_edges_goals(Es, Val),
        gcc_goals(Vals).

gcc_edges_goals([], _) --> [].
gcc_edges_goals([E|Es], Val) -->
        gcc_edge_goal(E, Val),
        gcc_edges_goals(Es, Val).

gcc_edge_goal(arc_from(_,_,_,_), _) --> [].
gcc_edge_goal(arc_to(_,_,V,F), Val) -->
        (   { get_attr(F, flow, 0),
              get_attr(V, lowlink, L1),
              get_attr(Val, lowlink, L2),
              L1 =\= L2,
              get_attr(Val, value, Value) } ->
            [neq_num(V, Value)]
        ;   []
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Like in all_distinct/1, first use breadth-first search, then
   construct an augmenting path in reverse.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

maximum_flow(S, T) :-
        (   gcc_augmenting_path([[S]], Levels, T) ->
            phrase(augmenting_path(S, T), Path),
            Path = [augment(_,First,_)|Rest],
            path_minimum(Rest, First, Min),
            maplist(gcc_augment(Min), Path),
            maplist(maplist(clear_parent), Levels),
            maximum_flow(S, T)
        ;   true
        ).

feasible_flow([], _, _).
feasible_flow([A|As], S, T) :-
        make_arc_feasible(A, S, T),
        feasible_flow(As, S, T).

make_arc_feasible(A, S, T) :-
        A = arc_to(L,_,V,F),
        get_attr(F, flow, Flow),
        (   Flow >= L -> true
        ;   Diff is L - Flow,
            put_attr(V, parent, S-augment(F,Diff,+)),
            gcc_augmenting_path([[V]], Levels, T),
            phrase(augmenting_path(S, T), Path),
            path_minimum(Path, Diff, Min),
            maplist(gcc_augment(Min), Path),
            maplist(maplist(clear_parent), Levels),
            make_arc_feasible(A, S, T)
        ).

gcc_augmenting_path(Levels0, Levels, T) :-
        Levels0 = [Vs|_],
        Levels1 = [Tos|Levels0],
        phrase(gcc_reachables(Vs), Tos),
        Tos = [_|_],
        (   member(To, Tos), To == T -> Levels = Levels1
        ;   gcc_augmenting_path(Levels1, Levels, T)
        ).

gcc_reachables([])     --> [].
gcc_reachables([V|Vs]) -->
        { get_attr(V, edges, Es) },
        gcc_reachables_(Es, V),
        gcc_reachables(Vs).

gcc_reachables_([], _)     --> [].
gcc_reachables_([E|Es], V) -->
        gcc_reachable(E, V),
        gcc_reachables_(Es, V).

gcc_reachable(arc_from(_,_,V,F), P) -->
        (   { \+ get_attr(V, parent, _),
              get_attr(F, flow, Flow),
              Flow > 0 } ->
            { put_attr(V, parent, P-augment(F,Flow,-)) },
            [V]
        ;   []
        ).
gcc_reachable(arc_to(_L,U,V,F), P) -->
        (   { \+ get_attr(V, parent, _),
              get_attr(F, flow, Flow),
              Flow < U } ->
            { Diff is U - Flow,
              put_attr(V, parent, P-augment(F,Diff,+)) },
            [V]
        ;   []
        ).


path_minimum([], Min, Min).
path_minimum([augment(_,A,_)|As], Min0, Min) :-
        Min1 is min(Min0,A),
        path_minimum(As, Min1, Min).

gcc_augment(Min, augment(F,_,Sign)) :-
        get_attr(F, flow, Flow0),
        gcc_flow_(Sign, Flow0, Min, Flow),
        put_attr(F, flow, Flow).

gcc_flow_(+, F0, A, F) :- F is F0 + A.
gcc_flow_(-, F0, A, F) :- F is F0 - A.

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Build value network for global cardinality constraint.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

gcc_arcs([], _, []).
gcc_arcs([Key-Num0|KNs], S, Vals) :-
        (   get_attr(Num0, clpfd_gcc_vs, Vs) ->
            get_attr(Num0, clpfd_gcc_num, Num),
            get_attr(Num0, clpfd_gcc_occurred, Occ),
            (   nonvar(Num) -> U is Num - Occ, U = L
            ;   fd_get(Num, _, n(L0), n(U0), _),
                L is L0 - Occ, U is U0 - Occ
            ),
            put_attr(Val, value, Key),
            Vals = [Val|Rest],
            put_attr(F, flow, 0),
            append_edge(S, edges, arc_to(L, U, Val, F)),
            put_attr(Val, edges, [arc_from(L, U, S, F)]),
            variables_with_num_occurrences(Vs, VNs),
            maplist(val_to_v(Val), VNs)
        ;   Vals = Rest
        ),
        gcc_arcs(KNs, S, Rest).

variables_with_num_occurrences(Vs0, VNs) :-
        include(var, Vs0, Vs1),
        msort(Vs1, Vs),
        (   Vs == [] -> VNs = []
        ;   Vs = [V|Rest],
            variables_with_num_occurrences(Rest, V, 1, VNs)
        ).

variables_with_num_occurrences([], Prev, Count, [Prev-Count]).
variables_with_num_occurrences([V|Vs], Prev, Count0, VNs) :-
        (   V == Prev ->
            Count1 is Count0 + 1,
            variables_with_num_occurrences(Vs, Prev, Count1, VNs)
        ;   VNs = [Prev-Count0|Rest],
            variables_with_num_occurrences(Vs, V, 1, Rest)
        ).


target_to_v(T, V-Count) :-
        put_attr(F, flow, 0),
        append_edge(V, edges, arc_to(0, Count, T, F)),
        append_edge(T, edges, arc_from(0, Count, V, F)).

val_to_v(Val, V-Count) :-
        put_attr(F, flow, 0),
        append_edge(V, edges, arc_from(0, Count, Val, F)),
        append_edge(Val, edges, arc_to(0, Count, V, F)).


gcc_successors(V, Tos) :-
        get_attr(V, edges, Tos0),
        phrase(gcc_successors_(Tos0), Tos).

gcc_successors_([])     --> [].
gcc_successors_([E|Es]) --> gcc_succ_edge(E), gcc_successors_(Es).

gcc_succ_edge(arc_to(_,U,V,F)) -->
        (   { get_attr(F, flow, Flow),
              Flow < U } -> [V]
        ;   []
        ).
gcc_succ_edge(arc_from(_,_,V,F)) -->
        (   { get_attr(F, flow, Flow),
              Flow > 0 } -> [V]
        ;   []
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Simple consistency check, run before global propagation.
   Importantly, it removes all ground values from clpfd_gcc_vs.

   The pgcc_check/1 propagator in itself suffices to ensure
   consistency.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

gcc_check(Pairs) :-
        disable_queue,
        gcc_check_(Pairs),
        enable_queue.

gcc_done(Num) :-
        del_attr(Num, clpfd_gcc_vs),
        del_attr(Num, clpfd_gcc_num),
        del_attr(Num, clpfd_gcc_occurred).

gcc_check_([]).
gcc_check_([Key-Num0|KNs]) :-
        (   get_attr(Num0, clpfd_gcc_vs, Vs) ->
            get_attr(Num0, clpfd_gcc_num, Num),
            get_attr(Num0, clpfd_gcc_occurred, Occ0),
            vs_key_min_others(Vs, Key, 0, Min, Os),
            put_attr(Num0, clpfd_gcc_vs, Os),
            put_attr(Num0, clpfd_gcc_occurred, Occ1),
            Occ1 is Occ0 + Min,
            geq(Num, Occ1),
            % The queue is disabled for efficiency here in any case.
            % If it were enabled, make sure to retain the invariant
            % that gcc_global is never triggered during an
            % inconsistent state (after gcc_done/1 but before all
            % relevant constraints are posted).
            (   Occ1 == Num -> all_neq(Os, Key), gcc_done(Num0)
            ;   Os == [] -> gcc_done(Num0), Num = Occ1
            ;   length(Os, L),
                Max is Occ1 + L,
                geq(Max, Num),
                (   nonvar(Num) -> Diff is Num - Occ1
                ;   fd_get(Num, ND, _),
                    domain_infimum(ND, n(NInf)),
                    Diff is NInf - Occ1
                ),
                L >= Diff,
                (   L =:= Diff ->
                    Num is Occ1 + Diff,
                    maplist(=(Key), Os),
                    gcc_done(Num0)
                ;   true
                )
            )
        ;   true
        ),
        gcc_check_(KNs).

vs_key_min_others([], _, Min, Min, []).
vs_key_min_others([V|Vs], Key, Min0, Min, Others) :-
        (   fd_get(V, VD, _) ->
            (   domain_contains(VD, Key) ->
                Others = [V|Rest],
                vs_key_min_others(Vs, Key, Min0, Min, Rest)
            ;   vs_key_min_others(Vs, Key, Min0, Min, Others)
            )
        ;   (   V =:= Key ->
                Min1 is Min0 + 1,
                vs_key_min_others(Vs, Key, Min1, Min, Others)
            ;   vs_key_min_others(Vs, Key, Min0, Min, Others)
            )
        ).

all_neq([], _).
all_neq([X|Xs], C) :-
        neq_num(X, C),
        all_neq(Xs, C).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%    circuit(+Vs)
%
%     True iff the list Vs of finite domain variables induces a
%     Hamiltonian circuit. The k-th element of Vs denotes the
%     successor of node k. Node indexing starts with 1. Examples:
%
%     ==
%     ?- length(Vs, _), circuit(Vs), label(Vs).
%     Vs = [] ;
%     Vs = [1] ;
%     Vs = [2, 1] ;
%     Vs = [2, 3, 1] ;
%     Vs = [3, 1, 2] ;
%     Vs = [2, 3, 4, 1] .
%     ==

circuit(Vs) :-
        must_be(list, Vs),
        maplist(fd_variable, Vs),
        length(Vs, L),
        Vs ins 1..L,
        (   L =:= 1 -> true
        ;   neq_index(Vs, 1),
            make_propagator(pcircuit(Vs), Prop),
            distinct_attach(Vs, Prop, []),
            trigger_once(Prop)
        ).

neq_index([], _).
neq_index([X|Xs], N) :-
        neq_num(X, N),
        N1 is N + 1,
        neq_index(Xs, N1).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Necessary condition for existence of a Hamiltonian circuit: The
   graph has a single strongly connected component. If the list is
   ground, the condition is also sufficient.

   Ts are used as temporary variables to attach attributes:

   lowlink, index: used for SCC
   [arc_to(V)]: possible successors
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

propagate_circuit(Vs) :-
        with_local_attributes([], [],
            (same_length(Vs, Ts),
             circuit_graph(Vs, Ts, Ts),
             scc(Ts, circuit_successors),
             maplist(single_component, Ts)), _).

single_component(V) :- get_attr(V, lowlink, 0).

circuit_graph([], _, _).
circuit_graph([V|Vs], Ts0, [T|Ts]) :-
        (   nonvar(V) -> Ns = [V]
        ;   fd_get(V, Dom, _),
            domain_to_list(Dom, Ns)
        ),
        phrase(circuit_edges(Ns, Ts0), Es),
        put_attr(T, edges, Es),
        circuit_graph(Vs, Ts0, Ts).

circuit_edges([], _) --> [].
circuit_edges([N|Ns], Ts) -->
        { nth1(N, Ts, T) },
        [arc_to(T)],
        circuit_edges(Ns, Ts).

circuit_successors(V, Tos) :-
        get_attr(V, edges, Tos0),
        maplist(arg(1), Tos0, Tos).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% cumulative(+Tasks)
%
%  Equivalent to cumulative(Tasks, [limit(1)]).

cumulative(Tasks) :- cumulative(Tasks, [limit(1)]).

%% cumulative(+Tasks, +Options)
%
%  Schedule with a limited resource. Tasks is a list of tasks, each of
%  the form task(S_i, D_i, E_i, C_i, T_i). S_i denotes the start time,
%  D_i the positive duration, E_i the end time, C_i the non-negative
%  resource consumption, and T_i the task identifier. Each of these
%  arguments must be a finite domain variable with bounded domain, or
%  an integer. The constraint holds iff at each time slot during the
%  start and end of each task, the total resource consumption of all
%  tasks running at that time does not exceed the global resource
%  limit (which is 1 by default). Options is a list of options.
%  Currently, the only supported option is:
%
%    * limit(L)
%      The integer L is the global resource limit.
%
%  For example, given the following predicate that relates three tasks
%  of durations 2 and 3 to a list containing their starting times:
%
%  ==
%  tasks_starts(Tasks, [S1,S2,S3]) :-
%          Tasks = [task(S1,3,_,1,_),
%                   task(S2,2,_,1,_),
%                   task(S3,2,_,1,_)].
%  ==
%
%  We can use cumulative/2 as follows, and obtain a schedule:
%
%  ==
%  ?- tasks_starts(Tasks, Starts), Starts ins 0..10,
%     cumulative(Tasks, [limit(2)]), label(Starts).
%  Tasks = [task(0, 3, 3, 1, _G36), task(0, 2, 2, 1, _G45), ...],
%  Starts = [0, 0, 2] .
%  ==

cumulative(Tasks, Options) :-
        must_be(list(list), [Tasks,Options]),
        (   Options = [] -> L = 1
        ;   Options = [limit(L)] -> must_be(integer, L)
        ;   domain_error(cumulative_options_empty_or_limit, Options)
        ),
        (   Tasks = [] -> true
        ;   maplist(task_bs, Tasks, Bss),
            maplist(arg(1), Tasks, Starts),
            maplist(fd_inf, Starts, MinStarts),
            maplist(arg(3), Tasks, Ends),
            maplist(fd_sup, Ends, MaxEnds),
            min_list(MinStarts, Start),
            max_list(MaxEnds, End),
            min_end_time(Tasks, L, Start),
            resource_limit(Start, End, Tasks, Bss, L)
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Naive lower bound for global end time, assuming no gaps.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

min_end_time(Tasks, Limit, Start) :-
        maplist(task_duration_consumption, Tasks, Ds, Cs),
        maplist(area, Ds, Cs, As),
        sum(As, #=, ?(Area)),
        ?(MinTime) #= (Area + Limit - 1) // Limit,
        tasks_all_done(Tasks, DoneTime),
        DoneTime #>= Start + MinTime.

task_duration_consumption(task(_,D,_,C,_), D, C).

area(X, Y, Area) :- ?(Area) #= ?(X) * ?(Y).

tasks_all_done(Tasks, End) :-
        maplist(task_end, Tasks, [End0|Es]),
        foldl(max_, Es, End0, End).

max_(E, M0, M) :- ?(M) #= max(E, M0).

task_end(task(_,_,End,_,_), ?(End)).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   All time slots must respect the resource limit.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

resource_limit(T, T, _, _, _) :- !.
resource_limit(T0, T, Tasks, Bss, L) :-
        maplist(contribution_at(T0), Tasks, Bss, Cs),
        sum(Cs, #=<, L),
        T1 is T0 + 1,
        resource_limit(T1, T, Tasks, Bss, L).

task_bs(Task, InfStart-Bs) :-
        Task = task(Start,D,End,_,_Id),
        ?(D) #> 0,
        ?(End) #= ?(Start) + ?(D),
        maplist(finite_domain, [End,Start,D]),
        fd_inf(Start, InfStart),
        fd_sup(End, SupEnd),
        L is SupEnd - InfStart,
        length(Bs, L),
        task_running(Bs, Start, End, InfStart).

task_running([], _, _, _).
task_running([B|Bs], Start, End, T) :-
        ((T #>= Start) #/\ (T #< End)) #<==> ?(B),
        T1 is T + 1,
        task_running(Bs, Start, End, T1).

contribution_at(T, Task, Offset-Bs, Contribution) :-
        Task = task(Start,_,End,C,_),
        ?(C) #>= 0,
        fd_inf(Start, InfStart),
        fd_sup(End, SupEnd),
        (   T < InfStart -> Contribution = 0
        ;   T >= SupEnd -> Contribution = 0
        ;   Index is T - Offset,
            nth0(Index, Bs, B),
            ?(Contribution) #= B*C
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% disjoint2(+Rectangles)
%
%  True iff Rectangles are not overlapping. Rectangles is a list of
%  terms of the form F(X_i, W_i, Y_i, H_i), where F is any functor,
%  and the arguments are finite domain variables or integers that
%  denote, respectively, the X coordinate, width, Y coordinate and
%  height of each rectangle.

disjoint2(Rs0) :-
        must_be(list, Rs0),
        maplist(=.., Rs0, Rs),
        non_overlapping(Rs).

non_overlapping([]).
non_overlapping([R|Rs]) :-
        maplist(non_overlapping_(R), Rs),
        non_overlapping(Rs).

non_overlapping_(A, B) :-
        a_not_in_b(A, B),
        a_not_in_b(B, A).

a_not_in_b([_,AX,AW,AY,AH], [_,BX,BW,BY,BH]) :-
        ?(AX) #=< ?(BX) #/\ ?(BX) #< ?(AX) + ?(AW) #==>
                   ?(AY) + ?(AH) #=< ?(BY) #\/ ?(BY) + ?(BH) #=< ?(AY),
        ?(AY) #=< ?(BY) #/\ ?(BY) #< ?(AY) + ?(AH) #==>
                   ?(AX) + ?(AW) #=< ?(BX) #\/ ?(BX) + ?(BW) #=< ?(AX).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% automaton(+Signature, +Nodes, +Arcs)
%
%  Describes a list of finite domain variables with a finite
%  automaton. Equivalent to automaton(_, _, Signature, Nodes, Arcs,
%  [], [], _), a common use case of automaton/8. In the following
%  example, a list of binary finite domain variables is constrained to
%  contain at least two consecutive ones:
%
%  ==
%  :- use_module(library(clpfd)).
%
%  two_consecutive_ones(Vs) :-
%          automaton(Vs, [source(a),sink(c)],
%                    [arc(a,0,a), arc(a,1,b),
%                     arc(b,0,a), arc(b,1,c),
%                     arc(c,0,c), arc(c,1,c)]).
%  ==
%
%  Example query:
%
%  ==
%  ?- length(Vs, 3), two_consecutive_ones(Vs), label(Vs).
%  Vs = [0, 1, 1] ;
%  Vs = [1, 1, 0] ;
%  Vs = [1, 1, 1].
%  ==

automaton(Sigs, Ns, As) :- automaton(_, _, Sigs, Ns, As, [], [], _).


%% automaton(?Sequence, ?Template, +Signature, +Nodes, +Arcs, +Counters, +Initials, ?Finals)
%
%  Describes a list of finite domain variables with a finite
%  automaton. True iff the finite automaton induced by Nodes and Arcs
%  (extended with Counters) accepts Signature. Sequence is a list of
%  terms, all of the same shape. Additional constraints must link
%  Sequence to Signature, if necessary. Nodes is a list of
%  source(Node) and sink(Node) terms. Arcs is a list of
%  arc(Node,Integer,Node) and arc(Node,Integer,Node,Exprs) terms that
%  denote the automaton's transitions. Each node is represented by an
%  arbitrary term. Transitions that are not mentioned go to an
%  implicit failure node. `Exprs` is a list of arithmetic expressions,
%  of the same length as Counters. In each expression, variables
%  occurring in Counters correspond to old counter values, and
%  variables occurring in Template correspond to the current element
%  of Sequence. When a transition containing expressions is taken,
%  each counter is updated as stated by the result of the
%  corresponding arithmetic expression. By default, counters remain
%  unchanged. Counters is a list of variables that must not occur
%  anywhere outside of the constraint goal. Initials is a list of the
%  same length as Counters. Counter arithmetic on the transitions
%  relates the counter values in Initials to Finals.
%
%  The following example is taken from Beldiceanu, Carlsson, Debruyne
%  and Petit: "Reformulation of Global Constraints Based on
%  Constraints Checkers", Constraints 10(4), pp 339-362 (2005). It
%  relates a sequence of integers and finite domain variables to its
%  number of inflexions, which are switches between strictly ascending
%  and strictly descending subsequences:
%
%  ==
%  :- use_module(library(clpfd)).
%
%  sequence_inflexions(Vs, N) :-
%          variables_signature(Vs, Sigs),
%          automaton(_, _, Sigs,
%                    [source(s),sink(i),sink(j),sink(s)],
%                    [arc(s,0,s), arc(s,1,j), arc(s,2,i),
%                     arc(i,0,i), arc(i,1,j,[C+1]), arc(i,2,i),
%                     arc(j,0,j), arc(j,1,j),
%                     arc(j,2,i,[C+1])],
%                    [C], [0], [N]).
%
%  variables_signature([], []).
%  variables_signature([V|Vs], Sigs) :-
%          variables_signature_(Vs, V, Sigs).
%
%  variables_signature_([], _, []).
%  variables_signature_([V|Vs], Prev, [S|Sigs]) :-
%          V #= Prev #<==> S #= 0,
%          Prev #< V #<==> S #= 1,
%          Prev #> V #<==> S #= 2,
%          variables_signature_(Vs, V, Sigs).
%  ==
%
%  Example queries:
%
%  ==
%  ?- sequence_inflexions([1,2,3,3,2,1,3,0], N).
%  N = 3.
%
%  ?- length(Ls, 5), Ls ins 0..1,
%     sequence_inflexions(Ls, 3), label(Ls).
%  Ls = [0, 1, 0, 1, 0] ;
%  Ls = [1, 0, 1, 0, 1].
%  ==

template_var_path(V, Var, []) :- var(V), !, V == Var.
template_var_path(T, Var, [N|Ns]) :-
        arg(N, T, Arg),
        template_var_path(Arg, Var, Ns).

path_term_variable([], V, V).
path_term_variable([P|Ps], T, V) :-
        arg(P, T, Arg),
        path_term_variable(Ps, Arg, V).

initial_expr(_, []-1).

automaton(Seqs, Template, Sigs, Ns, As0, Cs, Is, Fs) :-
        must_be(list(list), [Sigs,Ns,As0,Cs,Is]),
        (   var(Seqs) ->
            (   current_prolog_flag(clpfd_monotonic, true) ->
                instantiation_error(Seqs)
            ;   Seqs = Sigs
            )
        ;   must_be(list, Seqs)
        ),
        maplist(monotonic, Cs, CsM),
        maplist(arc_normalized(CsM), As0, As),
        include_args1(sink, Ns, Sinks),
        include_args1(source, Ns, Sources),
        maplist(initial_expr, Cs, Exprs0),
        phrase((arcs_relation(As, Relation),
                nodes_nums(Sinks, SinkNums0),
                nodes_nums(Sources, SourceNums0)),
               [s([]-0, Exprs0)], [s(_,Exprs1)]),
        maplist(expr0_expr, Exprs1, Exprs),
        phrase(transitions(Seqs, Template, Sigs, Start, End, Exprs, Cs, Is, Fs), Tuples),
        list_to_drep(SourceNums0, SourceDrep),
        Start in SourceDrep,
        list_to_drep(SinkNums0, SinkDrep),
        End in SinkDrep,
        tuples_in(Tuples, Relation).

expr0_expr(Es0-_, Es) :-
        pairs_keys(Es0, Es1),
        reverse(Es1, Es).

transitions([], _, [], S, S, _, _, Cs, Cs) --> [].
transitions([Seq|Seqs], Template, [Sig|Sigs], S0, S, Exprs, Counters, Cs0, Cs) -->
        [[S0,Sig,S1|Is]],
        { phrase(exprs_next(Exprs, Is, Cs1), [s(Seq,Template,Counters,Cs0)], _) },
        transitions(Seqs, Template, Sigs, S1, S, Exprs, Counters, Cs1, Cs).

exprs_next([], [], []) --> [].
exprs_next([Es|Ess], [I|Is], [C|Cs]) -->
        exprs_values(Es, Vs),
        { element(I, Vs, C) },
        exprs_next(Ess, Is, Cs).

exprs_values([], []) --> [].
exprs_values([E0|Es], [V|Vs]) -->
        { term_variables(E0, EVs0),
          copy_term(E0, E),
          term_variables(E, EVs),
          ?(V) #= E },
        match_variables(EVs0, EVs),
        exprs_values(Es, Vs).

match_variables([], _) --> [].
match_variables([V0|Vs0], [V|Vs]) -->
        state(s(Seq,Template,Counters,Cs0)),
        { (   template_var_path(Template, V0, Ps) ->
              path_term_variable(Ps, Seq, V)
          ;   template_var_path(Counters, V0, Ps) ->
              path_term_variable(Ps, Cs0, V)
          ;   domain_error(variable_from_template_or_counters, V0)
          ) },
        match_variables(Vs0, Vs).

nodes_nums([], []) --> [].
nodes_nums([Node|Nodes], [Num|Nums]) -->
        node_num(Node, Num),
        nodes_nums(Nodes, Nums).

arcs_relation([], []) --> [].
arcs_relation([arc(S0,L,S1,Es)|As], [[From,L,To|Ns]|Rs]) -->
        node_num(S0, From),
        node_num(S1, To),
        state(s(Nodes, Exprs0), s(Nodes, Exprs)),
        { exprs_nums(Es, Ns, Exprs0, Exprs) },
        arcs_relation(As, Rs).

exprs_nums([], [], [], []).
exprs_nums([E|Es], [N|Ns], [Ex0-C0|Exs0], [Ex-C|Exs]) :-
        (   member(Exp-N, Ex0), Exp == E -> C = C0, Ex = Ex0
        ;   N = C0, C is C0 + 1, Ex = [E-C0|Ex0]
        ),
        exprs_nums(Es, Ns, Exs0, Exs).

node_num(Node, Num) -->
        state(s(Nodes0-C0, Exprs), s(Nodes-C, Exprs)),
        { (   member(N-Num, Nodes0), N == Node -> C = C0, Nodes = Nodes0
          ;   Num = C0, C is C0 + 1, Nodes = [Node-C0|Nodes0]
          )
        }.

include_args1(Goal, Ls0, As) :-
        include(Goal, Ls0, Ls),
        maplist(arg(1), Ls, As).

source(source(_)).

sink(sink(_)).

monotonic(Var, ?(Var)).

arc_normalized(Cs, Arc0, Arc) :- arc_normalized_(Arc0, Cs, Arc).

arc_normalized_(arc(S0,L,S,Cs), _, arc(S0,L,S,Cs)).
arc_normalized_(arc(S0,L,S), Cs, arc(S0,L,S,Cs)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% transpose(+Matrix, ?Transpose)
%
%  Transpose a list of lists of the same length. Example:
%
%  ==
%  ?- transpose([[1,2,3],[4,5,6],[7,8,9]], Ts).
%  Ts = [[1, 4, 7], [2, 5, 8], [3, 6, 9]].
%  ==
%
%  This predicate is useful in many constraint programs. Consider for
%  instance Sudoku:
%
%  ==
%  :- use_module(library(clpfd)).
%
%  sudoku(Rows) :-
%          length(Rows, 9), maplist(length_list(9), Rows),
%          append(Rows, Vs), Vs ins 1..9,
%          maplist(all_distinct, Rows),
%          transpose(Rows, Columns),
%          maplist(all_distinct, Columns),
%          Rows = [A,B,C,D,E,F,G,H,I],
%          blocks(A, B, C), blocks(D, E, F), blocks(G, H, I).
%
%  length_list(L, Ls) :- length(Ls, L).
%
%  blocks([], [], []).
%  blocks([A,B,C|Bs1], [D,E,F|Bs2], [G,H,I|Bs3]) :-
%          all_distinct([A,B,C,D,E,F,G,H,I]),
%          blocks(Bs1, Bs2, Bs3).
%
%  problem(1, [[_,_,_,_,_,_,_,_,_],
%              [_,_,_,_,_,3,_,8,5],
%              [_,_,1,_,2,_,_,_,_],
%              [_,_,_,5,_,7,_,_,_],
%              [_,_,4,_,_,_,1,_,_],
%              [_,9,_,_,_,_,_,_,_],
%              [5,_,_,_,_,_,_,7,3],
%              [_,_,2,_,1,_,_,_,_],
%              [_,_,_,_,4,_,_,_,9]]).
%  ==
%
%  Sample query:
%
%  ==
%  ?- problem(1, Rows), sudoku(Rows), maplist(writeln, Rows).
%  [9,8,7,6,5,4,3,2,1]
%  [2,4,6,1,7,3,9,8,5]
%  [3,5,1,9,2,8,7,4,6]
%  [1,2,8,5,3,7,6,9,4]
%  [6,3,4,8,9,2,1,5,7]
%  [7,9,5,4,6,1,8,3,2]
%  [5,1,9,2,8,6,4,7,3]
%  [4,7,2,3,1,9,5,6,8]
%  [8,6,3,7,4,5,2,1,9]
%  Rows = [[9, 8, 7, 6, 5, 4, 3, 2|...], ... , [...|...]].
%  ==

transpose(Ls, Ts) :-
        must_be(list(list), Ls),
        lists_transpose(Ls, Ts).

lists_transpose([], []).
lists_transpose([L|Ls], Ts) :- foldl(transpose_, L, Ts, [L|Ls], _).

transpose_(_, Fs, Lists0, Lists) :-
        maplist(list_first_rest, Lists0, Fs, Lists).

list_first_rest([L|Ls], L, Ls).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% zcompare(?Order, ?A, ?B)
%
% Analogous to compare/3, with finite domain variables A and B.
% Example:
%
% ==
% :- use_module(library(clpfd)).
%
% n_factorial(N, F) :-
%         zcompare(C, N, 0),
%         n_factorial_(C, N, F).
%
% n_factorial_(=, _, 1).
% n_factorial_(>, N, F) :-
%         F #= F0*N, N1 #= N - 1,
%         n_factorial(N1, F0).
% ==
%
% This version is deterministic if the first argument is instantiated:
%
% ==
% ?- n_factorial(30, F).
% F = 265252859812191058636308480000000.
% ==

zcompare(Order, A, B) :-
        (   nonvar(Order) ->
            zcompare_(Order, A, B)
        ;   freeze(Order, zcompare_(Order, A, B)),
            fd_variable(A),
            fd_variable(B),
            propagator_init_trigger([A,B], pzcompare(Order, A, B))
        ).

zcompare_(=, A, B) :- ?(A) #= ?(B).
zcompare_(<, A, B) :- ?(A) #< ?(B).
zcompare_(>, A, B) :- ?(A) #> ?(B).

%% chain(+Zs, +Relation)
%
% Zs form a chain with respect to Relation. Zs is a list of finite
% domain variables that are a chain with respect to the partial order
% Relation, in the order they appear in the list. Relation must be #=,
% #=<, #>=, #< or #>. For example:
%
% ==
% ?- chain([X,Y,Z], #>=).
% X#>=Y,
% Y#>=Z.
% ==

chain(Zs, Relation) :-
        must_be(list, Zs),
        maplist(fd_variable, Zs),
        must_be(ground, Relation),
        (   chain_relation(Relation) -> true
        ;   domain_error(chain_relation, Relation)
        ),
        chain_(Zs, Relation).

chain_([], _).
chain_([X|Xs], Relation) :- foldl(chain(Relation), Xs, X, _).

chain_relation(#=).
chain_relation(#<).
chain_relation(#=<).
chain_relation(#>).
chain_relation(#>=).

chain(Relation, X, Prev, X) :- call(Relation, ?(Prev), ?(X)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Reflection predicates
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

%% fd_var(+Var)
%
%  True iff Var is a CLP(FD) variable.

fd_var(X) :- get_attr(X, clpfd, _).

%% fd_inf(+Var, -Inf)
%
%  Inf is the infimum of the current domain of Var.

fd_inf(X, Inf) :-
        (   fd_get(X, XD, _) ->
            domain_infimum(XD, Inf0),
            bound_portray(Inf0, Inf)
        ;   must_be(integer, X),
            Inf = X
        ).

%% fd_sup(+Var, -Sup)
%
%  Sup is the supremum of the current domain of Var.

fd_sup(X, Sup) :-
        (   fd_get(X, XD, _) ->
            domain_supremum(XD, Sup0),
            bound_portray(Sup0, Sup)
        ;   must_be(integer, X),
            Sup = X
        ).

%% fd_size(+Var, -Size)
%
%  Size is the number of elements of the current domain of Var, or the
%  atom *sup* if the domain is unbounded.

fd_size(X, S) :-
        (   fd_get(X, XD, _) ->
            domain_num_elements(XD, S0),
            bound_portray(S0, S)
        ;   must_be(integer, X),
            S = 1
        ).

%% fd_dom(+Var, -Dom)
%
%  Dom is the current domain (see in/2) of Var. This predicate is
%  useful if you want to reason about domains. It is _not_ needed if
%  you only want to display remaining domains; instead, separate your
%  model from the search part and let the toplevel display this
%  information via residual goals.
%
%  For example, to implement a custom labeling strategy, you may need
%  to inspect the current domain of a finite domain variable. With the
%  following code, you can convert a _finite_ domain to a list of
%  integers:
%
%  ==
%  dom_integers(D, Is) :- phrase(dom_integers_(D), Is).
%
%  dom_integers_(I)      --> { integer(I) }, [I].
%  dom_integers_(L..U)   --> { numlist(L, U, Is) }, Is.
%  dom_integers_(D1\/D2) --> dom_integers_(D1), dom_integers_(D2).
%  ==
%
%  Example:
%
%  ==
%  ?- X in 1..5, X #\= 4, fd_dom(X, D), dom_integers(D, Is).
%  D = 1..3\/5,
%  Is = [1,2,3,5],
%  X in 1..3\/5.
%  ==

fd_dom(X, Drep) :-
        (   fd_get(X, XD, _) ->
            domain_to_drep(XD, Drep)
        ;   must_be(integer, X),
            Drep = X..X
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Entailment detection. Subject to change.

   Currently, Goals entail E if posting ({#\ E} U Goals), then
   labeling all variables, fails. E must be reifiable. Examples:

   %?- clpfd:goals_entail([X#>2], X #> 3).
   %@ false.

   %?- clpfd:goals_entail([X#>1, X#<3], X #= 2).
   %@ true.

   %?- clpfd:goals_entail([X#=Y+1], X #= Y+1).
   %@ ERROR: Arguments are not sufficiently instantiated
   %@    Exception: (15) throw(error(instantiation_error, _G2680)) ?

   %?- clpfd:goals_entail([[X,Y] ins 0..10, X#=Y+1], X #= Y+1).
   %@ true.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

goals_entail(Goals, E) :-
        must_be(list, Goals),
        \+ (   maplist(call, Goals), #\ E,
               term_variables(Goals-E, Vs),
               label(Vs)
           ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Unification hook and constraint projection
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

attr_unify_hook(clpfd_attr(_,_,_,Dom,Ps), Other) :-
        (   nonvar(Other) ->
            (   integer(Other) -> true
            ;   type_error(integer, Other)
            ),
            domain_contains(Dom, Other),
            trigger_props(Ps),
            do_queue
        ;   fd_get(Other, OD, OPs),
            domains_intersection(OD, Dom, Dom1),
            append_propagators(Ps, OPs, Ps1),
            fd_put(Other, Dom1, Ps1),
            trigger_props(Ps1),
            do_queue
        ).

append_propagators(fd_props(Gs0,Bs0,Os0), fd_props(Gs1,Bs1,Os1), fd_props(Gs,Bs,Os)) :-
        maplist(append, [Gs0,Bs0,Os0], [Gs1,Bs1,Os1], [Gs,Bs,Os]).

bound_portray(inf, inf).
bound_portray(sup, sup).
bound_portray(n(N), N).

list_to_drep(List, Drep) :-
        list_to_domain(List, Dom),
        domain_to_drep(Dom, Drep).

domain_to_drep(Dom, Drep) :-
        domain_intervals(Dom, [A0-B0|Rest]),
        bound_portray(A0, A),
        bound_portray(B0, B),
        (   A == B -> Drep0 = A
        ;   Drep0 = A..B
        ),
        intervals_to_drep(Rest, Drep0, Drep).

intervals_to_drep([], Drep, Drep).
intervals_to_drep([A0-B0|Rest], Drep0, Drep) :-
        bound_portray(A0, A),
        bound_portray(B0, B),
        (   A == B -> D1 = A
        ;   D1 = A..B
        ),
        intervals_to_drep(Rest, Drep0 \/ D1, Drep).

attribute_goals(X) -->
        % { get_attr(X, clpfd, Attr), format("A: ~w\n", [Attr]) },
        { get_attr(X, clpfd, clpfd_attr(_,_,_,Dom,fd_props(Gs,Bs,Os))),
          append(Gs, Bs, Ps0),
          append(Ps0, Os, Ps),
          domain_to_drep(Dom, Drep) },
        (   { default_domain(Dom), \+ all_dead_(Ps) } -> []
        ;   [clpfd:(X in Drep)]
        ),
        attributes_goals(Ps).

clpfd_aux:attribute_goals(_) --> [].
clpfd_aux:attr_unify_hook(_,_) :- false.

clpfd_gcc_vs:attribute_goals(_) --> [].
clpfd_gcc_vs:attr_unify_hook(_,_) :- false.

clpfd_gcc_num:attribute_goals(_) --> [].
clpfd_gcc_num:attr_unify_hook(_,_) :- false.

clpfd_gcc_occurred:attribute_goals(_) --> [].
clpfd_gcc_occurred:attr_unify_hook(_,_) :- false.

clpfd_relation:attribute_goals(_) --> [].
clpfd_relation:attr_unify_hook(_,_) :- false.

clpfd_original:attribute_goals(_) --> [].
clpfd_original:attr_unify_hook(_,_) :- false.

attributes_goals([]) --> [].
attributes_goals([propagator(P, State)|As]) -->
        (   { ground(State) } -> []
        ;   { phrase(attribute_goal_(P), Gs) } ->
            { del_attr(State, clpfd_aux), State = processed,
              (   current_prolog_flag(clpfd_monotonic, true) ->
                  maplist(unwrap_with(bare_integer), Gs, Gs1)
              ;   maplist(unwrap_with(=), Gs, Gs1)
              ),
              maplist(with_clpfd, Gs1, Gs2) },
            list(Gs2)
        ;   [P] % possibly user-defined constraint
        ),
        attributes_goals(As).

with_clpfd(G, clpfd:G).

unwrap_with(_, V, V)           :- var(V), !.
unwrap_with(Goal, ?(V0), V)    :- !, call(Goal, V0, V).
unwrap_with(Goal, Term0, Term) :-
        Term0 =.. [F|Args0],
        maplist(unwrap_with(Goal), Args0, Args),
        Term =.. [F|Args].

bare_integer(V0, V)    :- ( integer(V0) -> V = V0 ; V = ?(V0) ).

attribute_goal_(presidual(Goal))       --> [Goal].
attribute_goal_(pgeq(A,B))             --> [?(A) #>= ?(B)].
attribute_goal_(pplus(X,Y,Z))          --> [?(X) + ?(Y) #= ?(Z)].
attribute_goal_(pneq(A,B))             --> [?(A) #\= ?(B)].
attribute_goal_(ptimes(X,Y,Z))         --> [?(X) * ?(Y) #= ?(Z)].
attribute_goal_(absdiff_neq(X,Y,C))    --> [abs(?(X) - ?(Y)) #\= C].
attribute_goal_(absdiff_geq(X,Y,C))    --> [abs(?(X) - ?(Y)) #>= C].
attribute_goal_(x_neq_y_plus_z(X,Y,Z)) --> [?(X) #\= ?(Y) + ?(Z)].
attribute_goal_(x_leq_y_plus_c(X,Y,C)) --> [?(X) #=< ?(Y) + C].
attribute_goal_(ptzdiv(X,Y,Z))         --> [?(X) // ?(Y) #= ?(Z)].
attribute_goal_(pdiv(X,Y,Z))           --> [?(X) div ?(Y) #= ?(Z)].
attribute_goal_(prdiv(X,Y,Z))          --> [?(X) rdiv ?(Y) #= ?(Z)].
attribute_goal_(pexp(X,Y,Z))           --> [?(X) ^ ?(Y) #= ?(Z)].
attribute_goal_(pabs(X,Y))             --> [?(Y) #= abs(?(X))].
attribute_goal_(pmod(X,M,K))           --> [?(X) mod ?(M) #= ?(K)].
attribute_goal_(prem(X,Y,Z))           --> [?(X) rem ?(Y) #= ?(Z)].
attribute_goal_(pmax(X,Y,Z))           --> [?(Z) #= max(?(X),?(Y))].
attribute_goal_(pmin(X,Y,Z))           --> [?(Z) #= min(?(X),?(Y))].
attribute_goal_(scalar_product_neq(Cs,Vs,C)) -->
        [Left #\= Right],
        { scalar_product_left_right([-1|Cs], [C|Vs], Left, Right) }.
attribute_goal_(scalar_product_eq(Cs,Vs,C)) -->
        [Left #= Right],
        { scalar_product_left_right([-1|Cs], [C|Vs], Left, Right) }.
attribute_goal_(scalar_product_leq(Cs,Vs,C)) -->
        [Left #=< Right],
        { scalar_product_left_right([-1|Cs], [C|Vs], Left, Right) }.
attribute_goal_(pdifferent(_,_,_,O))    --> original_goal(O).
attribute_goal_(weak_distinct(_,_,_,O)) --> original_goal(O).
attribute_goal_(pdistinct(Vs))          --> [all_distinct(Vs)].
attribute_goal_(pexclude(_,_,_))  --> [].
attribute_goal_(pelement(N,Is,V)) --> [element(N, Is, V)].
attribute_goal_(pgcc(Vs, Pairs, _))   --> [global_cardinality(Vs, Pairs)].
attribute_goal_(pgcc_single(_,_))     --> [].
attribute_goal_(pgcc_check_single(_)) --> [].
attribute_goal_(pgcc_check(_))        --> [].
attribute_goal_(pcircuit(Vs))       --> [circuit(Vs)].
attribute_goal_(pserialized(_,_,_,_,O)) --> original_goal(O).
attribute_goal_(rel_tuple(R, Tuple)) -->
        { get_attr(R, clpfd_relation, Rel) },
        [tuples_in([Tuple], Rel)].
attribute_goal_(pzcompare(O,A,B)) --> [zcompare(O,A,B)].
% reified constraints
attribute_goal_(reified_in(V, D, B)) -->
        [V in Drep #<==> ?(B)],
        { domain_to_drep(D, Drep) }.
attribute_goal_(reified_tuple_in(Tuple, R, B)) -->
        { get_attr(R, clpfd_relation, Rel) },
        [tuples_in([Tuple], Rel) #<==> ?(B)].
attribute_goal_(kill_reified_tuples(_,_,_)) --> [].
attribute_goal_(tuples_not_in(_,_,_)) --> [].
attribute_goal_(reified_fd(V,B)) --> [finite_domain(V) #<==> ?(B)].
attribute_goal_(pskeleton(X,Y,D,_,Z,F)) -->
        { Prop =.. [F,X,Y,Z],
          phrase(attribute_goal_(Prop), Goals), list_goal(Goals, Goal) },
        [?(D) #= 1 #==> Goal, ?(Y) #\= 0 #==> ?(D) #= 1].
attribute_goal_(reified_neq(DX,X,DY,Y,_,B)) -->
        conjunction(DX, DY, ?(X) #\= ?(Y), B).
attribute_goal_(reified_eq(DX,X,DY,Y,_,B))  -->
        conjunction(DX, DY, ?(X) #= ?(Y), B).
attribute_goal_(reified_geq(DX,X,DY,Y,_,B)) -->
        conjunction(DX, DY, ?(X) #>= ?(Y), B).
attribute_goal_(reified_and(X,_,Y,_,B))    --> [?(X) #/\ ?(Y) #<==> ?(B)].
attribute_goal_(reified_or(X, _, Y, _, B)) --> [?(X) #\/ ?(Y) #<==> ?(B)].
attribute_goal_(reified_not(X, Y))         --> [#\ ?(X) #<==> ?(Y)].
attribute_goal_(pimpl(X, Y, _))            --> [?(X) #==> ?(Y)].

conjunction(A, B, G, D) -->
        (   { A == 1, B == 1 } -> [G #<==> ?(D)]
        ;   { A == 1 } -> [(?(B) #/\ G) #<==> ?(D)]
        ;   { B == 1 } -> [(?(A) #/\ G) #<==> ?(D)]
        ;   [(?(A) #/\ ?(B) #/\ G) #<==> ?(D)]
        ).

original_goal(V) -->
        (   { get_attr(V, clpfd_original, Goal) } ->
            { del_attr(V, clpfd_original) },
            [Goal]
        ;   []
        ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Projection of scalar product.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

scalar_product_left_right(Cs, Vs, Left, Right) :-
        pairs_keys_values(Pairs0, Cs, Vs),
        partition(ground, Pairs0, Grounds, Pairs),
        maplist(pair_product, Grounds, Prods),
        sum_list(Prods, Const),
        NConst is -Const,
        partition(compare_coeff0, Pairs, Negatives, _, Positives),
        maplist(negate_coeff, Negatives, Rights),
        scalar_plusterm(Rights, Right0),
        scalar_plusterm(Positives, Left0),
        (   Const =:= 0 -> Left = Left0, Right = Right0
        ;   Right0 == 0 -> Left = Left0, Right = NConst
        ;   Left0 == 0 ->  Left = Const, Right = Right0
        ;   (   Const < 0 ->
                Left = Left0,       Right = Right0+NConst
            ;   Left = Left0+Const, Right = Right0
            )
        ).

negate_coeff(A0-B, A-B) :- A is -A0.

pair_product(A-B, Prod) :- Prod is A*B.

compare_coeff0(Coeff-_, Compare) :- compare(Compare, Coeff, 0).

scalar_plusterm([], 0).
scalar_plusterm([CV|CVs], T) :-
        coeff_var_term(CV, T0),
        foldl(plusterm_, CVs, T0, T).

plusterm_(CV, T0, T0+T) :- coeff_var_term(CV, T).

coeff_var_term(C-V, T) :- ( C =:= 1 -> T = ?(V) ; T = C * ?(V) ).

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Global variables
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

make_clpfd_var('$clpfd_queue') :-
        make_queue.
make_clpfd_var('$clpfd_current_propagator') :-
        nb_setval('$clpfd_current_propagator', []).
make_clpfd_var('$clpfd_queue_status') :-
        nb_setval('$clpfd_queue_status', enabled).

:- multifile user:exception/3.

user:exception(undefined_global_variable, Name, retry) :-
        make_clpfd_var(Name), !.

warn_if_bounded_arithmetic :-
        (   current_prolog_flag(bounded, true) ->
            print_message(warning, clpfd(bounded))
        ;   true
        ).

:- initialization(warn_if_bounded_arithmetic).


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Messages
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/*
:- multifile prolog:message//1.

prolog:message(clpfd(bounded)) -->
        ['Using CLP(FD) with bounded arithmetic may yield wrong results.'-[]].


		 /*******************************
		 *	      SANDBOX		*
		 *******************************/

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The clpfd library cannot  be   analysed  completely by library(sandbox).
However, the API does not provide any  meta predicates. It provides some
unification hooks, but put_attr/3 does not  allow injecting in arbitrary
attributes.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

:- multifile
	sandbox:safe_primitive/1.

safe_api(Name/Arity, sandbox:safe_primitive(clpfd:Head)) :-
	functor(Head, Name, Arity).

term_expansion(safe_api, Clauses) :-
	module_property(clpfd, exports(API)),
	maplist(safe_api, API, Clauses).

safe_api.
% Support clpfd goal expansion.
sandbox:safe_primitive(clpfd:clpfd_equal(_,_)).
sandbox:safe_primitive(clpfd:clpfd_geq(_,_)).
sandbox:safe_primitive(clpfd:clpfd_in(_,_)).
*/
