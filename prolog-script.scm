(use-modules (system base language))
(use-modules (system repl repl))
(use-modules (system repl common))
(use-modules (ice-9 readline))
(activate-readline)

(load  (string-append (getenv "HOME") "/.guile"))

(define f0 (string-append (getenv "HOME") "/guile-prolog-scratch"))
(define f1 (string-append (getenv "HOME") "/guile-prolog-scratch/language"))
(define f2 (string-append (getenv "HOME")
			  "/guile-prolog-scratch/language/prolog"))
(define f3 (string-append (getenv "HOME")
			  "/guile-prolog-scratch/language/prolog/modules"))

(if (not (file-exists? f0))
    (mkdir f0))
(if (not (file-exists? f1))
    (mkdir f2))
(if (not (file-exists? f2))
    (mkdir f2))
(if (not (file-exists? f3))
    (mkdir f3))

(set! %load-path (cons f0 %load-path))

(let*  ((str (string-append "f() :- " (cadr (program-arguments))))
	(str (format #f 
"
((@ (guile) begin)
 (compile-prolog-string \"~a\") 
 (prolog-run 1 () (f)))" str)))
  (set-current-module
   ((language-make-default-environment (lookup-language 'prolog))))
  (let ((lang (lookup-language 'prolog)))
    ;(current-language lang)
    ((@ (guile) eval-string) str ((@ (guile) current-module)))))


