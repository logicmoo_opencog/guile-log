 (define-module (logic guile-log kanren)
  #:use-module (logic guile-log umatch)
  #:use-module ((logic guile-log) #:renamer 
		(lambda (x) (if  (eq? x '_) '_var_ x)))
  #:export(succeed fail sfail all all! all!! if-only exists
                   any fails succeeds any-interleave if-some 
                   all-interleave relation fact lift-to-relations 
                   let-gls == query solve solution project project/no-check 
                   predicate *equal? var? _ let-lv reify partially-eval-sgl
                   extend-relation extend-relation-with-recur-limit
                   intersect-relation any-union trace-vars))


(define (mk-names x)
  (syntax-case x ()
    ((x . l) #`(#,(datum->syntax #'x (gensym "a"))
                #,@(mk-names #'l)))
    (()      #'())))

(define-syntax mk
  (syntax-rules ()
    ((_ name gl-name)       
     (define-syntax name
       (lambda (x)
         (syntax-case x ()
           ((_ a (... ...))
            #'(lambda (s p cc)            
                (<with-guile-log> (s p cc)
                  (gl-name (a) (... ...)))))))))))

(define partially-eval-sgl
  (lambda x 
    (error "partialle eval sgl is not supported in guile-log-kanren")))
            
(mk all            <and>)
(mk all!           <and!>)
(mk all!!          <and!!>)
(mk any            <or>)
(mk any-interleave <or-i>)
(mk any-union      <or-union>)
(mk all-interleave <and-i>)
(mk if-only        <if>)
(mk if-some        <if-some>)
(mk fails          <not>)
(mk succeeds       <succeds>)


(define succeed (lambda (s p cc) (cc s p)))  ; eta-reduced
(define fail    (lambda (s p cc) (p)))
(define sfail    fail)
(define var?     gp-logical-var?)

(define (tr a b s)
  (define m (make-hash-table))
  (define i 0)
  (let lp ((x (gp->scm b s)))
    (cond
     ((pair? x)
      (cons (lp (car x)) (lp (cdr x))))

     ((vector? x)
      (list->vector
       (lp (vector->list x))))

     ((gp-var? x s)
      (let ((v (hash-ref m x #f)))
        (if v
            v
            (let ((v (string->symbol 
                      (format #f "~a~a" a i))))
              (hash-set! m x v)
              (set! i (+ i 1))
              v))))
     (else
      x))))


(define reify (lambda (x)
                (tr 'v. x (fluid-ref *current-stack*))))

(define-syntax let-lv
  (syntax-rules ()
    ((_ (id ...) body)
     (let ((id (gp-var! (fluid-ref *current-stack*))) ...) body))))

(define-syntax extend-relation-with-recur-limit
  (syntax-rules ()
    ((_ limit ids rel ...)     
     (let ((*counter* (gp-var! (fluid-ref *current-stack*))))
       (lambda ids
         (let ((gl (any (rel . ids) ...)))
           (lambda (s p cc)
             (if (gp-var? *counter* s)
                 (let ((s (gp-set! *counter* 0 s)))
                   (gl s p cc))
                 (let ((cnt (gp-lookup *counter* s)))
                   (if (<= limit cnt)
                       (sfail s p cc)
                       (let ((s (gp-set! *counter* (+ cnt 1) s)))
                         (gl s p cc))))))))))))

; The anonymous variable
(define-syntax _
  (lambda (x)
    (syntax-case x ()
      ((_ . l) #'(error "_ cannot be infunction possition"))
      (_       #'(gp-var! (fluid-ref *current-stack*))))))

;;The following is a direct translation of kanren.ss    
(define-syntax id-memv??
  (syntax-rules ()
    ((id-memv?? form (id ...) kt kf)
      (let-syntax
	((test
	   (syntax-rules (id ...)
	     ((test id _kt _kf) _kt) ...
	     ((test otherwise _kt _kf) _kf))))
	(test form kt kf)))))

; Test cases
; (id-memv?? x (a b c) #t #f)
; (id-memv?? a (a b c) 'OK #f)
; (id-memv?? () (a b c) #t #f)
; (id-memv?? (x ...) (a b c) #t #f)
; (id-memv?? "abc" (a b c) #t #f)
; (id-memv?? x () #t #f)
; (let ((x 1))
;   (id-memv?? x (a b x) 'OK #f))
; (let ((x 1))
;   (id-memv?? x (a x b) 'OK #f))
; (let ((x 1))
;   (id-memv?? x (x a b) 'OK #f))


(define-syntax let*-and
  (syntax-rules ()
    ((_ false-exp () body0 body1 ...) (begin body0 body1 ...))
    ((_ false-exp ((var0 exp0) (var1 exp1) ...) body0 body1 ...)
     (let ((var0 exp0))
       (if var0
         (let*-and false-exp ((var1 exp1) ...) body0 body1 ...)
         false-exp)))))

; Relations...........................

; The current incremented unification of argument passing is quite similar to
; the compilation of argument unifications in WAM.

; relation (VAR ...) (to-show TERM ...) [GL]
; Defines a relation of arity (length '(TERM ...)) with an optional body
; GL. VAR ... are logical variables that are local to the relation, i.e.,
; appear in TERM or GL. It's better to list as VAR ... only logical
; variables that appear in TERM. Variables that appear only in GL should
; be introduced with exists. That makes their existential quantification
; clearer. Variables that appear in TERM are universally quantified.
;
; relation (head-let TERM ...) [GL]
; See relation-head-let below.
;
; relation (ANNOT-VAR ...) (to-show TERM ...) [GL]  (see remark below!)
; where ANNOT-VAR is either a simple VAR or (once VAR)
; where 'once' is a distingushed symbol. The latter form introduces
; a once-var, aka linear variable. A linear variable appears only once in
; TERM ... and only at the top level (that is, one and only one TERM
; in the to-show pattern contains ONCE-VAR, and that term is ONCE-VAR
; itself). In addition, ONCE-VAR must appear at most once in the body GL.
; (Of course, then ONCE-VAR could be _, instead.)
; If these conditions are satisfied, we can replace a logical variable
; ONCE-VAR with a regular Scheme variable.

; Alternative notation:
; (relation (a c) (to-show term1 (once c) term2) body)
; Makes it easier to deal with. But it is unsatisfactory:
; to-show becomes a binding form...
;
; When ``compiling'' a relation, we now look through the
; (to-show ...) pattern for a top-level occurrence of the logical variable
; introduced by the relation. For example:
;	(relation (x y) (to-show `(,x . ,y) x) body)
; we notice that the logical variable 'x' occurs at the top-level. Normally we
; compile the relation like that into the following
;    (lambda (g1 g2)
;      (exists (x y)
;        (lambda@ (subst)
; 	 (let*-and (fail subst) ((subst (unify g1 `(,x . ,y)  subst))
; 			         (subst (unify g2 x subst)))
; 	     (@ body subst)))))
;
; However, that we may permute the order of 'unify g...' clauses
; to read
;    (lambda (g1 g2)
;      (exists (x y)
;        (lambda@ (subst)
; 	 (let*-and (fail subst) ((subst (unify x g2 subst))
; 			         (subst (unify g1 `(,x . ,y)  subst))
; 			         )
; 	     (@ body subst)))))
;
; We may further note that according to the properties of the unifier
; (see below), (unify x g2 subst) must always succeed, 
; because x is a fresh variable.
; Furthermore, the result of (unify x g2 subst) is either subst itself,
; or subst with the binding of x. Therefore, we can check if
; the binding at the top of (unify x g2 subst) is the binding to x. If
; so, we can remove the binding and convert the variable x from being logical
; to being lexical. Thus, we compile the relation as
;
;    (lambda (g1 g2)
;      (exists (x y)
;        (lambda@ (subst)
; 	 (let* ((subst (unify-free/any x g2 subst))
; 	        (fast-path? (and (pair? subst)
; 			         (eq? x (commitment->var (car subst)))))
; 	        (x (if fast-path? (commitment->term (car subst)) x))
; 	        (subst (if fast-path? (cdr subst) subst)))
; 	 (let*-and sfail ((subst (unify g1 `(,x . ,y)  subst))
; 			 )
; 	     (@ body subst))))))
;
; The benefit of that approach is that we limit the growth of subst and avoid
; keeping commitments that had to be garbage-collected later.


; A macro-expand-time memv function for identifiers
;	id-memv?? FORM (ID ...) KT KF
; FORM is an arbitrary form or datum, ID is an identifier.
; The macro expands into KT if FORM is an identifier that occurs
; in the list of identifiers supplied by the second argument.
; Otherwise, id-memv?? expands to KF.
; All the identifiers in (ID ...) must be unique.
; Two identifiers match if both refer to the same binding occurrence, or
; (both are undefined and have the same spelling).


(define-syntax relation
  (syntax-rules (to-show head-let once _)
    ((_ (head-let head-term ...) gl)
     (relation-head-let (head-term ...) gl))
    ((_ (head-let head-term ...))	; not particularly useful without body
     (relation-head-let (head-term ...)))
    ((_ () (to-show term ...) gl)	; pattern with no vars _is_ linear
     (relation-head-let (`,term ...) gl))
    ((_ () (to-show term ...))		; the same without body: not too useful
     (relation-head-let (`,term ...)))
    ((_ (ex-id ...) (to-show term ...) gl)  ; body present
     (relation "a" () () (ex-id ...) (term ...) gl))
    ((_ (ex-id ...) (to-show term ...))      ; no body
     (relation "a" () () (ex-id ...) (term ...)))
    ; process the list of variables and handle annotations
    ((_ "a" vars once-vars ((once id) . ids) terms . gl)
     (relation "a" vars (id . once-vars) ids terms . gl))
    ((_ "a" vars once-vars (id . ids) terms . gl)
     (relation "a" (id . vars) once-vars ids terms . gl))
    ((_ "a" vars once-vars () terms . gl)
     (relation "g" vars once-vars () () () (subst) terms . gl))
    ; generating temp names for each term in the head
    ; don't generate if the term is a variable that occurs in
    ; once-vars
    ; For _ variables in the pattern, generate unique names for the lambda
    ; parameters, and forget them
    ; also, note and keep track of the first occurrence of a term
    ; that is just a var (bare-var) 
    ((_ "g" vars once-vars (gs ...) gunis bvars bvar-cl (_ . terms) . gl)
     (relation "g" vars once-vars (gs ... anon) gunis
       bvars bvar-cl terms . gl))
    ((_ "g" vars once-vars (gs ...) gunis bvars (subst . cls)
           (term . terms) . gl)
     (id-memv?? term once-vars 
       ; success continuation: term is a once-var
       (relation "g" vars once-vars (gs ... term) gunis bvars (subst . cls)
	 terms . gl)
       ; failure continuation: term is not a once-var
       (id-memv?? term vars
	 ; term is a bare var
	 (id-memv?? term bvars
	   ; term is a bare var, but we have seen it already: general case
	   (relation "g" vars once-vars  (gs ... g) ((g . term) . gunis) 
	     bvars (subst . cls) terms . gl)
	   ; term is a bare var, and we have not seen it
	   (relation "g" vars once-vars (gs ... g) gunis
	     (term . bvars)
	     (subst
              (subst (gp-unify! term g subst))
              . cls)
	     terms . gl))
	 ; term is not a bare var
	 (relation "g" vars once-vars  (gs ... g) ((g . term) . gunis) 
	   bvars (subst . cls) terms . gl))))
    ((_ "g" vars once-vars gs gunis bvars bvar-cl () . gl)
     (relation "f" vars once-vars gs gunis bvar-cl . gl))

    ; Final: writing the code
    ((_ "f" vars () () () (subst) gl)   ; no arguments (no head-tests)
      (lambda ()
	(exists vars gl)))
                                    ; no tests but pure binding
    ((_ "f" (ex-id ...) once-vars (g ...) () (subst) gl)
     (lambda (g ...)
       (exists (ex-id ...) gl)))
				    ; the most general
    ((_ "f" (ex-id ...) once-vars (g ...) ((gv . term) ...) 
       (s let*-clause ...) gl) 
     (lambda (g ...)
       (exists (ex-id ...)
         (lambda (s p cc)
           (let* (let*-clause ...)
             (let*-and (p) ((s (gp-unify! gv term s)) ...)
                       (gl s p cc)))))))))

; relation-head-let (head-term ...) gl 
; A simpler, and more efficient kind of relation. The simplicity comes
; from a simpler pattern at the head of the relation. The pattern must
; be linear and shallow with respect to introduced variables.  The gl
; is optional (although omitting it doesn't make much sense in
; practice) There are two kinds of head-terms.  One kind is an
; identifier. This identifier is taken to be a logical identifier, to
; be unified with the corresponding actual argument.  Each logical
; identifier must occur exactly once.  Another kind of a head-terms is
; anything else. That anything else may be a constant, a scheme
; variable, or a complex term that may even include logical variables
; such as _ -- but not logical variables defined in the same head-let
; pattern.  To make the task of distinguishing logical identifiers
; from anything else easier, we require that anything else of a sort
; of a manifest constant be explicitly quoted or quasiquoted. It would
; be OK to add `, to each 'anything else' term.
;
; Examples:
; (relation-head-let (x y z) (foo x y z))
; Here x y and z are logical variables.
; (relation-head-let (x y '7) (foo x y))
; Here we used a manifest constant that must be quoted
; (relation-head-let (x y `(1 2 . ,_)) (foo x y))
; We used a quasi-quoted constant with an anonymous variable.
; (let ((z `(1 2 . ,_))) (relation-head-let (x y `,z) (foo x y))
; The same as above, but using a lexical Scheme variable.
; The binding procedure is justified by Proposition 9 of
; the Properties of Substitutions.
;
; 'head-let' is an example of "compile-time" simplifications. 
; For example, we distinguish constants in the term head at
; "compile time" and so we re-arrange the argument-passing
; unifications to handle the constants first.
; The test for the anonymous variable (eq? gvv0 _) below
; is an example of a global simplification with a run-time
; test. A compiler could have inferred the result of the test -- but only
; upon the global analysis of all the clauses.
; Replacing a logical variable with an ordinary variable, which does 
; not have to be pruned, is equivalent to the use of temporary and
; unsafe variables in WAM.


(define-syntax relation-head-let
  (syntax-rules ()
    ((_ (head-term ...) . gls)
     (relation-head-let "g" () (head-term ...) (head-term ...) . gls))
    ; generate names of formal parameters
    ((_ "g" (genvar ...) ((head-term . tail-term) . ht-rest)
       head-terms . gls)
     (relation-head-let "g" (genvar ... g) ht-rest head-terms . gls))
    ((_ "g" (genvar ...) (head-term . ht-rest) head-terms . gls)
     (relation-head-let "g" (genvar ... head-term) ht-rest head-terms . gls))
    ((_ "g" genvars  () head-terms . gls)
     (relation-head-let "d" () () genvars head-terms genvars . gls))
    ; partition head-terms into vars and others
    ((_ "d" vars others (gv . gv-rest) ((hth . htt) . ht-rest) gvs . gls)
     (relation-head-let "d" vars ((gv (hth . htt)) . others)
       gv-rest ht-rest gvs . gls))
    ((_ "d" vars others (gv . gv-rest) (htv . ht-rest) gvs . gls)
     (relation-head-let "d" (htv . vars) others
       gv-rest ht-rest gvs . gls))
    ((_ "d" vars others () () gvs . gls)
     (relation-head-let "f" vars others gvs . gls))
 
    ; final generation
    ((_ "f" vars ((gv term) ...) gvs) ; no body
     (lambda gvs
       (lambda (s p cc)
         (let*-and (p) ((s (gp-unify! gv term s)) ...)
                   (succeed s p cc)))))

    ((_ "f" (var0 ...) ((gvo term) ...) gvs gl)
     (lambda gvs
       (lambda (s p cc)
         (let*-and (p) ((s (gp-unify! gvo term s)) ...)
                   (let ((var0 (if (eq? var0 _) 
                                   (gp-var! (fluid-ref *current-stack*))
                                   var0)) ...)
                     (gl s p cc))))))))

(define-syntax fact
  (syntax-rules ()
    ((_ (ex-id ...) term ...)
     (relation (ex-id ...) (to-show term ...) succeed))))

(define-syntax define-rel-lifted-comb
  (syntax-rules ()
    ((_ rel-syntax-name gl-proc-or-syntax)
     (define-syntax rel-syntax-name
       (syntax-rules ()
         ((_ ids . rel-exps)
          (lift-gl-to-rel-aux gl-proc-or-syntax ids () . rel-exps)))))))

(define-syntax lift-gl-to-rel-aux
  (syntax-rules ()
    ((_ gl-handler ids ((g rel-var) ...))
     (let ((g rel-var) ...)
       (lambda ids
         (gl-handler (g . ids) ...))))
    ((_ gl-handler ids (let-pair ...) rel-exp0 rel-exp1 ...)
     (lift-gl-to-rel-aux gl-handler ids 
       (let-pair ... (g rel-exp0)) rel-exp1 ...))))

; The following  goal-to-relations 
; transformers are roughly equivalent. I don't know which is better.
; see examples below.

; (lift-to-relations ids (gl-comb rel rel ...))
(define-syntax lift-to-relations
  (syntax-rules ()
    ((_ ids (gl-comb rel ...))
     (lift-gl-to-rel-aux gl-comb ids () rel ...))))

; (let-gls ids ((name rel) ...) body)
; NB: some macro systems do not like if 'ids' below is replaced by (id ...)
(define-syntax let-gls
  (syntax-rules ()
    ((_ ids ((gl-name rel-exp) ...) body)
     (lambda ids
       (let ((gl-name (rel-exp . ids)) ...)
         body)))))

; Unify lifted to be a binary relation
(define-syntax ==
  (lambda (x)
    (syntax-case x ()
      ((x t u) 
       (or (eq? '_ (syntax->datum #'t))
           (eq? '_ (syntax->datum #'u)))
       #'(lambda (s p cc) (cc s p)))
      ((x t u)
       #'(lambda (s p cc)
           (<with-guile-log> (s p cc)
             (<unify> gp-unify! t u)))))))

;	query (redo-k subst id ...) A SE ... -> result or '()
; The macro 'query' runs the goal A in the empty
; initial substitution, and reifies the resulting
; answer: the substitution and the redo-continuation bound
; to fresh variables with the names supplied by the user.
; The substitution and the redo continuation can then be used
; by Scheme expressions SE ...
; Before running the goal, the macro creates logical variables
; id ... for use in A and SE ...
; If the goal fails, '() is returned and SE ... are not evaluated.
; Note the similarity with shift/reset-based programming
; where the immediate return signifies "failure" and the invocation
; of the continuation a "success"
; Returning '() on failure makes it easy to create the list of answers

(define-syntax query
  (syntax-rules ()
    ((_ (redo-k subst id ...) A SE ...)
      (let-lv (id ...)
	(A (lambda () '())
           (lambda (redo-k subst) SE ...))))))

(define-syntax solve
  (syntax-rules ()
    ((solve n vs g ...) 
     (begin (gp-clear (fluid-ref *current-stack*) )
            (with-fluids
                ((*gp-var-tr* 'v.))
              (<run> n vs (g) ...))))))

(define-syntax solution
  (syntax-rules ()
    ((_ (var0 ...) x)
     (let ((ls (solve 1 (var0 ...) x)))
       (if (null? ls) #f (car ls))))))

(define-syntax predicate
  (syntax-rules ()
    ((_ scheme-expression)
     (if scheme-expression succeed fail))))

(define-syntax *equal?
  (syntax-rules (_)
    ((_ t u)
     (lambda (s p cc)
       (<with-guile-log> (s p cc)
         (<unify> gp-m-unify! t u))))))

(define nonvar!
  (lambda (t)
    (if (var? t)
      (error "Logic variable found at lookup")
      t)))

(define-syntax project
  (syntax-rules ()
    ((_ (var ...) gl)
     (lambda (s p cc)
       (let ((var (nonvar! (gp-lookup var s))) ...)
         (gl s p cc))))))

(define-syntax project/no-check
  (syntax-rules ()
    ((_ (var ...) gl)
     (lambda (s p cc)
       (let ((var (gp-lookup var s)) ...)
         (gl s p cc))))))


	 
(define-syntax define-rel-lifted-comb
  (syntax-rules ()
    ((_ rel-syntax-name gl-proc-or-syntax)
     (define-syntax rel-syntax-name
       (syntax-rules ()
         ((_ ids . rel-exps)
          (lift-gl-to-rel-aux gl-proc-or-syntax ids () . rel-exps)))))))

(define-syntax lift-gl-to-rel-aux
  (syntax-rules ()
    ((_ gl-handler ids ((g rel-var) ...))
     (let ((g rel-var) ...)
       (lambda ids
         (gl-handler (g . ids) ...))))
    ((_ gl-handler ids (let-pair ...) rel-exp0 rel-exp1 ...)
     (lift-gl-to-rel-aux gl-handler ids 
       (let-pair ... (g rel-exp0)) rel-exp1 ...))))

(define-rel-lifted-comb extend-relation    any)
(define-rel-lifted-comb intersect-relation all)

(define-syntax exists
  (syntax-rules ()
    ((_ () gl) gl)
    ((_ (ex-id ...) gl)
     (let-lv (ex-id ...) gl))
    ))

(define-syntax promise-one-answer
  (syntax-rules ()
    ((_ gl) gl)))

(define-syntax trace-vars
  (syntax-rules ()
    ((_ title (var0 ...))
     (promise-one-answer
       (project/no-check (var0 ...)
         (predicate
	   (for-each 
	     (lambda (name val)
	       (format "~a ~a: ~a~%" title name val))
             '(var0 ...) (reify `(,var0 ...)))
           ))))))
