(define-module (logic guile-log db db)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:export
  (
   lock
   unloock
   
   ;; Classes
   DB
   FileDB

   ;; Methods
   init-db db-write-table-raw

   ;; procedures
   db-write-strings
   db-pull-strings

   db-write-modules
   db-pull-modules

   db-write-datas
   db-pull-datas

   db-write-tables
   db-pull-tables

   db-write-tables-vlist
   db-pull-tables-vlist
   
   db-write-vlists
   db-pull-vlists

   db-write-vhashs
   db-pull-vhashs

   db-write-objects
   db-pull-objects

   db-pull-data
   db-write-data
   ))

(define-class DB ()
  ;; Data
  strings
  rstrings  
  string-key
  string-last-key
  
  modules
  rmodules
  module-key
  module-last-key
  
  datas
  rdatas
  data-key
  data-last-key  

  tables
  rtables
  table-key
  table-last-key

  tables-vlist
  rtables-vlist
  table-key-vlist
  table-last-key-vlist
  
  vlists
  rvlists
  vlist-key
  vlist-last-key

  vhashs
  rvhashs
  vhash-key
  vhash-last-key

  objects
  robjects
  object-key
  object-last-key
  stored-objects
  )

(define-method (init-db (db DB) . l)
  (slot-set! db 'lock (lambda () #f))
  (slot-set! db 'unlock (lambda () #f))
  
  (slot-set! db 'strings         (make-hash-table))
  (slot-set! db 'rstrings        (make-hash-table))
  (slot-set! db 'string-key      0)
  (slot-set! db 'string-last-key 0)
  
  (slot-set! db 'modules         (make-hash-table))
  (slot-set! db 'rmodules        (make-hash-table))
  (slot-set! db 'module-key      0)
  (slot-set! db 'module-last-key 0)

  (slot-set! db 'datas           (make-hash-table))
  (slot-set! db 'rdatas          (make-hash-table))
  (slot-set! db 'data-key        0)
  (slot-set! db 'data-last-key   0)  

  (slot-set! db 'tables          (make-hash-table))
  (slot-set! db 'rtables         (make-hash-table))
  (slot-set! db 'table-key       0)
  (slot-set! db 'table-last-key  0)
  
  (slot-set! db 'tables-vlist          (make-hash-table))
  (slot-set! db 'rtables-vlist         (make-hash-table))
  (slot-set! db 'table-key-vlist       0)
  (slot-set! db 'table-last-key-vlist  0)  

  (slot-set! db 'vhashs          (make-hash-table))
  (slot-set! db 'rvhashs         (make-hash-table))
  (slot-set! db 'vhash-key       0)
  (slot-set! db 'vhash-last-key  0)  

  (slot-set! db 'vlists          (make-hash-table))
  (slot-set! db 'rvlists         (make-hash-table))
  (slot-set! db 'vlist-key       0)
  (slot-set! db 'vlist-last-key  0)
  
  (slot-set! db 'objects         (make-hash-table))
  (slot-set! db 'robjects        (make-hash-table))
  (slot-set! db 'stored-objects  (make-hash-table))
  (slot-set! db 'object-key      0)
  (slot-set! db 'object-last-key 0)  
  )
 

(define-class FileDB (DB) dir)
(define-method (init-db (db FileDB) dir)
  (slot-set! db 'dir dir)
  (next-method))

(define-method (db-write-table-raw (db DB) table l)
  (error "db-write-table on a DB is abstract"))

(define (get-path db table)
  (string-append (slot-ref db 'dir) table ".dat"))

(define-method (db-write-table-raw (db FileDB) table l)
  (let ((path (get-path db table)))
    (let ((fd (open-file path "a")))
      (for-each
       (lambda (x) (write x fd))
       l)
      (close fd))))

(define (db-write-strings db l)
  (db-write-table-raw db "string-table" l))

(define-method (db-pull-string db last)
  (error "abstract db in db-pull-string"))

(define (fd-puller path last)
  (catch #t
    (lambda ()
      (let ((fd (open-file path "r")))
	(let lp ((v (read fd)) (l '()))
	  (if (not (eof-object? v))
	      (match v
		((s . k)
		 (if (>= k last)
		     (lp (read fd) (cons (cons k s) l))
		     (lp (read fd) l))))
	      l))))
    (lambda x
      '())))

(define-method (db-pull-strings (db FileDB) last)
  (let ((path (get-path db "string-table")))
    (fd-puller path last)))

(define (db-write-modules db l)
  (db-write-table-raw db "module-table" l))

(define-method (db-pull-modules db last)
  (error "abstract db in db-pull-modules"))

(define-method (db-pull-modules (db FileDB) last)
  (let ((path (get-path db "module-table")))
    (fd-puller path last)))

(define (db-write-datas db l)
  (db-write-table-raw db "data-table" l))

(define-method (db-pull-datas db last)
  (error "abstract db in db-pull-datas"))

(define-method (db-pull-datas (db FileDB) last)
  (let ((path (get-path db "data-table")))
    (fd-puller path last)))

(define (db-write-vlists db l)
  (db-write-table-raw db "vlist-table" l))

(define-method (db-pull-vlists db last)
  (error "abstract db in db-pull-vlists"))

(define-method (db-pull-vlists (db FileDB) last)
  (let ((path (get-path db "vlist-table")))
    (fd-puller path last)))

(define (db-write-tables db l)
  (db-write-table-raw db "table-table" l))

(define-method (db-pull-tables db last)
  (error "abstract db in db-pull-tables"))

(define-method (db-pull-tables (db FileDB) last)
  (let ((path (get-path db "table-table")))
    (fd-puller path last)))

(define (db-write-tables-vlist db l)
  (db-write-table-raw db "table-table-vlist" l))

(define-method (db-pull-tables-vlist db last)
  (error "abstract db in db-pull-tables-vlist"))

(define-method (db-pull-tables-vlist (db FileDB) last)
  (let ((path (get-path db "table-table-vlist")))
    (fd-puller path last)))

(define (db-write-vhashs db l)
  (db-write-table-raw db "vhash-table" l))

(define-method (db-pull-vhashs db last)
  (error "abstract db in db-pull-vhashs"))

(define-method (db-pull-vhashs (db FileDB) last)
  (let ((path (get-path db "vhash-table")))
    (fd-puller path last)))


(define (db-write-objects db l)
  (db-write-table-raw db "objects-table" l))

(define-method (db-pull-objects db last)
  (error "abstract db in db-pull-objects"))

(define-method (db-pull-objects (db FileDB) last)
  (let ((path (get-path db "objects-table")))
    (fd-puller path last)))
