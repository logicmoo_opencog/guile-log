(define-module (logic guile-log prolog dynamic)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log prolog compile)  
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log prolog directives)
  #:use-module ((logic guile-log slask) #:select (once-f maybe-call))
  #:use-module (logic guile-log match)
  #:use-module ((logic guile-log umatch) #:select (gp-cp gp-pair? gp-car gp-cdr gp-m-unify!))
  #:use-module (logic guile-log prolog goal)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog analyze)
  #:use-module (logic guile-log prolog var)
  #:use-module ((logic guile-log prolog util)
                #:select ((member . pr-member)))
  #:use-module (logic guile-log) 	
  #:use-module (ice-9 match)  
  #:re-export (define-dynamic define-dynamic! define-dynamic-f)
  #:export (asserta assertz assertac assertzc assertaf assertzf
		    assertg assertgc assertgf assertg-source
		    clause clausei retract  abolish current_predicate
		    asserta-source  assertz-source assertg-source
		    asserta-source+ assertz-source+ assertg-source+
		    accessify_predicate))

(define-syntax-rule (mk-assert++ asserta <push-dynamic>)
(define asserta 
  (<case-lambda>
   ((Arg ext)
    (<var> (U) (asserta Arg U ext)))
   ((Arg U ext)
    (<<match>> (#:mode - #:name asserta) (Arg)
     ((? <var?>)
      (instantiation_error))
     
     (#((":-" Head Body ))
      (<and>
      (<values> (Head Body) (analyze Head Body))
      (<recur> lp ((Head Head))
      (<<match>> (#:mode - #:name subassert) (Head)
        ((? <var?>)
         (instantiation_error))
        (#((F . A))
         (<cut> 
          (<recur> lp2 ((F (<lookup> F))) 
             (if (not (dynamic? F))
                 (if (procedure? F)
                     (if (object-property F 'prolog-symbol)
                         (lp2 (F))
                         (permission_error modify static_procedure
                                           (vector 
                                            (list divide 
                                                  F
                                                  (length (<scm> A))))))
                     (type_error callable F))
                 (<push-dynamic> (<lookup> F) U
                                 (catch #t
                                   (lambda () 
                                     (mockalambda #f 
						  (<scm> S)
						  (<scm> A)
						  (<scm> Body)))
                                   (lambda x
				     (format
				      #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
                                     (type_error S P CC callable Body))))))))
        (F
         (if (procedure? (<lookup> F))
             (lp (vector (list F)))
             (type_error callable Head)))))))

     (#((F . A))
      (<cut>
       (<values> (A Body) (analyze A true))
       (<recur> lp ((F (<lookup> F)))
	   (if (not (dynamic? F))
               (if (procedure? F)
                   (if (object-property F 'prolog-symbol)
		       (lp (F))
		       (permission_error modify static_procedure
					 (vector 
					  (list divide 
						F
						(length (<scm> A))))))
		   (type_error callable F))
	       
               (<and>
		(<push-dynamic> (<lookup> F) U 
		   (catch #t 
                     (lambda () 
		       (mockalambda #f (<scm> S) (<scm> A) Body))

                     (lambda x
                       (format #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
                       (type_error S P CC callable true)))))))))
     
     (F 
      (if (procedure? (<lookup> F))
          (asserta (vector (list F)) ext)
          (type_error callable Arg))))))))

(mk-assert++ asserta+_ <push-dynamic>)
(mk-assert++ assertz+_ <append-dynamic>)
(mk-assert++ assertg+_ <push-gen>)


(<define> (true . x) <cc>)

(define (simple s a)
  (let ((a (gp-cp a s)))
    (list
     (lambda () a)
     (lambda ()
       (lambda (s p cc cut scut x)
         (<with-guile-log> (scut cut s p cc)
	    (<=> x ,(gp-cp a S)))))
     (lambda () (cons a true)))))

(define (simple-3 s a g)
  (let ((a.g (gp-cp (cons a g) s)))
    (list
     (lambda () (car a.g))
     (lambda ()
       (lambda (s p cc cut scut x)
         (let ((a.g (gp-cp a.g s)))
           (<with-guile-log> (scut cut s p cc)
             (<=> x ,(car a.g))
             (goal-eval (cdr a.g))))))
     (lambda () a.g))))

(define-syntax-rule (mk-assert+ asserta <push-dynamic>)
  (define asserta
  (<case-lambda>
   ((Arg ext)
    (<var> (U) (asserta Arg U ext)))
   
   ((Arg U ext)
    (let ((Arg (<lookup> Arg)))
      (cond
       ((<var?> Arg)
	(instantiation_error))

       ((and (vector? Arg) (= (vector-length Arg) 1))
	(let ((Arg (<lookup> (vector-ref Arg 0))))
	  (if (gp-pair? Arg S)
	      (let ((H (gp-car Arg S)))
		(cond
		 ((gp-m-unify! ":-" H S)
		  (<var> (Head Body)
		   (<=> ,(gp-cdr Arg S) (Head Body))
		   (<recur> lp ((Head Head))
	             (<<match>> (#:mode - #:name subassert) (Head)
		       ((? <var?>)
			(instantiation_error))
	
		       (#((F . A))
			(<cut>
			 (<recur> lp2 ((F (<lookup> F))) 
		          (if (not (dynamic? F))
			      (if (procedure? F)
				  (if (object-property F 'prolog-symbol)
				      (lp2 (F))
				      (permission_error modify static_procedure
							(vector 
							 (list divide 
							       F
							       (length (<scm> A))))))
				  (type_error callable F))
			      (<push-dynamic> (<lookup> F) U
					      (simple-3 S A Body))))))
	 
		       (F
			(if (procedure? (<lookup> F))
			    (lp (vector (list F)))
			    (type_error callable Head)))))))
		(else
		 (let ((A (gp-cdr Arg S)))
		   (<recur> lp ((F (<lookup> H)))
		     (if (not (dynamic? F))
			 (if (procedure? F)
			     (if (object-property F 'prolog-symbol)
				 (lp (F))
				 (permission_error modify static_procedure
						   (vector 
						    (list divide 
							  F
							  (length (<scm> A))))))
			     (type_error callable F))

			 (<push-dynamic> (<lookup> F) (simple S A))))))))
	     
	      (type_error callable Arg))))
		 

       ((procedure? (<lookup> Arg))
	(asserta (vector (list Arg)) ext))
      
       (else
	(type_error callable Arg))))))))
	   
(mk-assert+ asserta_ <push-dynamic>)
(mk-assert+ assertz_ <append-dynamic>)
(mk-assert+ assertg_ <push-gen>)

(define-syntax-rule (mk-assertc+ asserta <push-dynamic>)
(define asserta 
  (<case-lambda>
   ((Arg ext)
    (<var> (u)
     (asserta Arg u ext)))
   ((Arg U ext)
    (<<match>> (#:mode - #:name asserta) (Arg)
     ((? <var?>)
      (instantiation_error))
     
     (#((":-" Head Body ))
      (<and>
      (<values> (HHead BBody) (analyze Head Body))
      (<recur> lp ((Head Head))
      (<<match>> (#:mode - #:name subassert) (HHead)
        ((? <var?>)
         (instantiation_error))
        (#((F . A))
         (<cut>
          (<recur> lp2 ((F (<lookup> F))) 
             (if (not (dynamic? F))
                 (if (procedure? F)
                     (if (object-property F 'prolog-symbol)
                         (lp2 (F))
                         (permission_error modify static_procedure
                                           (vector 
                                            (list divide 
                                                  F
                                                  (length (<scm> A))))))
                     (type_error callable F))
                 (<push-dynamic> (<lookup> F) U
                                 (catch #t
                                   (lambda () 
                                     (compile-prolog (<scm> S)
						     (<scm> A)
						     (<scm> BBody)
						     #f
						     ext))
                                   (lambda x
				     (format
				      #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
                                     (type_error S P CC callable Body))))))))
        (F
         (if (procedure? (<lookup> F))
             (lp (vector (list F)))
             (type_error callable Head)))))))

     (#((F . A))
      (<cut>
       (<values> (AA BBody) (analyze A true))
       (<recur> lp ((F (<lookup> F)))
	   (if (not (dynamic? F))
               (if (procedure? F)
                   (if (object-property F 'prolog-symbol)
		       (lp (F))
		       (permission_error modify static_procedure
					 (vector 
					  (list divide 
						F
						(length (<scm> AA))))))
		   (type_error callable F))

               (<and>
                 (<push-dynamic> (<lookup> F)
		   (catch #t 
                     (lambda () (compile-prolog (<scm> S) (<scm> AA)
						BBody #f
						ext))
                     (lambda x
                       (format #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
                       (type_error S P CC callable true)))))))))
     
     (F 
      (if (procedure? (<lookup> F))
          (asserta (vector (list F)) ext)
          (type_error callable Arg))))))))
	   
(mk-assertc+ assertac_ <push-dynamic>)
(mk-assertc+ assertzc_ <append-dynamic>)
(mk-assertc+ assertgc_ <push-gen>)

(define (get-name f)
  (let ((mod (procedure-property f 'module)))
    (if mod
	(list '@@ mod (procedure-name f))
	(procedure-name f))))
  
(define-syntax-rule (mk-assert+source asserta <push-dynamic>)
  (<define> (asserta stx Arg ext)
  (<<match>> (#:mode - #:name asserta) (Arg)
     ((? <var?>)
      (instantiation_error))

     (#((":-" Head))      
      (translate-directive stx Head))
    
     (#(((and Op ":-") Head Body ))
      (<and>
       (<values> (Head Body) (analyze Head Body))
      (<recur> lp ((Head Head))
      (<<match>> (#:mode - #:name subassert) (Head)
        ((? <var?>)
         (instantiation_error))
        (#((F . A))
         (<cut>
          (<recur> lp2 ((F (<lookup> F))) 
             (if (not (dynamic? F))
                 (if (procedure? F)
                     (if (object-property F 'prolog-symbol)
                         (lp2 (F))
                         (permission_error modify static_procedure
                                           (vector 
                                            (list divide 
                                                  F
                                                  (length (<scm> A))))))
                     (type_error callable F))
		 (<cc>
		  #`(<push-dynamic> #,(datum->syntax stx 
						     (get-name (<lookup> F)))
		      #,(catch #t
			  (lambda () 
			    (compile-prolog (<scm> S) 
					    (<scm> A)
					    (maybe-call (<scm> Body))
					    stx
					    ext))
			  (lambda x
			    (format
			     #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
			    (type_error S P CC callable Body)))))))))
        (F
         (if (procedure? (<lookup> F))
             (lp (vector (list F)))
             (type_error callable Head)))))))

     (#((F . A))
      (<cut>
       (<values> (A Body) (analyze A true))
       (<recur> lp ((F (<lookup> F)))
           (if (not (dynamic? F))
               (if (procedure? F)
                   (if (object-property F 'prolog-symbol)
		       (lp (F))
		       (permission_error modify static_procedure
					 (vector 
					  (list divide 
						F
						(length (<scm> A))))))
		   (type_error callable F))

               (<cc>
                #`(<push-dynamic> #,(datum->syntax stx (get-name (<lookup> F)))
		    #,(catch #t 
			(lambda () (compile-prolog (<scm> S) (<scm> A)
						   Body stx
						   ext))
			(lambda x
			  (format #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
			  (type_error S P CC callable true)))))))))
     
     (F 
      (if (procedure? (<lookup> F))
	  (asserta stx (vector (list F)) ext)
	  (type_error callable Arg))))))
	   
(mk-assert+source asserta-source <push-dynamic>)
(mk-assert+source assertz-source <append-dynamic>)
(mk-assert+source assertg-source <push-gen>)

(define-syntax-rule (mk-assert++source asserta <push-dynamic>)
(<define> (asserta stx Arg ext)
  (<<match>> (#:mode - #:name asserta) (Arg)
     ((? <var?>)
      (instantiation_error))

     (#((":-" Head))      
      (translate-directive stx Head))
    
     (#(((and Op ":-") Head Body ))
      (<and>
      (<values> (Head Body) (analyze Head Body))
      (<recur> lp ((Head Head))
      (<<match>> (#:mode - #:name subassert) (Head)
        ((? <var?>)
         (instantiation_error))
        (#((F . A))
         (<cut> 
          (<recur> lp2 ((F (<lookup> F)))
             (if (not (dynamic? F))
                 (if (procedure? F)
                     (if (object-property F 'prolog-symbol)
                         (lp2 (F))
                         (permission_error modify static_procedure
                                           (vector 
                                            (list divide 
                                                  F
                                                  (length (<scm> A))))))
                     (type_error callable F))
		 (<cc>
		  #`(<push-dynamic> #,(datum->syntax stx 
						     (get-name (<lookup> F)))
		      #,(catch #t
			  (lambda () 
			    (mockalambda stx
					 (<scm> S) 
					 (<scm> A)
					 (maybe-call (<scm> Body))))
			  (lambda x
			    (format
			     #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
			    (type_error S P CC callable Body)))))))))
        (F
         (if (procedure? (<lookup> F))
             (lp (vector (list F)))
             (type_error callable Head)))))))

     (#((F . A))
      (<cut>
       (<values> (A Body) (analyze A true))
       (<recur> lp ((F (<lookup> F)))
           (if (not (dynamic? F))
               (if (procedure? F)
                   (if (object-property F 'prolog-symbol)
		       (lp (F))
		       (permission_error modify static_procedure
					 (vector 
					  (list divide 
						F
						(length (<scm> A))))))
		   (type_error callable F))

               (<cc>
                #`(<push-dynamic> #,(datum->syntax stx (get-name (<lookup> F)))
		    #,(catch #t 
			(lambda ()
			  (mockalambda stx
				       (<scm> S) (<scm> A)
				       Body))
			(lambda x
			  (format #t "PROLOG HOT COMPILE ERROR:~%~a~%~%" x)
			  (type_error S P CC callable true)))))))))
     
     (F 
      (if (procedure? (<lookup> F))
	  (asserta stx (vector (list F)) ext)
	  (type_error callable Arg))))))
	   
(mk-assert++source asserta-source+ <push-dynamic>)
(mk-assert++source assertz-source+ <append-dynamic>)
(mk-assert++source assertg-source+ <push-gen>)


(<define*> (asserta x #:optional (u #f) #:key (ext #f))
  (if u
      (asserta_ x u ext)
      (asserta_ x ext)))

(<define*> (assertz x #:optional (u #f) #:key (ext #f))
  (if u
      (assertz_ x u ext)
      (assertz_ x ext)))

(<define*> (assertg x #:optional (u #f) #:key (ext #f))
  (if u
      (assertg_ x u ext)
      (assertg_ x ext)))

(<define*> (assertac x #:optional (u #f) #:key (ext #f))
  (if u
      (assertac_ x u ext)
      (assertac_ x ext)))

(<define*> (assertzc x #:optional (u #f) #:key (ext #f))
  (if u
      (assertzc_ x u ext)
      (assertzc_ x ext)))

(<define*> (assertgc x #:optional (u #f) #:key (ext #f))
  (if u
      (assertgc_ x u ext)
      (assertgc_ x ext)))

(<define*> (assertaf x #:optional (u #f) #:key (ext #f))
  (if u
      (asserta+_ x u ext)
      (asserta+_ x ext)))

(<define*> (assertzf x #:optional (u #f) #:key (ext #f))
  (if u
      (assertz+_ x u ext)
      (assertz+_ x ext)))

(<define*> (assertgf x #:optional (u #f) #:key (ext #f))
  (if u
      (assertg+_ x u ext)
      (assertg+_ x ext)))

(define clause
  (<case-lambda>
   ((Head Body)
    (clause Head Body #f))
   ((Head Body Ref)
    (if (or (not Ref) (attvar? Ref))
	(<let> ((Head (<lookup> Head)))
	  (cond
	   ((<var?> Head)
	    (instantiation_error))

	   ((procedure? Head)
	    (clause (vector (list Head)) Body))

	   (else
	    (<var> (F A)
	      (<if> (<or> (<=> Head ,(vector (cons F  A))) 
			  (<and> (<=> Head F) (<=> A ())))
		 (<recur> lp ((FF (<lookup> F)))
		    (cond
		     ((dynamic? FF)
		      (if Ref
			  (<clause-dynamic-3> FF A Body Ref)
			  (<clause-dynamic> FF A Body)))
		      
		     ((procedure? FF)
		      (if (object-property FF 'prolog-symbol)
			  (lp (FF))
			  (permission_error access private_procedure 
					    (vector 
					     (list divide 
						   FF
						   (length (<scm> A)))))))
		     (else
		      (type_error callable F))))
                 
		 (type_error callable Head))))))
	(<var> (F A)
	  (<clause-dynamic-3> F A Body Ref)
	  (if (null? A)
	      (<=> Head F)
	      (<=> Head ,(vector (cons F A)))))))))

(define clausei
  (<case-lambda>
   ((Head Body Ref)
    (if (or (not Ref) (attvar? Ref))
	(<let> ((Head (<lookup> Head)))
	  (cond
	   ((<var?> Head)
	    (instantiation_error))

	   ((procedure? Head)
	    (clause (vector (list Head)) Body))

	   (else
	    (<var> (F A)
	      (<if> (<or> (<=> Head ,(vector (cons F  A))) 
			  (<and> (<=> Head F) (<=> A ())))
		 (<recur> lp ((FF (<lookup> F)))
		    (cond
		     ((dynamic? FF)
		      (if Ref
			  (<clause-dynamic-3i> FF A Body Ref)
			  (<clause-dynamic> FF A Body)))
		      
		     ((procedure? FF)
		      (if (object-property FF 'prolog-symbol)
			  (lp (FF))
			  (permission_error access private_procedure 
					    (vector 
					     (list divide 
						   FF
						   (length (<scm> A)))))))
		     (else
		      (type_error callable F))))
                 
		 (type_error callable Head))))))
	(<var> (F A)
	  (<clause-dynamic-3> F A Body Ref)
	  (if (null? A)
	      (<=> Head F)
	      (<=> Head ,(vector (cons F A)))))))))



(<define> (retract Arg)
 (<let> ((Arg (<lookup> Arg)))
  (cond
   ((<var?> Arg)
    (instantiation_error))
   ((procedure? Arg)
    (retract (vector (list Arg))))
   (else
      (<var> (Head Body F A)
         (<if> (<=> Arg ,(vector (list fact Head Body)))
               (if (<var?> Head)
                   (instantiation_error)
                   (<if> (<=> Head ,(vector (cons F  A)))
                         (<recur> lp ((F (<lookup> F)))
                           (cond
                            ((dynamic? F)
                             (<and>
                              (<clause-dynamic>  (<lookup> F) A Body)
                              (<retract-dynamic> (<lookup> F) (cons A Body))))
                            ((object-property F 'prolog-symbol)
                             (lp (F)))
                            (else
                             (permission_error 
                              modify static_procedure
                              (vector (list divide 
                                            F
                                            (length (<scm> A))))))))
                         (type_error callable Head)))
               (<if> (<=> Arg ,(vector (cons F  A)))
                     (<recur> lp ((F (<lookup> F)))
                              (cond 
                               ((dynamic? F)
                                (<and>
                                 (<clause-dynamic> F A true))
                                (<retract-dynamic> F (cons A true)))

                               ((object-property F 'prolog-symbol)
                                (lp (F)))

                               (else
                                (permission_error 
                                 modify static_procedure
                                 (vector (list divide 
                                               F
                                               (length (<scm> A))))))))
                     (type_error callable Head))))))))

(<define> (abolish pred)
  (<let> ((pred (<lookup> pred)))
    (cond
     ((<var?> pred)
      (instantiation_error))
     (else
      (<<match>> (#:mode - #:name abolish) (pred)
        (#((,divide F N))
	   (<recur> lp ((F (<lookup> F))
                      (N (<lookup> N)))
	     (cond
	      ((or (<var?> F) (<var?> N))
	       (instantiation_error))

	      ((dynamic? F)
	       (if (and (number? N) (integer? N))
		   (cond
		    ((< N 0)
		     (domain_error not_less_than_zero N))
		    ((< (get-flag max_arity) N)
		     (representation_error max_arity))
		    (else
		     (<code> (dynamic-abolish F))))
		   (type_error integer N)))
          
	      ((object-property F 'prolog-symbol)
	       (lp (F) N))

	      ((procedure? F)
	       (permission_error modify static_procedure 
				 (vector (list divide 
					       F N))))
	      (else
	       (type_error atom F)))))
        
        (_
         (type_error predicate_indicator pred)))))))

           
(define (get-dyns mod)
  (define l '())
  (module-for-each
   (lambda (k v)
     (let ((v (variable-ref v)))
       (if (and (procedure? v) (or (dynamic? v) 
                                   (and (object-property v 'prolog-symbol)
                                        (v 'defined))))
           (set! l (cons v l)))))
   mod)
  l)

(<define> (current_predicate X)
  <fail>
  (<match> (#:mode - #:name current_predicate) (X)
    (#((,divide F N))
     (<cut>
      (<let> ((F (<lookup> F)))
       (cond
        ((<var?> F)
         (<let> ((L (get-dyns (current-module))))
           (pr-member F L)))
        ((procedure? F)
         (<cut> (<when> (or (dynamic? F) 
                            (and (object-property F 'prolog-symbol)
                                 (F 'defined))
                            (and (procedure? F)
                                 (not (object-property F 'prolog-symbol)))))))
        (else
         (type_error predicate_indicator X))))))
    (_
     (type_error predicate_indicator X))))

(set! (@@ (logic guile-log slask) assertz-source+) assertz-source)

(<define> (accessify_predicate f)
   (<recur> lp ((ff (<lookup> f)))
     (if (not (dynamic? ff))
	 (if (procedure? ff)
	     (if (object-property ff 'prolog-symbol)
		 (lp (ff))
		 (type_error predicate_indicator f))
	     (type_error predicate_indicator f))
	 (<let> ((env (object-property ff 'dynamic-data)))
	   (<code>
	    (set-object-property! f 'get-accessor (vector-ref env 4))
	    (set-object-property! f 'set-accessor (vector-ref env 5)))))))
