(define-module (inriasuite)
  #:use-module (logic guile-log iso-prolog)
  #:use-module ((guile) #:select (@ @@ define))
  #:pure
  #:re-export (prolog-run)
  #:export (run_tests  run_all_tests unexpected_ball <-- exists
		       failure success impl_def undefined))

(compile-prolog-file "inriasuite.pl")
(save-operator-table)
