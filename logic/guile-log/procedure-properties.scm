(define-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log vlist)
  #:replace (procedure-name procedure-properties
			    procedure-property
			    set-procedure-properties!
			    set-procedure-property!
			    object-property
			    object-properties
			    set-object-property!
			    set-object-properties!
			    ))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define pps (@ (guile) procedure-properties))
(define spp (@ (guile) set-procedure-property!))
(define ops (@ (guile) object-properties))
(define pn  (@ (guile) procedure-name))

(define *pp* (make-fluid
	      (with-fluids ((init-block-size 100000))
		(vhash-consq 1 '() vlist-null))))
(define *op* (make-fluid
	      (with-fluids ((init-block-size 100000))
		(vhash-consq 1 '() vlist-null))))

(define (procedure-name f)
  (aif it (procedure-property f 'name)
       it
       (aif it (pn f)
	    (begin
	      (set-procedure-property! f 'name it)
	      it)
	    it)))

(define (procedure-properties f)
  (aif it (vhash-assq f (fluid-ref *pp*))
       (cdr it)
       (aif it (pps f)
	    (begin
	      (set-procedure-properties! f it)
	      it)
	    it)))

(define-inlinable (object-properties- f)
  (aif it (vhash-assq f (fluid-ref *op*))
       (cdr it)
       (aif it (ops f)
	    (begin
	      (set-object-properties! f it)
	      it)
	    it)))

(define (object-properties f) (object-properties- f))

(define (set-procedure-properties! f v)
  (fluid-set! *pp*
	      (vhash-consq f v (fluid-ref *pp*))))

(define (set-object-properties! f v)
  (fluid-set! *op*
	      (vhash-consq f v (fluid-ref *op*))))

(define procedure-property
  (case-lambda
    ((f k)
     (procedure-property f k #f))
    ((f k p)
     (aif it (procedure-properties f)
	  (aif it (assoc k it)
	       (cdr it)
	       p)
	  p))))

(define object-property
  (case-lambda
    ((f k)
     (object-property f k #f))
    ((f k p)
     (aif it (object-properties- f)
	  (aif it (assoc k it)
	       (cdr it)
	       p)
	  p))))

(define (set-procedure-property! f k v)
  (set-procedure-properties! f
			     (aif it (procedure-properties f)
				  (cons (cons k v) it)
				  (list (cons k v))))
  (if (eq? k 'name)
      (spp f k v)))

(define (set-object-property! f k v)
  (set-object-properties! f
			  (aif it (object-properties f)
			       (cons (cons k v) it)
			       (list (cons k v)))))

((@@ (logic guile-log code-load) set-proc-prop!) *pp*)

(set! (@@ (logic guile-log code-load) multibute)
  (lambda (f)
    (set-object-property! f (@@ (logic guile-log code-load) delayed-id) #t)
    f))
