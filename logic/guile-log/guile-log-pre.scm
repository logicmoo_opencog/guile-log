(define-module (logic guile-log guile-log-pre)
  #:use-module ((system syntax) #:select (syntax-local-binding))
  #:use-module (compat racket misc)
  #:export (define-guile-log guile-log-macro? log-code-macro log-code-macro?
	     define-and-log and-log-macro? bounded-equal? stx-case))

(define (bounded-equal? stx-x stx-y)
  (call-with-values (lambda () (syntax-local-binding stx-x))
    (lambda (type-x val-x)
      (lambda (type-y val-y)
	(and (equal? type-x type-y) (equal? val-x val-y))))))

(define-syntax-rule (stx-case x . l)
  (let ((xx x))
    (stx-case-w xx . l)))

(define-syntax stx-case-w
  (lambda (x)
    (syntax-case x (else)
      ((_ x (else . code))
       #'(begin . code))
      ((_ x ((m ...) code ...) . l)
       #'(if (and (bounded-equal? x m) ...)
	     (begin code ...)
	     (stx-case-w x . l)))
      ((_ x)
       #'(error "stx-case did not match")))))


(define *guile-log-macros* (make-weak-key-hash-table))
(define *log-code-macros*  (make-weak-key-hash-table))
(define *and-code-macros*  (make-weak-key-hash-table))

(define-syntax and-let*
  (syntax-rules ()
    ((_ ((x v) . l) . code)
     (let ((x v))
       (if x
	   (and-let* l . code)
	   #f)))
    ((_ ((x) . l) . code)
     (if x (and-let* l . code) #f))
    ((_ () . code)
     (begin . code))))

(define (setter table)
  (lambda (s)
    (if (and-let* (((symbol? s))
		   (m (module-ref (current-module) s))
		   ((macro? m))
		   (n (macro-binding m)))
	   (hash-set! table n #t)
	   #t)
	#t
	(warn (format #f "macro type setter fails, ~a is not a macro" s)))))


(define (tester table)
  (lambda (s)       
    (and (syntax? s)
         (syntax-case s (@@ @)
           (id
            (identifier? #'id)
            (call-with-values (lambda () (syntax-local-binding #'id))
              (lambda (type value)
                (let ((i (syntax->datum #'id)))
                  (if (eq? i '<let-with-lr-guard>)
		      `(tester ,type ,value ,#'id)))
                (case type
                  ((macro)
                   (hash-ref table value #f))
                  (else
                   #f)))))

           ((@ l nm)
            (catch #t
              (lambda ()
                (let ((l  (syntax->datum #'l))
                      (nm (syntax->datum #'nm)))
                  (let ((box (macro-binding 
                              (module-ref 
                               (resolve-module l) nm))))
                    (hash-ref table box #f))))
              (lambda x #f)))

           ((@@ l nm)
            (catch #t
              (lambda ()
                (let ((l  (syntax->datum #'l))
                      (nm (syntax->datum #'nm)))
                  (let ((box (macro-binding 
                              (module-ref 
                               (resolve-module l) nm))))
                    (hash-ref table box #f))))
              (lambda x #f)))
           
           (_ #f)))))
                
                

(define guile-log-macro?  (tester *guile-log-macros*))
(define and-log-macro?    (tester *and-code-macros*))
(define log-code-macro?   (tester *log-code-macros*))
(define guile-log-macro!  (setter *guile-log-macros*))
(define and-log-macro!    (setter *and-code-macros*))
(define log-code-macro!   (setter *log-code-macros*))

(define-syntax log-code-macro
  (lambda (x)
    (syntax-case x (quote)
      ((_ (quote x)) 
       #'(eval-when (compile load eval)
	   (begin             
             (log-code-macro! 'x) #f)))
      ((_ x)       
       #'(log-code-macro 'x)))))

(define-syntax define-guile-log
  (lambda (x)
    (syntax-case x ()
      ((_ n . l)
       #'(begin
	   (define-syntax n . l)
	   (eval-when (compile load eval)
	     (guile-log-macro! 'n)))))))
	   
	   

(define-syntax define-and-log
  (lambda (x)
    (syntax-case x ()
      ((_ n . l)
       #'(begin
	   (define-syntax n . l)
	   (eval-when (compile load eval)
	     (and-log-macro! 'n)))))))
