(define-module (logic guile-log collects)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:export (<collect>  
	    <collect-2>
	    <collect-step>  
	    <collect-step-2>

	    <sum>  
	    <sum-2>
	    <sum-step>  
	    <sum-step-2>

	    <prod>  
	    <prod-2>
	    <prod-step>  
	    <prod-step-2>

	    <max>  
	    <max-2>
	    <max-step>  
	    <max-step-2>

	    <min>  
	    <min-2>
	    <min-step>  
	    <min-step-2>
	    
	    <soland>  
	    <soland-2>
	    <soland-step>  
	    <soland-step-2>

	    <solor>  
	    <solor-2>
	    <solor-step>  
	    <solor-step-2>))
(define Inf (inf))
(define -Inf (- (inf)))
	        
(<define> (<collect> Lam X L)
  (<fold> cons '() Lam X L))

(<define> (<collect-2> Lam X Y L)
  (<fix-fold> cons '() Lam X Y L))

(<define> (<sum> Lam X L)
  (<fold> + 0 Lam X L))

(<define> (<sum-2> Lam X Y L)
  (<fix-fold> + 0 Lam X Y L))

(<define> (<prod> Lam X L)
  (<fold> * 1 Lam X L))

(<define> (<prod-2> Lam X Y L)
  (<fix-fold> * 1 Lam X Y L))

(<define> (<max> Lam X L)
  (<fold> max -Inf Lam X L))

(<define> (<max-2> Lam X Y L)
  (<fix-fold> max -Inf Lam X Y L))

(<define> (<min> Lam X L)
  (<fold> min Inf Lam X L))


(<define> (<min-2> Lam X Y L)
  (<fix-fold> min Inf Lam X Y L))
(define (and* x y) (and x y))
(define (or*  x y) (or  x y))
(<define> (<soland> Lam X L)
  (<fold> and* #t Lam X L))

(<define> (<soland-2> Lam X Y L)
  (<fix-fold> and* #t Lam X Y L))

(<define> (<solor> Lam X L)
  (<fold> or* #f Lam X L))

(<define> (<solor-2> Lam X Y L)
  (<fix-fold> or* #f Lam X Y L))

(<define> (<collect-step> Lam X L)
  (<fold-step> cons '() Lam X L))

(<define> (<collect-step-2> Lam X Y L)
  (<fix-fold-step> cons '() Lam X Y L))

(<define> (<sum-step> Lam X L)
  (<fold> + 0 Lam X L))

(<define> (<sum-step-2> Lam X Y L)
  (<fix-fold> + 0 Lam X Y L))

(<define> (<prod-step> Lam X L)
  (<fold> * 1 Lam X L))

(<define> (<prod-step-2> Lam X Y L)
  (<fix-fold> * 1 Lam X Y L))

(<define> (<max-step> Lam X L)
  (<fold-step> max -Inf Lam X L))

(<define> (<max-step-2> Lam X Y L)
  (<fix-fold-step> max -Inf Lam X Y L))

(<define> (<min-step> Lam X L)
  (<fold> min Inf Lam X L))

(<define> (<min-step-2> Lam X Y L)
  (<fix-fold> min Inf Lam X Y L))

(<define> (<soland-step> Lam X L)
  (<fold> and* #t Lam X L))

(<define> (<soland-step-2> Lam X Y L)
  (<fix-fold> and* #t Lam X Y L))

(<define> (<solor-step> Lam X L)
  (<fold> or* #f Lam X L))

(<define> (<solor-step-2> Lam X Y L)
  (<fix-fold> or* #f Lam X Y L))
