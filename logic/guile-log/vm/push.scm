(define-module (logic guile-log vm push)
  #:use-module ((logic guile-log iso-prolog)
		#:renamer
		(lambda (x) (if (eq? x 'reset)
				'reset_iso
				x)))
  #:use-module (logic guile-log vm utils)
  #:replace (cutter goto-inst sp-move equal-instruction
		   push-instruction pushv push-variable set_p
		   pop-variable pop seek dup clear-sp push_at
                   push-variable-scm push-s
                   ))

(compile-prolog-string
"
  'push-s'(I,II,L,LL) :-
    II is I + 1,
    gset(sp(I),s,I,L,LL).

  'clear-sp'(I,0,L,LL) :-
     reset(0,L,LL).

   cutter(V,VC,I,L,LL) :-
      gset(cut,V,I,L,L1),
      gset(scut,VC,I,L1,LL).

   'goto-inst'(Inst,I,I,L,LL) :-
      generate(jmp(Inst),L,LL).

   cut(I,L,LL) :-
      isZero(cut,I,L,L2),
      jne(Lab,L2,L3),
      gset(p,vcut,I,L3,L4),
      j(Lab2,L4,L5),
      gset(p,cut,I,L6,L7).
      scmcall('gp-prune',[scut],I,_,L7,LL).

 
   push_at(K,I,II,L,LL) :-
      gset(sp(I),svar(K),I,L,LL),
      II is I + 1.  

   'sp-move'(_,V,I,II,L,LL) :-
     II is I - 1,
     gset(V,sp(II),I,L,LL).

   'equal-instruction'(C,I,II,L,LL) :-
      pltest('equal?',[sp(I-1),c(C)],I,L,LL),
      II is I - 1.
   
   -trace.
   'push-instruction'(C,I,II,L,LL) :-
     gset(sp(I),c(C),I,L,LL),
     II is I + 1.

   pushv(X,I,II,L,LL) :-
     scmcall('gp-var!',[s],I,I2,L,L1),
     gset(sp[I],svar(I2-1),I2,L1,LL),
     II is I + 1.


   'push-variable'(V,I,II,L,LL) :-
      gset(sp(I),V,I,L,LL),
      II is I + 1.
   
   -trace.
   'push-variable-scm'(V,I,II,L,LL) :-
      gset(sp(I),V,I,L,L1),
      generate(lookup(base+I),L1,LL),
      II is I + 1.

   'pop-variable'(V,I,II,L,LL) :-
       gset(V,sp(I-1),L,LL),
       II is I - 1.

   pop(N,I,II,L,L) :-
      II is I - N.
    
   pop(I,II,L,L) :-
      II is I - 1.


   seek(N,I,II,L,L) :-
      II is I + N.

   dup(I,II,L,LL) :-
      movex(I,I-1,L,L),
      II is I + 1.
   
   -trace.
   set_p(P,I,I,L,LL) :-
      P=ground(X) ->
         gset(p,l(X),I,L,LL);
      L=LL.
")

  
