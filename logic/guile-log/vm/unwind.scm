(define-module (logic guile-log vm unwind)
  #:use-module (logic guile-log vm utils)
  #:use-module (logic guile-log iso-prolog)
  #:export (unwind-light-tail unwind-light unwind unwind-negation
           unwind-tail unwind-ps unwind-psc unwind-p
           post-negation  post-s post-q post-c post-sc restore-c restore-s restore-pc
	   restore-p
           softie softie-light softie-ps softie-pc softie-psc
	   fail-p fail-psc fail-pc fail-p0))

(compile-prolog-string
"
   'post-negation'(NP,CUT,I,L,LL) :-
      scm_call('gp-post-negation',[vec,c(NP),c(CUT)],I,L,L1),
      generate('immediate-tag=?'(I,3839,4),L1,L2),
      generate(je(A),L2,L3),
      gfail(I,L3,L4),
      label(A,L4).

   'post-s'(NP,CUT,I,L,LL) :-
      scmcall('gp-post-s',[vec,c(NP),c(CUT)],I,L,LL).

   'post-q'(CUT,I,L,LL) :-
      gset(cut,c(CUT),I,L,LL).

   'post-c'(C,I,L,LL) :-
      gset(cut,C,I,L,LL).

   'post-sc'(S,C,I,L,LL) :-
      gset(s,S,I,L,L1),
      gset(cut,C,I,L1,LL).

   -trace.
   'unwind-tail'(S,P,I,I,L,LL) :-
       gset(s,S,I,L,L1),
       gset(p,P,I,L1,L2),
       generate('unwind-tail',L2,LL).

   -trace.
   'unwind-light-tail'(Tag,I,I,L,LL) :-
      gset(p,Tag,I,L,LL).

   'unwind-ps'(P,S,I,I,L,LL) :-
      gset(s,S,I,L,L1),
      generate('unwind-tail',L1,L2),
      gset(p,P,I,L2,LL).
   
    'unwind-p'(P,I,I,L,LL) :- 
       gset(p,P,I,L,LL).
      
   'unwind-psc'(P,S,C,I,L,LL) :-
      gset(s,S,I,L1,L2),
      generate('unwind-tail',L,L1),
      gset(p,P,I,L2,L3),
      gset(cut,C,I,L3,LL).

   'restore-c'(C,I,I,L,LL) :-
      gset(cut,C,I,L,LL).

   -trace.
   'restore-p'(P,I,I,L,LL) :-
      gset(p,P,I,L,LL).

   'restore-s'(S,I,I,L,LL) :-
      gset(s,S,I,L,LL).

   'restore-pc'(P,C,I,I,L,LL) :-
      gset(p,P,I,L,L1),
      gset(cut,C,I,L1,LL).

   softie(P,I,I,L,LL) :-
     gset(p,P,L,LL).

   'softie-light'(Tag,I,I,L,LL) :-
     gset(p,Tag,I,L,LL).

   -trace.
   'softie-ps'(P,S,I,I,L,LL) :-
       gset(p,P,I,L,L1),
       gset(s,S,I,L1,LL).

   'softie-pc'(P,C,I,L,LL) :-
       gset(p,P,I,L,L1),
       gset(cut,C,I,L1,LL).

   'softie-psc'(P,S,C,I,L,LL) :-
       gset(p,P,I,L,L1),
       gset(s,S,I,L1,LL),
       gset(cut,C,I,L1,L2).

   unwind(S,P,A,I,I,L,LL) :-
      gset(p,P,I,L,L1),
      gset(s,S,I,L1,L2),
      generate(unwind,L2,LL).
      

   -trace.
   'unwind-light'(P,A,I,I,L,LL) :-
      gset(P,l(A),I,L,LL).

   'unwind-negation'(N,CUT,I,L,LL) :-
      scmcall('gp-unwind-negation',[vec,c(N),c(CUT)],I,L,LL).

    gfail(I,L,LL) :-
       generate(tailcall(1),L,LL).

   'fail-psc'(P,C,S,I,I,L,LL) :-
      gset(p,P,I,L,L1),
      gset(cut,C,I,L1,L2),
      gset(s,S,L2,L3),
      gfail(I,L3,LL).

   'fail-pc'(P,C,I,I,L,LL) :-
      gset(p,P,I,L,L1),
      gset(cut,C,I,L1,L2),
      gfail(I,L2,LL).

   'fail-p'(P,I,I,L,LL) :-
      gset(p,P,I,L,L1),
      gfail(I,L1,LL).

   'fail-p0'(ground(P),I,I,L,LL) :-
      generate(goto(P),L,LL).
")
