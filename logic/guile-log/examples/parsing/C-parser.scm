(use-modules (logic guile-log parser))

(define f-digit   (f-reg! "[0-9]"))
(define f-integer (f+ f-digit))
(define f-decimal (f-seq f-integer
			 (f-or! (f-seq (f-tag! ".")
                                       (f* digit))
                                f-true)))
(define f-exponent (f-seq f-decimal 
			  (f-char #\e)
			  (f-or (f-char #\+)
				(f-char #\-)
				f-true)
			  f-decimal))

(define prefix   (mk-token (f-or! (f-tag "0x") (f-tag "0o") (f-tag "0b"))))
(define postfix  (mk-token (f-reg! "[lL]
(define p-number  (p-or p-exponent p-decimal))
(define C-number  (mk-token p-number read-mk))


;; C Symbols
(define C-head     (p-regchar "[_a-zA-Z]"))
(define C-rest     (p-regchar "[_a-zA-Z0-9]"))
(define C-symbol   (p-symbol 
		    (p-seq C-head (p* C-rest))))

;; C Strings
(define C-stringbody
  (p-or (p-reg "[^\"\\]")
	(p-seq (p-char #\\) (p-reg "."))))
	 
(define C-string  
  (p-string
   (p-string (p-seq (p-char #\") (p* C-strbody) (p-char #\")))))

;; C kewords, C_Identifier
(define Ckw (make-keywords-data))
(apply add-keywords Ckw 
       '(break case char const continue default do double else enum extern 
	       float for goto if int long register return short signed sizeof 
	       static struct switch typedef union unsigned void volatile while))

(define-id/keyword (C-id C-kw) Ckw C-symbol)

;; C Expression
(define C-expr0 #f)
(define C-expr (Dp C-expr0))
(define C-pterm
  (let ((p-l (p-char #\())
	(p-r (p-char #\))))
    (<p-lambda> x
      (.. c1 (p-l _))
      (.. c2 (C-expr))
      (.. c3 (p-r c1))
      (<let> (Res)
	(<=> Res (#:expr c2))
	(<cc> X XL N M Res)))))


(define Cop (make-opdata))
(define (mk-tag-matcher prefix name)
  (<p-lambda> ()
   (.. c1 (C-expr))
   (.. c2 ((p-char str) _))
   (<var> (Res)
     (<=> Res (name . c1)))
   (<cc> X XL N M Res)))

(define C-call (mk-tag-matcher ")" #:call))
(define C-aref (mk-tag-matcher "]" #:aref))
(add-operator fy  "(" 2  C-call)
(add-operator fy  "[" 2  C-aref)
(add-operator yf  "(" 3  C-typecast)
(add-operator yfx "?" 15 C-?:)
(<p-define> (C-?)
   (.. c1 (C-expr))
   (.. c2 ((p-char #\:) _))
   (<cc> X XL N M c2))

(define C-atom (p-or C-pexpr 
		     C-string
		     C-number
		     C-identifier
		     C-char))

(for-each
 (lambda (l)
   (apply add-operator Cop l))
 '((fy  ++     2) (fy  --     2) (fy  "."    2) (fy  ->     2)

   (yf  ++     3) (yf  --     3) (yf  +      3) (yf  -      3)
   (yf  !      3) (yf  ~      3) (yf  *      3) (yf  &      3)
   (yf  sizeof 3)

   (xfy *      5) (xfy /      5) (xfy %      5)
   
   (xfy +      6) (xfy -      6)
   
   (xfy <<     7) (xfy >>     7)

   (xfy <      8) (xfy >      8) (xfy <=     8) (xfy >=     8)
   
   (xfy ==     9) (xfy !=     9)
   
   (xfy &      10)

   (xfy ^      11)
   
   (xfy "|"    12)
   
   (xfy &&     13)

   (xfy "||"   14)
   
   (yfx =      16) (yfx +=     16) (yfx -=     16) (yfx *=     16)
   (yfx /=     16) (yfx %=     16) (yfx <<=    16) (yfx >>=    16)
   (yfx &=     16) (yfx ^=     16) (yfx "|="   16)
   
   (xfy ,      18)))

(set! C-expr0 (mk-operator-expression C-atom Cop))
