(use-modules (logic guile-log grep))
(use-modules (logic guile-log parsing sch-match))
(use-modules (rnrs io ports))
(define (f)
  (format #t
"

(let ((x (+ 1 a))
      (y 2)
      (z 3) 
      (w 4))
  (do-someting x y z w))


"))


(define-match-class swap
  (pattern #(l (x y) r) 
     #:with tr #'#(l (x.l y.it x.r y.l x.it y.r) r)))

(define-match-class (tr-it oldval newval)
  (pattern #(l ,oldval r)
     #:with tr #`#(l #,(datum->syntax #'a newval) r)))

(define (test)
  (par-sed (scm-sed (#(l ((~var let (tr-it 'let 'tel))
			      #(a ((~var bind swap) ...) b)
			 body ...) r)
		     #'#(l (let.tr #(a (bind.tr ...) b) body ...) r)))
      (f)))
 