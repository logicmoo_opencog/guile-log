(define-module (logic guile-log guile-prolog postpone)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log postpone)
  #:re-export (postpone)
  #:export (postpone_frame))

(<define> (postpone_frame . x) (<apply> postpone-frame x))



