(define-module (logic guile-log guile-prolog delay)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (delay insert val eplus cell cellit cplus))

(define-record-type <delay>
  (make-delay exp var)
  delay?
  (exp    delay-exp)
  (var    delay-var))

(set-record-type-printer! <delay>
   (lambda (vl port) 
     (format port "<delayed>")))

(define-record-type <cell>
  (make-cell exp var val)
  cell?
  (exp    cell-exp)
  (val    cell-val)
  (var    cell-var))

(set-record-type-printer! <delay>
   (lambda (vl port) 
     (format port "<delayed>")))

(set-record-type-printer! <cell>
   (lambda (vl port) 
     (format port "<cell ~a>" (val vl))))

(define val
  (case-lambda
    ((x) (if (cell? x)
             (cell-val x)
             x))
    (x
     (apply
      (<case-lambda>
       ((x v) (<=> v ,(val (<lookup> x)))))
      x))))
     
(<define> (not_delayed x)
  (if (delay? (<lookup> x))
      <fail>
      <cc>))

(<define> (not_var_cell x)
  (if (cell? (<lookup> x))
      (if (<var?> (val (<lookup> x)))
	  <fail>
	  <cc>)
      <cc>))

(define cell
  (<case-lambda>
   ((x)    
    (let ((x (<lookup> x)))
      (if (<var?> x)
          (<var> (u)
                 (<set> x (make-cell true x u)))
          (<match> (#:mode +) (x)
           ((a . b)
            (<and>
             <cut>
             (cell a)
             (cell b)))
           (_ (<and> <cut> <cc>))))))
   
   ((x v)
    (let ((x (<lookup> x)))
      (if (<var?> x)
          (<var> (u)
             (<set> u v)
             (<set> x (make-cell true x u)))
          (<match> (#:mode +) (x v)
            ((a . b) (v1 . v2)
             (<and>
              <cut>
              (cell a v1)
              (cell b v2)))
            (_ _ (<and> <cut> <cc>))))))))

(compile-prolog-string 
"
delayMatch(X,Y):- X==Y,!. 
delayMatch(X,(A,B)) :- delayMatch(X,A) -> true ; delayMatch(X,B).

non(X) :- nonvar(X) -> (not_delayed(X) -> not_var_cell(X) ; fail)  ; fail.

add_nonvar([X], non(X)) :- !.
add_nonvar([X|L],(non(X),U)) :- add_nonvar(L,U).

delay(Exp,LX) :- 
    add_nonvar(LX,LL),
    Goal1 = (LL,Exp),
    Goal2 = (Goal1 -> true ; true),
    ( 
       Goal1 -> 
          true; 
       delay0(Goal2,LX)
    ).

cellit(Exp,LX) :- 
    add_nonvar(LX,LL),
    Goal1 = (LL,Exp),
    Goal2 = (Goal1 -> true ; true),
    ( 
       Goal1 -> 
          true; 
       cell0(Goal2,LX)
    ).
")

(<define> (delay0 e l)
  (<recur> lp ((l l) (hit? #f))
    (<<match>> (#:mode -) (l)	   
      ((x . l)
       (<let> ((x (<lookup> x)))
	 (cond
	  ((<var?> x)
	   (<=> x ,(make-delay e x))
	   (lp l #t))
	  ((delay? x)
	   (<let> ((ee (delay-exp x)))
	     (<if> (delayMatch e ee)
		   <cc>
		   (<set> (delay-var x)
			  (make-delay (vector (list #{,}# ee e))
				      (delay-var x))))
	     (lp l #t)))
	  (else
	   (lp l hit?)))))
      (()
       (when hit?)))))

(<define> (cell0 e l)
  (<recur> lp ((l l) (hit? #f))
    (<<match>> (#:mode -) (l)	   
      ((x . l)
       (<let> ((x (<lookup> x)))
	 (cond
	  ((<var?> x)
	   (<var> (u)
	     (<=> x ,(make-cell e x u)))
	   (lp l #t))
	  
	  ((delay? x)
	   (<let> ((ee (delay-exp x)))
	     (<var> (u)
	      (<set> (delay-var x)
		     (make-cell (vector (list #{,}# ee e))
				(delay-var x)
				u)))
	     (lp l #t)))

	  ((cell? x)
	   (<let> ((ee (delay-exp x)))
             (<set> (delay-var x)
                    (make-cell (vector (list #{,}# ee e))
			       (cell-var x)
			       (cell-val x)))
	     (lp l #t)))
	  (else
	   (lp l hit?)))))
      (()
       (when hit?)))))

(<define> (delayed_exp_p x)
  (<let> ((x (<lookup> x)))
    (when (delay? x)
      (<cc> (delay-exp x) (delay-var x)))))

(<define> (cell_exp_p x)
  (<let> ((x (<lookup> x)))
    (when (cell? x)
      (<cc> (cell-exp x) (cell-var x) (cell-val x)))))

(<define> (insert val var)
 (<or>
  (<and>
   (<values> (f v) (delayed_exp_p var))
   <cut>
   (<set> v val)
   (<if> (goal-eval f) <cc> <cc>))
  (<and>
   (<values> (f v cval) (cell_exp_p var))
   <cut>
   (<set> cval val)
   (<if> (goal-eval f) <cc> <cc>))
  (<=> var val)))

;; Examples
(compile-prolog-string
"
  plusx(X,Y,Z) :- ZZ is X + Y, pp(ZZ),insert(ZZ,Z).
  eplus(X,Y,Z) :- delay(plusx(X,Y,Z),[X,Y]).

  plusz(X,Y,Z) :- XX is val(X),YY is val(Y), ZZ is XX + YY, insert(ZZ,Z).
  cplus(X,Y,Z) :- cellit(plusz(X,Y,Z),[X,Y]).
")
 

#;
(compile-prolog-string
"
random(N,X,P) :- (get_fail(P), random(N,X)) ; nextRand(N,X,P).

adjust_window(0,L)     :- L=[]).
adjust_window(N,[_|L]) :- NN is N - 1, adjust_window(NN,L).
adjust_window(N,[]).

next_inc(N,NN,NN) :- NN is N + 1.
next_history(L,LL,M,X,LL) :- LL = [X|L], adjust_window(M,LL).
start_inc(_,N,N).
start_history(_,S) :- S=[].

mwop(Op, Seed, [A|B] , X) :-
   Op(A,Seed,Z),
   mwop(Op, Z, B, X).

mwop(_,X,[],X).
maxx(Z,[X|_],[Y|_])  :- Z is max(X,Y).
  

plusz(Z,X,Y) :- 
   delay_exp(plusz,Z,X,Y) -> true ; 
   (ZZ is X + Y   , force_exp(Z,ZZ)).

% M : x1 x2 x3 x4...  => q1 q2 q3 ...
      q1 = max(x1,x2), q2=max(x1,x2,x3), g3=max(x2,x3,x4) ...
% generera två slumpmässiga l1 l2, s.a. M(l2)>=M(l1), skriv ut M(l1) + M(l2)

start1([[],[],0]).
next1 -->
  {
     random(10,X1), 
     random(10,X2,P) 
  },

  +[
     next_history(3,[X1,V1]  ,LX1),
     next_history(3,[X2,V2,P],LX2),
     plusz(Z,Res)
  ],

  {
    mwop(maxz,0,LX1,M1), 
    mwop(maxz,0,LX2,M2),
    last([_,_,P],LX2),
    length(LX1,1) -> true ;
    ref([_,MM1,_],1,LX1),
    ref([_,MM2,_],1,LX2))
    (M2 < M1 -> fail_it(P) ;  true),
    force_exp(MM1,M1),
    force_exp(MM2,M2),
    plusz(V1,Z,V2,_),
    writez([X1,X2,Z],Res)
  }.
")  
