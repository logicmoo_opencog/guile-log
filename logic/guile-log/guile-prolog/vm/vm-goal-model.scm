(<define> (set_extended x)
  (<code> (set! unify_operators (combine_ops (<scm> x)))))

(<define> (gen x) (<=> x ,(gensym "disj")))
(<define> (gen-rec x) (<=> x ,(gensym "Rec")))

(<define> (get_consts x) (<=> x ,(get-consts)))

(compile-prolog-string "

new_var0(V,B,C) :-
  get_F(B,F), 
  add_var_f(V,B,F,C).


reverse_op(<,>).
reverse_op(>,<).
reverse_op(=<,>=).
reverse_op(>=,=<).
reverse_op(@<,@>).
reverse_op(@>,@<).
reverse_op(@=<,@>=).
reverse_op(@>=,@=<).
reverse_op(=:=,=:=).
reverse_op(=\\=,=\\=).

rev(X,Y) :-
  rev(X,[],Y).

rev([],X,X).
rev([A|X],Y,Z) :-
  YY = [A|Y],
  rev(X,YY,Z).

zero(V)    :- get_A(V,A),A=[[0|_]].

print([]).
print([X|L]) :- write(X),nl,print(L).

wrap(Code,V,[L,LL]) :-
  catch(Code,E,
     (  
        tfc(E),
        (
           E==#t -> 
             (
               get_P(P0),
               L=[[[cc,P0]|LL],LL]
             );
           E==c  -> 
             (
                get_C(V,[[[P0,TagC2],_]|_]),
                set_P(V,P0),
                L=[[[cut,P0,TagC2],[fail,TagC2]|LL],LL]
             );
           (
              get_P(V,P0),
              L=[[[fail,P0]|LL],LL]
           )
        )
     )).

-trace.
-extended(',',m_and,;,m_or,\\+,m_not).
compile_goal(Code,Iout):- !,
  compile_goal(Code,Iout,StackSize,Narg,Consts,#t).

compile_goal(Code,Iout,StackSize,Narg,Constants,Pretty) :- !,
  (
    Code = (F(|A) :- Goal) -> length(A,Narg) ;
    Code = (F     :- Goal) -> Narg = 0       ;
    throw(compiles_only_clauses(Code))
  ),
  make_vhash(HV),
  make_vhash(HC),
  init_vars,init_e,
  (var(Constants) -> init_const ; true),
  b_setval(pretty,#t),
  make_state(0,[[0,_,_]],[],[[0,_]],0,0,0,0,0,[HC,HV],[],V),
  wrap(compile_goal(Code,#t,V,[L,[]]),V,[L,[]]),!,
  print(L),nl,!,
  get_M(V,StackSize),
  handle_all(L,LL),
  %(var(Constants)-> get_consts(Constants);true),
  %print(LL),nl,!,
  (true -> Iout=LL ; (b_setval(pretty,#f),mmtr(LL,Iout))).

compile_goal(X,Tail,V,L) :- var_p(X),!,
  compile_goal(call(X),Tail,V,L).


compile_goal(pr(X),Tail,V,[L,L]) :- !,
  write(pr(X)),nl.

compile_goal(set_p(Q),Tail,V,[L,LLL]) :- !,
  get_P(V,P),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true),
  L=[[set_p,Q,P]|LL].

compile_goal(!,Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  get_C(V,C),
  C=[[P0,TagC2]|_],
  set_P(V,P0),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true),
  (Tail==#t -> L=[[cut,P0,TagC2],[cc,P0]|LL] ; L=[[cut,P0,TagC2]|LL]).

compile_goal(softie(A),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  tail(Tail,V,LL,LLL),
  get_P(V,P),
  (Tail==#t -> L=[[softie,A],[cc,P]|LL] ; L=[[softie,A]|LL]).

compile_goal(begin_att,Tail,V,[L,LLL]) :- !,
   check_tail(Tail),
   tail(Tail,V,LL,LLL),
   (
     Tail==#t -> L=LLL ;
     (
       get_A(V,A),
       A=[[Ai  | _] | _],
       set_A(V,[[AAi,AAx,AAt] | A]),
       AAi is Ai + 1,
       var_p(AAx),
       add_var(AAx,V,Tagx),
       tr('pre-unify',Pre),
       get_P(V,P),
       L = [[Pre,AAt,Tagx,P]|LL]
     )
   ).

compile_goal(end_att,Tail,V,[L,LL]) :- !,
   check_tail(Tail),
   get_AF(V, A, F),
   [[Ai,Ax,At]|AA]=A,
   set_A(V, AA),
   (Ai==0  -> throw(missmatching_begin_end_pair) ; true),
   var_p(Ax),
   add_var(Ax,V,Tag),
   (Tail==#t -> tr('post-unify-tail',Post) ; tr('post-unify',Post)),      
   (
     Tail==#t ->  LLL=LL ; 
     (
       At==#t ->
         (
           tr('post-unicall',PostCall),
           get_C(V,[C|_]),
           LLL=[[PostCall,C,F]|LL]
         ) ;
         LLL=LL
     )
   ),
   get_P(V,P),
   (
      At   == #t  ->  L = [[Post,Tag]|LLL] ; 
      Tail == #t  ->  L = [[cc,P]|LLL]       ; 
                      L = LLL
   ).  

compile_goal(pop(N),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  M is -N,
  push_v(M,V),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true),
  L=[[pop,N]|LL].

compile_goal(with_cut(C,CS),Tail,V,[L,LL]) :- !,
  var_p(C),
  var_p(CS),
  add_var(C ,V,TagC ),
  add_var(CS,V,TagCS),
  L=[[cutter,TagC,TagCS]|LL].

compile_goal((Args <= Goal),Tail,V,L) :- !,
  (listp(Args) -> true ; throw(not_proper_head(Args <= goal))),
  reverse(Args,AArgs),
  mg(cc(|Args),AArgs,Impr,0,N),
  NN is N + 3,
  link_l(L,L1,L2,L3),
  compile_goal(Goal,label(G,NN),V,L1),
  L2=[[[label,G]|U],U],
  set_S(V,0),
  push_v(NN,V),
  compile_goal((begin_att,Impr,pop(3),end_att),Tail,V,L3).

compile_goal((F(|ASrgs) :- Goal),Tail,V,L) :- !,
  rev(ASrgs,Args),
  (listp(Args) -> true ; throw(not_proper_head(F(|Args)))),
  reverse(Args,AArgs),
  mg(F(|Args),AArgs,Impr,0,N),  
  push_v(N,V),
  (
   get_A(V,[[0|_]|_]) ->
     wrap(compile_goal((move_args(0,N),begin_att,Impr,end_att,Goal),
                       Tail,V,L),V,L);
     wrap(compile_goal((move_args(0,N),begin_att,Impr,Goal),Tail,V,L),V,L)
  ).

compile_goal((F :- Goal),Tail,V,L) :- !,
  push_v(4,V),
  wrap(compile_goal((pop(4),Goal),Tail,V,L),V,L).

compile_goal(newtag_F(F),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,V,L,LL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true),
  gen(F).

compile_goal(extended_off,Tail,V,[L,L]) :- !,
    set_extended(#f).

compile_goal(move_args(N,N),Tail,V,[L,LL]) :- !,
    L=[['handle-spcc',N]|LL].

compile_goal(move_args(K,N),Tail,V,[L,LL]) :- !,
    L=[push_at(K)|L1],
    KK is K + 1,
    compile_goal(move_args(KK,N),Tail,V,[L1,LL]).

compile_goal(extended_on(|L),Tail,V,[L,L]) :- !,
  set_extended(L).

compile_goal(extended_on,Tail,V,[L,L]) :- !,
  set_extended([]).
compile_goal(collect_F(F),Tail,V,[L,L]) :-
  get_F(V,F).

compile_goal(Op(\"recur\",Args),Tail,V,[L,LL]) :-    
   Args = F(|U),
   Op=\"op2*\",!,
   get_line(U,X,Xin,N),
   reverse(X,XX),
   mg(recur(|U),XX,Impr,0,N),
   add_recur(F,A,N),
   push_args_args(#f,Xin,V,L,L1,_,_),
   touch_A(V),
   touch_Q(10,V),
   %gen_rec(Sym)
   %set_F(V,Sym)
   L1=[[label,A]|L2],
   compile_goal((begin_att,Impr,end_att),Tail,V,[L2,LL]).

compile_goal((X,Y),Tail,V,L) :- !,
  collect_conj((X,Y),Gs,[]),
  compile_conj(Gs,Tail,V,L).

compile_goal(m_or(fail,false),#t,V,[[[fail,P],[cc,P] |L], L]) :-
   get_P(V,P).
compile_goal(m_or(fail,false),#f,V,[[[fail,P]      |L], L]) :-
   get_P(V,P).
compile_goal(true,_,_,_) :- throw(#t).
compile_goal(';'(|X),Tail,V,[L,LLL]) :- !,
   write(disjTail(Tail)),nl,
   collect_disj(X,XX,[]),
   (
     XX=[]  -> L=[[false]|LL]                ;
     XX=[Z] -> compile_goal(Z,Tail,V,[L,LL]) ; 
     (
       get_AB2ESM(V,Aq,B2,E,S,M),
       get_F(V,F), 
       Q=[_,_],
       compile_disjunction(XX,#t,Aq,Ae,Out,Lab,LabA,Tail,S,U,B2,V,LM,Q,QQ),!,
       (zero(V) -> Tp is 0 ; Tp is 1),
       pushl_Q(V,QQ),
       (
         varx(Q) ->
            (
              var_p(Var1),
              var_p(Var2),
              add_var_f(Var1,V,F,LabA1),
              add_var_f(Var2,V,F,LabA2),
              LabA = [LabA1,LabA2],
              L   = [['newframe-light',LabA,Lab,Q] | LX]
            ); 
            (
              var_p(Var1),
              var_p(Var2),
              add_var_f(Var1,V,F,LabA1),
              add_var_f(Var2,V,F,LabA2),
              LabA = [LabA1,LabA2],
              L   = [[newframe,LabA,Lab,Q] | LX]   
            )
       ),
       LM = [LX,  [[label,Out] | LL]],
       get_EBH(V,Ed,B,H),
       add_missing_variables(H,U,Ed,Ed,EEd),!,
       BB2 is B2 \\/ EEd,
       EE is  E  \\/ EEd,
       set_B2E(V,BB2,EE)
     )
  ),
  get_P(V,PPP),
  (Tail==#t -> LL=[[cc,PPP]|LLL]; LL=LLL).

compile_goal(verbatim_call(X),Tail,V,[L,LL]) :- !,
   ( \\+var_p(X),
     ( 
       F(|Args)=X -> 
        (
          (var_p(F) ; isApply(Args)) -> fail ; true
        ) ; 
       true
     )
   ),
   !, 
   compile_goal(X,Tail,V,[L,LL]).

compile_goal(call(X),Tail,V,[L,LLL]) :- !,
   get_P(V,P0),
   (P0=ground(_) -> PP=1; PP=0),

   tail(Tail,V,LL,LLL),

   new_var0(VP,V,TagP1),
   new_var0(VC,V,TagC1),
   
   (
     (
       PP == 0, 
       set_P(V,ground(_)),
       compile_goal(call_(X,A,Al,[C0|_],Pre,Post,LP,#f),Tail,V,[L,LL]),
       Al=[A0,Cut_p],
       Cut_p == (#t)
     );
     (
       compile_goal(call_(X,A,Al,[C0|_],Pre,Post,LP,#f),Tail,V,[L,LL]),
       Al=[A0,Cut_p]
     )
   ),!,
     
   new_var0(VP,V,TagP2),
   new_var0(VC,V,TagC2),

   (
     (Cut_p == (#t), PP == 0) ->
       (
         Ground = ground(LFail),
         Pre  = ['store-pc'   , TagP1, TagC1        ],
         Post = [['goto-inst' , Out                 ],
                 [label       , LFail               ],
                 ['fail-pc'   , TagP1, TagC2        ],
                 [label       , Out                 ],
                 ['restore-c' , TagC2               ]|LP]
       ) ; 
     (
       Pre  = [true],
       Post = LP
     )
   ),
   (Tail == (#t) -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(call_(X,A0,Al,C0,Pre,Post,LP,Off),Tail,V,[L,LL]) :- !,
   L=[Pre|LX],
   get_C(V,C),
   (
      Off == #t -> 
        (
          get_AA(V,A),
          AA = [A0|A],
          set_AA(V,AA)
        );
      get_AA(V,[A0|_]) -> 
        true;
      A0=(#t)
   ),
   C  = [C0|_],
   CC = [Al|C],
   set_C(V,CC),
   compile_goal(X,#f,V,[LX,LLX]),
   (Off == #t -> set_AA(V,A) ; true),
   LLX = Post,
   set_C(V,C),
   LP=LL.

compile_goal((X =.. Y),Tail,V, L, LL) :- !,
  (var_p(X);constant(X)) ->
  (
     compile_arg(X,Branch,HC,HV,L,LLX),
     compile_imprint(Y,Branch,HC,HV,LLX,LLY),
     tr(=..,Op),
     LLY=[[Op]|LL]
  ).
  

compile_goal(set(V,X),Tail,V,[L,LL]) :- !,
  touch_Q(11,V),
  tr(set,Set),
  (var_p(X) -> true ; throw(no_var_in_set)),
  add_var(X,V,Tag),
  (isFirst(Tag) -> true ; force_variable(Tag)),
  push_args(X,V,L,LX),
  push_v(-1,V),
  get_P(V,P),
  (
    Tail==#t ->
       LX=[[Set,Tag],[cc,P]|LL];
       LX=[[Set,Tag]     |LL]  
  ).

compile_goal(once(G),Tail,V,[L,LLL]) :- !,
  tail(Tail,V,LL,LLL),
  
  get_P(V,P0), 

  A0=[P0,TagC2],

  ((nonvar(P0),P0=ground(_)) -> PP=1 ; PP=0),

  var_p(VP),
  var_p(VC),

  new_var0(VP,V,TagP1),
  new_var0(VC,V,TagC1),

  (
    (
       PP == 0,
       set_P(V,ground(_)),  
       compile_goal(call_(G,A,Al,[C0|_],Pre,Post,LP,#t),#f,V,[L,LL]),
       Al=[A0,Cut_p],             
       Cut_p == (#t)
    );
    (
       compile_goal(call_(G,A,Al,[C0|_],Pre,Post,LP,#t),#f,V,[L,LL]),
       Al=[A0,Cut_p]
    )
  ),!,
  
  set_P(V,P0),

  new_var0(VP,V,TagP2),
  new_var0(VC,V,TagC2),

  (
    Cut_p == (#t) ->
      (
        PP == 0 ->
          (
            Ground = ground(LFail),
            Pre  = ['store-pc', TagP1,TagC1 ],
            Post = [['goto-inst'  , Out          ],
                    [label        , LFail        ],
                    ['fail-pc'    , TagP2, TagC2 ],
                    [label        , Out          ],
                    ['restore-pc' , TagP2, TagC2 ]|LP]
          );
        (
          Pre  = [true],
          Post = LP
        )
      );
    (
      PP == 0 ->
        (
          Pre  = ['store-p', TagP1],
          Post = [['restore-p', TagP2]|LP]
        );
      (
        Pre  = [true],
        Post = LP
      )
    )
  ),
  (Tail == (#t) -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal((X->Y),Tail,V,L) :- !,
  compile_goal((once(X),Y),Tail,V,L).

compile_goal(\\+\\+\\+X,Tail,V,[L,LL]) :- !,
   compile_goal(\\+X,Tail,V,[L,LL]).

compile_goal(\\+\\+X,Tail,V,[L,LLL]) :- !,
   get_P(V,P0), 
   ((nonvar(P0),P0=ground(_)) -> PP=1 ; PP=0),

   A0 = [P0,TagC2],
   tail(Tail,V,LL,LLL),

   get_QAESBB2(V,Q,AA,E,S,B,B2),
   set_QAE(V,[],[[0|_]],0),
   
   var_p(VP),
   var_p(VC),   
   var_p(VS), 

   get_F(V,F),

   add_var_f(VP,V,F,TagP1),
   add_var_f(VS,V,F,TagS1),
   add_var_f(VC,V,F,TagC1),

   compile_goal(call_(X,AQ,Ad,[C0|_],Pre,Post,LP,#t),#f,V,[L,LL]),
   set_P(V,P0),

   add_var_f(VP,V,F,TagP2),
   add_var_f(VS,V,F,TagS2),
   add_var_f(VC,V,F,TagC2),

   set_QAESBB2(V,Q,AA,E,S,B,B2),
   (A=[[0|_]|_] -> Tp is 0 ; Tp is 1),
   Ad=[A0,Cut_p],

   ( 
     AQ==(#t) ->
       (
         (Cut_p == #t, PP == 0) ->
           (
             Pre  = ['newframe-psc', TagP1, TagS1, TagC1],
             Post = [['unwind-psc', TagP2, TagS2, TagC2]|LP]
           ) ;
         (
           Pre  = ['newframe-ps', TagP1, TagS1],
           Post = [['unwind-ps', TagP2, TagS2]|LP]
         )   
      );
    (
      (Cut_p == #t, PP == 0) ->
        (
           Pre  = ['store-pc', TagP1, TagC1],
           Post = ['restore-pc', TagP2, TagC2]
        );
      (
         Pre  = ['store-p',TagP1],
         Post = [['restore-p',TagP2]|LP]
      )
    )
 ),  
 (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(\\+X,Tail,V,[L,LLL]) :- !,  
  check_tail(Tail),
  tail(Tail,V,LL,LLL),

  get_P(V,P0),
  ((nonvar(P0),P0 = ground(_)) -> PP=1 ; PP=0),
  
  new_var0(VS,V,TagS1),
  new_var0(VP,V,TagP1),
  new_var0(VC,V,TagC1),

  get_QAESB(V,Q,A,E,S,B),

  
  ifc(
  (
     set_QAE(V,[],[[0|_]],0),
     set_P(V,ground(LFail)),
     compile_goal(call_(X,AQ,Ad,[C0|_],Pre,Post,LP,#t),#f,V,[L,LL])
  ),Er,
  (   
      tfc(Er),
      (Er==#t -> throw(#f) ; throw(#t))
  ),
  (
    new_var0(VS,V,TagS2),
    new_var0(VP,V,TagP2),
    new_var0(VC,V,TagC2),

    set_QAESB(V,Q,A,E,S,B),
  
    Ad=[A0,Cut_p],
    
    ( 
      AQ == (#t) ->
       (
         Cut_p == (#t) ->
           (
             PP == 0 ->
                (
                  Pre  = ['newframe-psc',TagP1,TagS1,TagC1], 
                  Post = [['fail-pc',TagP2,TagC2],
                          [label,LFail],
                          ['unwind-psc',TagP2,TagS2,TagC2]|LP]
                );
             (
                Pre  = ['newframe-ps',TagP1,TagS1], 
                Post = [['fail-p0',P0],
                        [label,LFail],
                        ['unwind-ps',TagP2,TagS2]|LP]
             )
           );
         (
           PP == 0 ->
             (
               Pre  = ['newframe-ps',TagP1,TagS1],
               Post = [['fail-p',TagP2],
                       [label,LFail],
                       ['unwind-ps',TagP2,TagS2]|LP]
             );
           (
             Pre  = ['newframe-s',TagS1],
             Post = [['fail-p0',P0],
                     [label,LFail],
                     ['unwind-s',TagS2]|LP]
           )
         )
       );
     (
       Cut_p == (#t) ->
         (
            PP == 0 ->
              (
                 Pre  = ['store-pc',TagP1,TagC1], 
                 Post = [['fail-pc',TagP2,TagC2],
                         [label,LFail],
                         ['restore-pc',TagP2,TagC2]|LP]
              );
            (
              Pre  = ['store-p',TagP1], 
              Post = [['fail-p0',P0],
                      [label,LFail],
                      ['restore-p',TagP2]|LP]
            )
         );
       (        
         PP == 0 ->
           (
             Pre  = ['store-p',TagP1],
             Post = [['fail-p',TagP2],
                     [label,LFail],
                     ['restore-p',TagP2]|LP]
           );
         (
           Pre  = [true],
           Post = [['fail-p0',P0],
                   [label,LFail]|LP]
         )
       )
     )
   )
  )),
  (Tail == (#t) -> (touch_Q(7,V),touch_A(V)) ; true).


compile_goal((m_and(Op,m_or(<,>,=<,>=,=:=,=\\=,@<,@=<,@>,@>=)))(X,Y),
              Tail,V,[L,LLL]) :- !,
  write(compTail(1,Op,Tail)),nl,
  check_tail(Tail),
  tail(Tail,V,LL,LLL),
  get_P(V,P),
  ifc(compile_scm(X,V,L,LX),EX,
     (
       ifc(compile_scm(Y,V,LX,LY),EY,
          (
             call(Op(EX,EY)) -> throw(#t) ; throw(#f)
          ),
          (
              push_v(-1,V),
              binop(Or,O),
              tr(O,OO),
              LY=[[OO,P]|LL],
              L=[['push-instruction',EX]|LX]
         ))
     ),
     ifc(compile_scm(Y,V,LX,LY),EY,
       (
         push_v(-1,V),
         binop(Op,O),
         tr(O,OO),
         LX=[['push-instruction',EY],[OO,P]|LL]
       ),
       (
         push_v(-2,V),
         binop(Op,O),
         LY=[[O,P]|LL]
       ))),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).


compile_goal((m_and(Op,m_or(\"op2<\",\"op2>\",\"op2=<\",\"op2>=\",\"=:=\",\"=\\\\=\",\"@<\",\"@=<\",\"@>\",\"@>=\")))(Y,X),
              Tail,V,[L,LLL]) :- !,
  write(compTail(2,Op,Tail)),nl,
  check_tail(Tail),
  get_P(V,P),
  ifc(compile_scm(X,V,L,LX),EX,
     (
       ifc(compile_scm(Y,V,L,LY),EY,
          (
             call(Op(EX,EY)) -> throw(#t) ; throw(#f)
          ),
          (
              LY=[['push-instruction',EX]|LLY],
              caller0(Op,2,Tail,V,[LLY,LLL])
         ))
     ),
     ifc(compile_scm(Y,V,LX,LY),EY,
       (
          LX=[['push-instruction',EY]|LLX],
          caller0(Op,2,Tail,V,[LLX,LLL])
       ),
       (
          caller0(Op,2,Tail,V,[LY,LLL])
       ))).


compile_goal(X is Y,Tail,V,L) :- !,
    (zero(V) -> 
      compile_goal(iss(X,Y),Tail,V,L);
      compile_goal(iss(X,Y),Tail,V,L)).

compile_goal(iss(X,Y),Tail,V,[LA,LLL]) :- !,
  check_tail(Tail),
  compile_goal(begin_att,#f,V,[LA,L]),
  (
    instruction(Y) ->
    (
      instruction(X) ->
      (
        X==Y -> throw(#t) ; throw(#f)
      );
      (
        tr('unify-instruction-2',Unify),
        var_p(X),
        add_var(X,V,Tag),
        (isFirst(Tag) -> true ; touch_A(V)),
        L=[[Unify,Tag,Y,#t]|LL]
      )
    );    
    ifc(compile_scm(Y,V,L,LX),EY,compile_goal(X is EY, Tail, V, [L,LL]),
    (
      var_p(X)   ->   
       ( 
         add_var(X,V,Tag),
         (isFirst(Tag) -> true ; touch_A(V)),
         %(isFirst(Tag) -> true ; touch_Q(12,V)),
         push_v(-1,V),
         tr(unify,Unify),
         LX=[[unify,Tag,#t]|LL]
       );
       (
         (
          constant(X) ->
             tr('equal-constant',Equal) ; 
          tr('equal-instruction',Equal)
         ),
         push_v(-1,V),
         LX=[[Equal,X]|LL]
       )
     ))),
  compile_goal(end_att,Tail,V,[LL,LLL]).
    
compile_goal(unify_with_occurs_check(X,Y),Tail,V,L) :- !,
  touch_Q(13,V),
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,uni_0(X,Y),end_att),Tail,V,L);
      compile_goal(uni_0(X,Y),Tail,V,L)
  ).

compile_goal(uni_0(X,Y),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  compile_unify(X,Y,V,[L,LLL],0),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).


compile_goal(X = Y,Tail,V,L) :- !,
  touch_Q(14,V),
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,uni_x(X,Y),end_att),Tail,V,L);
      compile_goal(uni_x(X,Y),Tail,V,L)
  ).

compile_goal(uni_x(X,Y),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  compile_unify(X,Y,V,[L,LL],#t),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(X == Y,Tail,V,L) :- !,
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,unim(X,Y),end_att),Tail,V,L);
      compile_goal(unim(X,Y),Tail,V,L)
  ).

compile_goal(uni(X,Y),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  compile_unify(X,Y,V,[L,LL],#t),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(unim(X,Y),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  compile_unify(X,Y,V,[L,LL],#f),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(imprint(X,M),Tail,V,[L,LLL]) :- !,
  check_tail(Tail),
  compile_imprint(X,V,L,LL,M),
  tail(Tail,V,LL,LLL),
  (Tail==#t -> (touch_Q(7,V),touch_A(V)) ; true).

compile_goal(m_and(X,F(|AArgs)),Tail,V,L) :- !,
   (
    (var_p(F) ; isApply(AArgs)) -> compile_goal(call(X),Tail,V,L) ;
    (rev(AArgs,Args), caller(F,Args,Tail,V,L)),
    set_P(V,\"procedure\")
   ).

compile_goal(F,Tail,V,L) :-
  constant(F) -> (!,caller(F,[],Tail,V,L)).

compile_goal(X,_,_,_) :-
  throw(failed_compile_goal(X)).
      
isApply(X) :- var_p(X),!.
isApply([X|L]) :- isApply(L).

ncons(X,N) :-
  ncons(X,0,N).

")


(set! (@@ (logic guile-log guile-prolog vm vm-var) compile_goal)
  compile_goal)
