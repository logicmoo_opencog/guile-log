(compile-prolog-string "
%newvars needs to be variables

ttt('newframe-light', newframe).
ttt('unwind-light'  , unwind).
ttt(newframe, 'newframe-light').
ttt(unwind  , 'unwind-light').

newv([]).

newv([[newvar,[[V,S],N,F]]|L]) :- !,
   (N==1 -> true ; S=#t).

newv([_|L]) :- newv(L).

not_first([_,_,F|_]) :- F == #t -> false ; true.

handle_all(L,Lout) :-
  newv(L),
  handle_all(L,0,_,Lout,[]).

handle_all([],I,I,L,L).
handle_all([X|Y],I,II,L,LL) :- !,
  handle(X,I,I1,L,L1),!,
  handle_all(Y,I1,II,L1,LL).


chech_push(F) :-
     F==#t ->  
       throw(first_variable_in_scheme_context); 
     true.

-trace. 
-extended.
handle([true],I,I,L,L) :- !.

handle([cut,P,[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :- !,
  (
    P=ground(_) ->
      true;
    (
      new_var(V1,Q1,S),
      (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
      cut(P,E1,I,II,L,LL)
    )
  ).

handle([cut,P,false],I,II,L,LL) :- !,
  (
    P=ground(_) ->
      true;
    (
      cut(P,false,I,II,L,LL)
    )
  ).

handle(['cc',P],I,II,L,LL) :- !,
   'cc-call'(P,I,II,L,LL).

handle([('unify-constant';'unify-instruction'),C,M],I,II,L,LL) :- !,
   'unify-instruction'(C,M,I,II,L,LL).

handle([\"label\",C],I,I,L,LL) :- !,
   I=II,
   inlabel(C,L,LL).

handle([\"scm-call\",N],I,II,L,LL) :- !,
   II is I - N,
   NN is N + 1,
   scmcall0(NN,II,I,L,LL).
    

handle([\"label\",C,D],I,I,L,LL) :- !,
   I=II,
   inlabel(C,D,L,LL).

handle(['clear-sp'],I,0,L,LL) :- !,
   'clear-sp'(I,0,L,LL).

handle(['handle-spcc',C],I,II,L,LL) :- !,
   'handle-spcc'(C,I,II,L,LL).

handle([\"call\",N],I,II,L,LL) :- !,
   'in-call'(N,I,II,L,LL).

handle([\"tail-call\",N],I,II,L,LL) :- !,
   'in-tailcall'(N,I,II,L,LL).

handle(['push-3',P,N],I,II,L,LL) :- !,
   'push-3'(P,N,I,II,L,LL).

handle([\"push-s\"],I,II,L,LL) :- !,
   'push-s'(I,II,L,LL).

handle(['pushtail-3',P],I,II,L,LL) :- !,
   'pushtail-3'(P,I,II,L,LL).

handle([seek,N],I,II,L,LL) :- !,
   seek(N,I,II,L,LL).

handle(['mk-cons'],I,II,L,LL) :- !,
   'mk-cons'(I,II,L,LL).

handle(['mk-fkn',N],I,II,L,LL) :- !,
   'mk-fkn'(N,I,II,L,LL).

handle(['mk-curly'],I,I,L,LL) :- !,
   'mk-curly'(I,L,LL).

handle(['icons!'],I,II,L,LL) :-
  'icons!'(I,II,L,LL).

handle(['ifkn!'],I,I,L,LL) :-
  'ifkn!'(I,L,LL).

handle(['icurly!'],I,I,L,LL) :-
  'icurly!'(I,L,LL).

handle(['icons'],I,II,L,LL) :-
  'icons'(I,II,L,LL).

handle(['ifkn'],I,I,L,LL) :-
  'ifkn'(I,L,LL).

handle(['icurly'],I,I,L,LL) :-
  'icurly'(I,L,LL).

handle([<,P],I,II,L,LL) :-
  lt(P,I,II,L,LL).

handle([>,P],I,II,L,LL) :-
  gt(P,I,II,L,LL).

handle([=<,P],I,II,L,LL) :-
  le(P,I,II,L,LL).

handle([>=,P],I,II,L,LL) :-
  ge(P,I,II,L,LL).

handle(['op2+'],I,II,L,LL) :-
  plus(I,II,L,LL).

handle(['op2-'],I,II,L,LL) :-
  minus(I,II,L,LL).

handle(['op1_-'],I,II,L,LL) :-
  minus1(I,II,L,LL).

handle([*],I,II,L,LL) :-
  mul(I,II,L,LL).

handle([/],I,II,L,LL) :-
  divide(I,II,L,LL).

handle([softie,A],I,II,L,LL) :- !,
   (
     (var(A) ; number(A)) ->
       softie(A,I,II,L,LL) ;
     (
       A=[_,[[S,V,Q],N,F|_]],
       new_var(V,Q,S),
       (V=[VC|_] -> E=var(VC) ; E=svar(V)),
       'softie-light'(E,I,II,L,LL)
     )
   ).

handle(push_at(K),I,II,L,LL) :- !,
   push_at(K,I,II,L,LL).

handle([\"set_p\",Q,P],I,II,L,LL) :- !,
   (
     varx(Q) ->
       (
          I=II,L=LL
       );
     (
       set_p(P,I,II,L,LL)
     )
   ).


handle([(F,('newframe-light';'unwind-light')),
           (U,[[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]]),A,QQ],I,II,L,LL) :- !,
  (
     varx(QQ) ->
       (
          new_var(V1,Q1,S1),
         (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
          F(E1,A,I,II,L,LL)
       );
     (
       ttt(F,FF),
       handle([FF,U,A,QQ],I,II,L,LL)
     )
  ).

handle([(F,('newframe';'unwind')),
           (U,[[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]]),A,QQ],I,II,L,LL) :- !,
  (
     \\+(varx(QQ)) ->
       (
          new_var(V1,Q1,S1),
          new_var(V2,Q2,S2),
         (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
         (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),
          F(E1,E2,A,I,II,L,LL)
       );
     (
       ttt(F,FF),
       handle([FF,U,A,QQ],I,II,L,LL)
     )
  ).

handle(['unwind-light-tail',(U,[[[S,V,Q],N,F|_],_]),QQ],I,II,L,LL) :- !,
  new_var(V,Q,S),
  (V=[VC|_] -> E=var(VC) ; E=svar(V)),
  (
     varx(QQ) ->
        'unwind-light-tail'(E,I,II,L,LL);
     handle(['unwind-tail',U,QQ],I,II,L,LL)
  ).


handle([(X,('goto-inst';'store-state';'post-q')),N],I,II,L,LL) :- !,
   X(N,I,II,L,LL).

handle([(X,('unwind-tail')),
        [[[S1,V1,Q1],N1,F1|_],
         [[S2,V2,Q2],N2,F2|_]], Q],I,II,L,LL) :- !,
    write(unwindtail),nl,
    new_var(V1,Q1,S1),
    (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
    write(unwindtail),nl,
    new_var(V2,Q2,S2),
    (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),
    write(unwindtail),nl,
    X(E1,E2,I,II,L,LL).

handle([(X,('unwind-p')),
             [[S1,V1,Q1],N1,F1|_]] ,I,II,L,LL) :- !,

    new_var(V1,Q1,S1),
    (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
    X(E1,I,II,L,LL).

handle(['unwind-psc',
        [[S1,V1,Q1],N1,F1|_],
        [[S2,V2,Q2],N2,F2|_],
        [[S3,V3,Q3],N3,F3|_]] ,I,II,L,LL) :- !,

    new_var(V1,Q1,S1),
    new_var(V2,Q2,S2),
    new_var(V3,Q3,S3),
    (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
    (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),
    (V3=[VC3|_] -> E3=var(VC3) ; E3=svar(V3)),
    'unwind-psc'(E1,E2,E3,I,II,L,LL).

handle([(X,('newframe-ps';'newframe-pc';
            'unwind-ps';'unwind-pc')),
        [[S1,V1,Q1],N1,F1|_],
        [[S2,V2,Q2],N2,F2|_]] ,I,II,L,LL) :- !,
    new_var(V1,Q1,S1),
    (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
    new_var(V2,Q2,S2),
    (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),
    X(E1,E2,I,II,L,LL).

handle([(X,('newframe-psc')),
             [[S1,V1,Q1],N1,F1|_],
             [[S2,V2,Q2],N2,F2|_],
             [[S3,V3,Q3],N3,F3|_]] ,I,II,L,LL) :- !,

    new_var(V1,Q1,S1),
    (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),
    new_var(V2,Q2,S2),
    (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),
    new_var(V3,Q3,S3),
    (V3=[VC3|_] -> E3=var(VC3) ; E3=svar(V3)),
    X(E1,E2,E3,I,II,L,LL).


handle([(F,(           'newframe-negation' ; 'post-negation'
                                           ; 'unwind-negation'
                     ; 'post-s')),
        [[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],B],I,II,L,LL) :- !,
  new_var(V1,Q1,S1),
  (V1=[VC1|_] -> E1=var(VC1) ; E1=svar(V1)),

  new_var(V2,Q2,S2),
  (V2=[VC2|_] -> E2=var(VC2) ; E2=svar(V2)),

  F(E1,E2,B,I,II,L,LL).

handle([(F,('store-ps';'fail-psc')),
       [[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 (V2=[V2C|_] -> E2=var(V2C) ; E2=svar(V2)),
 F(E1,E2,I,II,L,LL).

handle(['store-p',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 'store-p'(E1,I,II,L,LL).


handle(['store-s',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 'store-s'(E1,I,II,L,LL).


handle(['store-pc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !, 
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 (V2=[V2C|_] -> E2=var(V2C) ; E2=svar(V2)),
 'store-pc'(E1,E2,I,II,L,LL).

handle(['restore-p',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 write(1),
 new_var(V1,Q1,S1),
 write(1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 write(1),
 'restore-p'(E1,I,II,L,LL).


handle(['restore-s',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 'restore-s'(E1,I,II,L,LL).

handle(['restore-pc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 (V2=[V2C|_] -> E2=var(V2C) ; E2=svar(V2)),
 'restore-pc'(E1,E2,I,II,L,LL).

handle(['restore-c',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :- !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 'restore-c'(E1,I,II,L,LL).

handle([(F,('softie-pc';'post-sc')),[[S1,V1,Q1],N1,F1|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 F(E1,I,II,L,LL).

handle(['fail-p0',P],I,II,L,LL) :-
  'fail-p0'(P,I,II,L,LL).

handle(['fail-pc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 (V2=[V2C|_] -> E2=var(V2C) ; E2=svar(V2)),
 'fail-pc'(E1,E2,I,II,L,LL).

handle(['fail-p',[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 'fail-p'(E1,I,II,L,LL).

handle([(F,('softie-psc';'softie-ps';cutter)),
           [[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=var(V1C) ; E1=svar(V1)),
 (V2=[V2C|_] -> E2=var(V2C) ; E2=svar(V2)),
 F(E1,E2,I,II,L,LL).

handle([pop,N],I,II,L,LL) :-
  pop(N,I,II,L,LL).

handle(['post-c',C],I,II,L,LL) :- !,
 'post-c'(C,I,II,L,LL).

handle(['post-c',C,T,A],I,II,L,LL) :- !,
  (
    A==#t ->
      handle(['post-sc',T,C],I,II,L,LL);
   'post-c'(C,I,II,L,LL)
  ).

handle(['push-instruction',C],I,II,L,LL) :- !,
 'push-instruction'(C,I,II,L,LL).

handle([label,N,[_,Tags]],I,II,L,LL) :- !,
  L=[N|_],
  addvs(Tags,L,LL).

handle([\"post-call\",A,P],I,II,L,LL) :- !,
  'in-post-call'(I,II,L,LL).

handle((X,['post-unicall',A,P]),I,II,L,LL) :- !,
  get_nsvars(P,N),
  set(P,N),
  II is I + 3, L=[X|LL].
  
handle(['newvar', _],L,L) :- !.

handle(['pre-unify',At,Vx,P],I,II,L,LL) :- !,
 Vx=[[S,V,Q],N,F|_],
 (  
   N==1 -> 
      throw(begin_with_no_end) ;
   isCplx(At) -> 
      (
        new_var(V,Q,S),
        (V=[VC|_] -> E=var(VC) ; E=svar(V)),
        set_p(P,I,I1,L,L1),
        'pre-unify'(E,I1,II,L1,LL)
      ) ;
   (L=LL,I=II)
 ).

handle(['post-unify-tail',[[S,V,Q],N,F|_]],I,II,L,LL) :- 
 !,
 (
   F==#t -> throw(end_with_no_begin) ;
   (
     new_var(V,Q,S),
     (V=[VC|_] -> E=var(VC) ; E=svar(V)),
     'post-unify-tail'(E,I,II,L,LL)
   )
 ).

handle(['post-unify',[[S,V,Q],N,F|_]],I,II,L,LL) :- 
 !,
 (
   F==#t -> throw(end_with_no_begin) ;
   (
     (get_nsvars(Q,M) -> true ; M=0),
     new_var(V,Q,S),
     (V=[VC|_] -> E=var(VC) ; E=svar(V)),
     'post-unify'(E,M,I,II,L,LL)
   )
 ).

handle([set,(W,[[S,V,Q],N,F|_])],I,II,L,LL) :- !,
  (
    (F=#t,N=1) -> (L=LL)                                ;
     S=#t      -> (new_var(V,Q,S),ggset(V,I,II,L,LL)) ;
                  handle([unify,W,#t],I,II,L,LL)
  ).

handle(['push-variable',[[S,V,Q],N,F|_]],I,II,L,LL) :- !,
  (
    (F==#t,N==1) -> pushv(#f,L,LL);
     F==#t       -> (new_var(V,Q,S), pushv(V,L,LL));
    (
      new_var(V,Q,S),
      (V=[VC|_] -> QQ=svar(VC) ; QQ=var(V)),
      'push-variable'(QQ,I,II,L,LL)
    )
  ).

handle(['push-variable-scm',[[S,V,Q],N,F|_]],I,II,L,LL) :- !,
  (
     F==#t        ->  
       throw(first_variable_in_scheme_context);
     (
        new_var(V,Q,S),
        (V=[VC|_] -> QQ=svar(VC) ; QQ=var(V)),
        'push-variable-scm'(QQ,I,II,L,LL)
     )
  ).

handle([comp,CMP,[[S1,V1,Q1],N1,F1|_],
                 [[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :- !,
    chech_push(F1),
    chech_push(F2),
    new_var(V1,Q1,S1),
    new_var(V2,Q2,S2),
    code([comp,CMP,V1,V2],Code,Q,Comp),
    Comp(Code,Q,I,II,L,LL).

handle([bin,Op,[[S1,V1,Q1],N1,F1|_],
               [[S2,V2,Q2],N2,F2|_],
               [[S3,V3,Q3],N3,F3|_]],I,II,L,LL) :- !,
    chech_push(F1),
    chech_push(F2),
    new_var(V1,Q1,S1),
    new_var(V2,Q2,S2),    
    (
      (F3==#t,N3==1) ->
        (
          gtrue(L,LL)
        );
      (F3==#t,S3==#t) ->
        (
          new_var(V3,Q3,S3),
          code([bin,Op,V1,V2,V3,3],K,Add),
          Add(K,L,LL)
        );
      F3==#t ->
        (
           new_var(V3,Q3,S3),
           code([bin,Op,V1,V2,V3,0],K,Add),
           Add(K,L,LL)
        );
      N3==1 ->
      (
        new_var(V3,Q3,S3),
        code([bin,Op,V1,V2,V3,1],5,Add),
        Add(K,L,LL)
      ) ;
      (
        new_var(V3,Q3,S3),
        code([bin,Op,V1,V2,V3,1],K,Unify),
        Unify(K,L,LL)
      )
    ).

handle([(Kind,(ibin;bini)),Op,X,
            [[S2,V2,Q2],N2,F2|_],
            [[S3,V3,Q3],N3,F3|_]],I,II,L,LL) :- !,
    chech_push(F2),
    new_var(V2,Q2,S2),
    (
      (F3==#t,N3==1) ->
        (
          gtrue(L,LL)
        );
      (F3==#t,S3==#t) ->
        (
          new_var(V3,Q3,S3),
          code([Kind,Op,V2,V3,3],K,Add),
          Add(X,K,L,LL)
        );
      F3==#t ->
        (
           new_var(V3,Q3,S3),
           code([Kind,Op,V2,V3,0],K,Add),
           Add(X,K,L,LL)
        );
      N3==1 ->
      (
        new_var(V3,Q3,S3),
        code([Kind,Op,V2,V3,5],K,Add),
        Add(X,K,L,LL)
      ) ;
      (
        new_var(V3,Q3,S3),
        code([Kind,Op,V2,V3,1],K,Unify),
        Unify(X,K,L,LL)
      )
    ).


handle([unify,[[S,V,Q],N,F|_],M],I,II,L,LL) :- !,
    (
      (F==#t,N==1) ->
        (
          (M==#t ; M==0) -> 
              pop(I,II,L,LL);
          gfalse(I,II,L,LL)
        );
      (F==#t,S==#t) ->
        (
          M=#f -> gfalse(I,II,L,LL);
          (
            new_var(V,Q,S),
            code([unify,M,V,#t],K,VV,Unify),
            Unify(K,VV,I,II,L,LL)
          )
        );
      (F==#t) ->
        (
          M=#f -> gfalse(I,II,L,LL);
          (
            new_var(V,Q,S),
            code([unify,M,V,#f],K,VV,Unify),
            Unify(K,VV,I,II,L,LL)
          )
        );
      (N==1) ->
      (
        code([unify,M,V,1],K,VV,Unify),
        Unify(K,VV,I,II,L,LL)
      ) ;
      (
        code([unify,M,V,0],K,VV,Unify),
        Unify(K,VV,I,II,L,LL)
      )
   ).

handle([U,[[S,V,Q],N,F|_],A,M],I,II,L,LL) :- !,
 ((U='unify-constant-2' ; U='unify-instruction-2') ->
  ( 
    regconst(A,XX),
    (
     (F==#t,N==1)  -> (L=LL,I=II) ;
     (F==#t,S==#t) ->
      (
        new_var(V,Q,S),
        (V=[VC|_] -> E=svar(VC) ; E=var(V)),
        U(M,XX,E,#t,I,II,L,LL)
      );
      F==#t -> 
      (
        new_var(V,Q,S),
        (V=[VC|_] -> E=svar(VC) ; E=var(V)),
        U(M,XX,E,#f,I,II,L,LL)
      ) ;
      N==1 ->
      (
        new_var(V,Q,S),
        (V=[VC|_] -> E=svar(VC) ; E=var(V)),
        U(M,XX,E,1,I,II,L,LL)
      ) ;        
      (
        new_var(V,Q,S),
        (V=[VC|_] -> E=svar(VC) ; E=var(V)),
        U(M,XX,E,0,I,II,L,LL)
      )
     )        
  );
 (U='unify-2') ->
   (
     A=[[S2,V2,Q2],N2,F2|_],
     (
       ((F==#t,N==1) ; (F2==#t,N2==1)) -> 
          (
            LL=L 
          ) ;
       (F==#t,S==#t,F2==#t,S2==#t) ->
          (
            M=#f -> gfalse(L,LL);
            (
              new_var(V,Q,S),new_var(V2,Q2,S2),
              code([U,M,V,#t,V2,#t],K),
              U(K,L,LL)
            )
          );
       (F==#t,S==#t,F2==#t) ->
          (
            M=#f -> gfalse(L,LL);
            (
              new_var(V,Q,S), new_var(V2,Q2,S2),
              code([U,M,V,#t,V2,#f],K),
              U(K,L,LL)
            )
          );
       (F==#t,F2==#t,S2==#t) ->
          (
            M=#f -> gfalse(L,LL);
            (
              new_var(V,Q,S), 
              new_var(V2,Q2,S2),
              code([U,M,V,#f,V2,#t],K),
              U(K,L,LL)
            )
          );
       (F==#t,F2==#t) ->
          (
            M=#f -> gfalse(L,LL);
            (
              new_var(V,Q,S), 
              new_var(V2,Q2,S2),
              code([U,M,V,#f,V2,#t],K),
              U(K,L,LL)
            )
          );
       F==#t ->
         (
            M=#f -> gfalse(L,LL) ;
            (
              new_var(V,Q,S),
              new_var(V2,Q2,S2),
              (N2 == 1 -> K=1 ; K=0),
              code([U,M,V,#f,V2,K],Code),
              U(Code,L,LL)
            )
         );
       F2==#t ->
         (
            M=#f -> gfalse(L,LL);
            (
              new_var(V2,Q2,S2),
              (N == 1 -> K=1 ; K=0),
              code([U,M,V,K,V2,#f],Code),
              U(Code,L,LL)
            )
         );
       (
         (N  == 1 -> K1=1 ; K1=0),
         (N2 == 1 -> K2=1 ; K2=0),
         code([U,M,V,K1,V2,K2],Code),
         U(Code,L,LL)
       )          
     )
   )).


handle(X,L,LL) :- !,
   (
    X=[A]   -> (U=[]) ;
    X=[A,N] -> 
      (regconst(N,J),U=[J]);
    X=[A,N,M] ->
      (regconst(N,J1),regconst(M,J2),U=[J1,J2]);
    X=[A,N,M,O] ->
      (regconst(N,J1),regconst(M,J2),regconst(O,J3),U=[J1,J2,J3]);
    X=[A,N,M,O,P] ->
      (
        regconst(N,J1),regconst(M,J2),regconst(O,J3),regconst(P,J4),
        U=[J1,J2,J3,J4]
      )
   ), 
   tr(A,AA),!,
   append(U,[L,LL],UU),
   AA(|UU).

-trace.
code([unify,M,V,K],Code,Q,Action) :- !,
  (
    (M\\=#f,K=#f) ->
      (
        V=[U|_] -> 
          (Action = 'sp-move', Q = svar(U));
          (Action = 'sp-move', Q = var(V))
       ) ;
     (
        Action=unify,
        (
           M = #f -> MC=2 ;
           M = #t -> MC=3 ;
           MC=M
        ),
        (V=[VC|_] -> (A=1,Q=svar(VC)) ; (Q=var(V), A=0)),
        (
           K = #f -> KC=2 ;
           K = #t -> KC=3 ;
           KC=K
        ),
        U    is A + KC << 1 + VC << 3,
        Code is MC + U << 2
      )
    ).

code(['unify-2',M,V1,K1,V2,K2],Code,Q1,Q2) :- !,
  (
    M = #f -> MC=2 ;
    M = #t -> MC=3 ;
    MC=M
  ),
  (V1=[V1C|_] -> (A1=1,Q1=svar(V1C)) ; (V1=V1C, A1=0, Q1=var(V1))),
  (V2=[V2C|_] -> (A2=1,Q2=svar(V2C)) ; (V2=V2C, A2=0, Q2=var(V2))),
  (
    K1  = #f -> K1C=2 ;
    K1  = #t -> K1C=3 ;
    K1C = K1
  ),
  (
    K2  = #f -> K2C=2 ;
    K2  = #t -> K2C=3 ;
    K2C = K2
  ),
  U1   is A1 + K1C << 1 + V1C << 3,
  U2   is A2 + K2C << 1 + V2C << 3,
  Code is  MC + U1 << 2 + U2 << (2 + 24).

code([bin,Op,V1,V2,V3,K3],Code,Action) :-
 (
  V1=[V1C|_] ->
    (
       V2=[V2C|_] ->
         (
            V3 = [V3C|_] ->
               (
                 K3 = #f ->
                   (
                     !,
                     code3(V1C,V2C,V3C,Code),
                     binss2(Op,Action)
                   ) 
               ) 
         )
    )
 ).

code([bin,Op,V1,V2,V3,K3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  (V3=[V3C|_] -> A3=1 ; (V3=V3C, A3=0)),
  KC = K3,
  A    is A1 + A2 << 1 + A3 << 2 + KC << 3,
  Code is V1C + V2C << 16 + V3C << 32 + A << 48,
  binxx2(Op,Action).

code([bini,OP,V2,V3,K3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
       K3 == 0 ->
         (
           !,
           code2(V2C,V3C,Code),
           binsi2(OP,Action)
         ) 
      ) 
   )
 ).

code([bini,OP,V1,V2,K],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1 +  K << 2,
  Code is V1C + V2C << 16 + A << 32,
  binxi2(OP,Action).

code([ibin,OP,V2,V3,K3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
       K3 == #f ->
         (
           !,
           code2(V2C,V3C,Code),
           binis2(OP,Action)
         ) 
      ) 
   )
 ).

code([push2,V2,V3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code2(V2C,V3C,Code),
        Action = 'push-2variables-s'
     ) 
   )
 ).

code([push2,V1,V2],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1,
  Code is V1C + V2C << 16 + A << 32,
  Action = 'push-2variables-x'.

code([comp,CMP,V2,V3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code2(V2C,V3C,Code),
        binss(CMP,Action)
     ) 
   )
 ).

code([comp,CMP,V2,V3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1,
  Code is V1C + V2C << 16 + A << 32,
  binxx(CMP,Action).

code([push3,V1,V2,V3],Code,Action) :-
 V1=[V1C|_] ->
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code3(V1C,V2C,V3C,Code),
        Action = 'push-3variables-s'
     ) 
   )
 ).

code([push3,V1,V2,V3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  (V3=[V3C|_] -> A3=1 ; (V3=V3C, A3=0)),
  A is A1 + A2 << 1 + A3 << 2,
  Code is V1C + V2C << 16 + V3C << 32 + A << 48,
  Action = 'push-3variables-x'.

code3(V1,V2,V3,C) :- C is V1 + V2 << 16 + V3 << 32.
code2(V1,V2,C) :- C is V1 + V2 << 16.


")
