(define-module (logic guile-log guile-prolog attributator)
  #:use-module ((logic guile-log) #:select (<wrap> <cp> <code> <lambda>))
  #:use-module (logic guile-log umatch) 
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log prolog goal-functors)
  #:export (with_attributate with_attributates))


(define (with_attributate s p cc attribute val code)
  (with-fluid-guard-dynamic-object s p cc *var-attributator*
    (<lambda> ()			     
      (<code> (fluid-set! *var-attributator*
			  (cons
			   (cons attribute val)
			   (fluid-ref *var-attributator*))))
      (code))))


(define (with_attributates s p cc attributes code)
  (with-fluid-guard-dynamic-object s p cc *var-attributator*
    (<lambda> ()			     
      (<code> (fluid-set! *var-attributator*
			  (append
                           attributes
			   (fluid-ref *var-attributator*))))
      (code))))
