(define-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog operators)
  #:use-module (logic guile-log prolog goal-transformers)
  #:replace
  (
   op2+ op2- op1+ op1- #{\\}# op2* op2/ //
   ** ^ << >> #{/\\}# #{\\/}# op2< op2> op2>= op2=< =:= #{=\\=}#
   ))

(define-op op2+ gop2+)
(define-op op2- gop2-)
(define-op op1+ gop1+)
(define-op op1- gop1-)
(define-op #{\\}# #{g\\}#)
(define-op op2* gop2*)
(define-op op2/ gop2/)
(define-op // g//)
(define-op ** g**)
(define-op ^  g^)
(define-op << g<<)
(define-op >> g>>)
(define-op #{/\\}# #{g/\\}#)
(define-op #{\\/}# #{g\\/}#)
(define-op op2< gop2<)
(define-op op2> gop2>)
(define-op op2>= gop2>=)
(define-op op2=< gop2=<)
(define-op =:= g=:=)
(define-op #{=\\=}# #{g=\\=}#)

	
